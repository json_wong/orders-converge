<?php

class MailAction extends BaseAction {
	
	public function _initialize() {
		parent::_initialize();
	}
	
	public function lists() {
		if (!in_array('view_mail_list', $this->user_relation['permissions'])) {
			
		} else {
			$where = array();
			$m_mail_list = M("mail_list");
			$fields = "id,order_id,order_status,pubdate,status,opened,title,resolved,solver";
			if (!empty($_GET['status']) && in_array($_GET['status'], array('failure','success','submit','resolved'))) {
				if ($_GET['status'] == 'resolved') {
					$where['resolved'] = 1;
					$status = 'resolved';
				} else {
					$status = $where['status'] = $_GET['status'];
				}
			} else {
				$status = '';
			}
			$all_count = $m_mail_list->where(array("id"=>array('egt', 12), 'status'=>'success', 'resolved'=>0))->count("id");
			$open_count = $m_mail_list->where(array("id"=>array('egt', 12), "opened"=>1))->count("id");
			$open_percent = ($open_count / $all_count) * 100;
			$count = $m_mail_list->where($where)->count("id");
			$page = get_page($count, 20);
			$mails = $m_mail_list->field($fields)->where($where)->page($page['p'], $page['single'])->order("id DESC")->select();
			$mails = array_map(array($this, 'review_mail_data'), $mails);
			
			$this->base_assign();
			$solver = safe_cookie('solver');
			empty($solver) && $solver = '';
			$this->assign(array(
					'mails'=>$mails, 'page'=>$page['show'], 'menu'=>'mail', 'page_title'=>'发件箱', 'status'=>$status, 'solver'=>$solver,
					'open_percent'=>number_format($open_percent, 2), 'all_count'=>$all_count, 'opened_count'=>$open_count
			));
			$this->display();
		}
	}
	
	public function inbox() {
		if (!in_array('view_mail_in', $this->user_relation['permissions'])) {
				
		} else {
			$where = array("is_deleted"=>0);
			$m_mail_inbox = M("mail_inbox");
			$m_mail_list = M("mail_list");
			$m_order = M("orders");
			$fields = "id,mg_id,in_reply_to,fromer,toer,sender,subject,attach_count,pubdate,is_replyed";
			
			$count = $m_mail_inbox->where($where)->count("id");
			$page = get_page($count, 20);
			$mails = $m_mail_inbox->field($fields)->where($where)->page($page['p'], $page['single'])->order("id DESC")->select();
			foreach ($mails as $key=>$val) {
				if (!empty($val['in_reply_to'])) {
					$mg_id = $val['in_reply_to'];
					$val['reply_mail'] = $m_mail_list->field("id,order_id,order_status,pubdate,title,content")->where(array('mg_id'=>$mg_id))->find();
					if (!empty($val['reply_mail']) && !empty($val['reply_mail']['order_id'])) {
						$order_id = intval($val['reply_mail']['order_id']);
						$order = $m_order->field("email")->find($order_id);
						if (!empty($order)) {
							$val['reply_mail']['email'] = $order['email'];
						}
					}
					$mails[$key] = $val;
				}
			}
			
			$this->base_assign();
			$this->assign(array(
					'mails'=>$mails, 'page'=>$page['show'], 'menu'=>'inbox', 'page_title'=>'收件箱', 'all_count'=>$count
			));
			$this->display();
		}
	}
	
	public function review_mail_data($data='') {
		if ($data['order_status']) {
			list($data['order_status'], $data['order_status_btn_class'], $data['order_status_label_class']) = get_order_status($data['order_status']);
		}
		
		switch ($data['status']) {
			case 'success':
				$data['mail_status'] = "发送成功";
				$data['mail_status_color'] = "success";
				$data['mail_status_bg_color'] = "info";
				break;
			case 'failure':
				$data['mail_status'] = "发送失败";
				$data['mail_status_color'] = "important";
				$data['mail_status_bg_color'] = "error";
				break;
			case 'submit':
				$data['mail_status'] = "等待发送";
				$data['mail_status_color'] = "info";
				$data['mail_status_bg_color'] = "warning";
				break;
		}
		
		//获取用户名称和邮箱
		$order_info = M("orders")->field('domain,order_id,first_name,last_name,email,timestamp')->find($data['order_id']);
		if (!empty($order_info)) {
			$data['first_name'] = $order_info['first_name'];
			$data['last_name'] = $order_info['last_name'];
			$data['email'] = $order_info['email'];
			$data['order_numid'] = $order_info['order_id'];
			$data['domain'] = $order_info['domain'];
			$data['date'] = date('Y-m-d H:i:s', $order_info['timestamp']);
		}
		
		//获取网站前缀
		$data['prefix'] = M("website")->where(array('weburl'=>$order_info['domain']))->getField('prefix');
		
		if ($data['resolved'] == 1) {
			$data['status_remark'] = "操作人：" . $data['solver'];
			$resolved = M("mail_logs")->field('pubdate,value')->where(array('mail_id'=>$data['id'], 'event'=>'resolved'))->find();
			if (!empty($resolved)) {
				if (($resolved_value = json_decode($resolved['value'], true)) !== false) {
					$resolutive_way = get_resolutive_way_text($resolved_value['resolutive_way']);
					$data['status_remark'] .= "<br>解决时间：". $resolved['pubdate'];
					$data['status_remark'] .= "<br>解决方式：". $resolutive_way;
					if (!empty($resolved_value['other_desc'])) {
						$data['status_remark'] .= "<br>描述：". $resolved_value['other_desc'];
					}
				}
			}
		}
		
		return $data;
	}

	public function del_reply_mail($mail_id=0) {
		$mail_id = intval($mail_id);
		if ($mail_id > 0) {
			M("mail_inbox")->where(array("id"=>$mail_id))->setField("is_deleted", 1);
			$log = array("uid"=>$this->user_relation['id'], "ip"=>$_SERVER['REMOTE_ADDR']);
			M("mail_logs")->add(array("mail_id"=>$mail_id, "event"=>"del_reply_mail", "value"=>json_encode($log)));
			header("Location:" . $_SERVER['HTTP_REFERER']);
		}
	}

	public function mark_reply($mail_id=0) {
		$mail_id = intval($mail_id);
		if ($mail_id > 0) {
			M("mail_inbox")->where(array("id"=>$mail_id))->setField("is_replyed", 1);
			$log = array("uid"=>$this->user_relation['id'], "ip"=>$_SERVER['REMOTE_ADDR']);
			M("mail_logs")->add(array("mail_id"=>$mail_id, "event"=>"mark_reply", "value"=>json_encode($log)));
			header("Location:" . $_SERVER['HTTP_REFERER']);
		}
	}
}