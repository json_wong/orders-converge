<?php

class MgApiAction extends BaseAction {
	
	private $website;
	private $type;
	private $mg_id;
	private $post;
	private $action;
	
	public function _initialize() {
		if (!empty($_GET["_URL_"]))
			$this->url = $_GET ["_URL_"];
		
		if (count($this->url) > 1) {
			$this->type = $this->url[1];
			//$this->website = $this->url[2];
		} else {
			exit();
		}
		
		if (empty($_POST)) {
// 			$this->post = $_POST;
			exit();
		} else {
			$this->post = $_POST;
		}
	}
	
	public function _empty($name) {
		$func = 'mark_' . $name;
		if (method_exists($this, $func)) {
			$this->$func();
		} else {
			exit();
		}
	}
	
	/**
	 * 接收邮件
	 */
	public function receive_mail() {
		$data = array();
		if (!empty($this->post['Message-Id'])) {
			$data['mg_id'] = str_replace(array('<', '>'), array('', ''), I("post.Message-Id"));
		} else if (!empty($this->post['message-id'])) {
			$data['mg_id'] = str_replace(array('<', '>'), array('', ''), I("post.message-id"));
		} else {
			$data['mg_id'] = '';
		}
		
		if (!empty($this->post['In-Reply-To'])) {
			$data['in_reply_to'] = str_replace(array('<', '>'), array('', ''), I("post.In-Reply-To"));
			if ($data['in_reply_to'] == $data['mg_id']) {
				$data['in_reply_to'] = '';
			}
		} else if (!empty($this->post['References'])) {
			$data['in_reply_to'] = str_replace(array('<', '>'), array('', ''), I("post.References"));
			if ($data['in_reply_to'] == $data['mg_id']) {
				$data['in_reply_to'] = '';
			}
		}
		
		$data['fromer'] = I('From');
		$data['toer'] = I('To');
		$data['sender'] = I('Sender');
		$data['subject'] = I('Subject');
		$data['attach_count'] = I('attachment-count', 0, 'intval');
		$data['value'] = json_encode($this->post);
		M("mail_inbox")->add($data);
	}
	
	/**
	 * 标记为已打开
	 */
	public function mark_opened() {
		if (!empty($this->post['event']) && $this->post['event'] == 'opened' && !empty($this->post['message-id'])) {
			$this->add_logs(I("post.message-id"));
		}
	}
	
	/**
	 * 标记为已点击
	 */
	public function mark_clicked() {
		if (!empty($this->post['event']) && $this->post['event'] == 'clicked' && !empty($this->post['message-id'])) {
			$this->add_logs(I("post.message-id"));
		}
	}
	
	/**
	 * 标记为退订
	 */
	public function mark_unsubscribed() {
		if (!empty($this->post['event']) && $this->post['event'] == 'unsubscribed' && !empty($this->post['message-id'])) {
			$this->add_logs(I("post.message-id"));
		}
	}
	
	/**
	 * 标记为弹回
	 */
	public function mark_bounced() {
		if (!empty($this->post['event']) && $this->post['event'] == 'bounced' && !empty($this->post['Message-Id'])) {
			$this->add_logs(I("post.Message-Id"));
		}
	}
	
	/**
	 * 标记为垃圾邮件投诉
	 */
	public function mark_complained() {
		if (!empty($this->post['event']) && $this->post['event'] == 'complained' && !empty($this->post['message-id'])) {
			$this->add_logs(I("post.Message-Id"));
		}
	}
	
	/**
	 * 标记为遇到问题
	 */
	public function mark_dropped() {
		if (!empty($this->post['event']) && $this->post['event'] == 'dropped' && !empty($this->post['Message-Id'])) {
			$this->add_logs(I("post.Message-Id"));
		}
	}
	
	/**
	 * 标记为成功接收
	 */
	public function mark_delivered() {
		if (!empty($this->post['event']) && $this->post['event'] == 'delivered' && !empty($this->post['Message-Id'])) {
			$this->add_logs(I("post.Message-Id"));
		}
	}
	
	/**
	 * 添加日志数据
	 * 
	 * @param string $mg_id mailgun ID
	 */
	public function add_logs($mg_id='') {
		if (!empty($mg_id)) {
			$mail_list = M("mail_list");
			$mg_id = str_replace(array('<', '>'), array('', ''), $mg_id);
			$mail_id = $mail_list->where(array('mg_id'=>$mg_id))->getField('id');
			if (!empty($mail_id) && $mail_id > 0) {
				$data = array('mail_id'=>$mail_id, 'event'=>$this->post['event'], 'value'=>json_encode($this->post));
				M("mail_logs")->add($data);
				
				if ($this->post['event'] == 'delivered') {
					$mail_list->where(array('id'=>$mail_id))->setField('status', 'success');
				} else if (in_array($this->post['event'], array('bounced', 'dropped'))) {
					$mail_list->where(array('id'=>$mail_id))->setField('status', 'failure');
				} else if ($this->post['event'] == 'opened') {
					$mail_list->where(array('id'=>$mail_id))->setField('opened', 1);
				}
			}
		}
	}
}