<?php
use Mailgun\Mailgun;
class AjaxAction extends BaseAction {
	
	/**
	 * 构造方法(non-PHPdoc)
	 *
	 * @see BaseAction::_initialize()
	 */
	public function _initialize() {
		if (! $this->isAjax ()) {
			$this->not_found();
		}
		parent::_initialize ();
	}
	
	/**
	 * (non-PHPdoc)
	 *
	 * @see Action::__call()
	 */
	public function _empty($method = '') {
		$this->not_found($method);
	}
	
	public function add_website() {
		if (!in_array('add_website', $this->user_relation['permissions'])) {
			$this->not_found('403');
		}
		
		if ($this->isPost()) {
			C('TOKEN_ON',false);
			$website = M('website');
			if ($website->create()) {
				preg_match("/^(http:\/\/)?([^\/]+)/i", $website->weburl, $matches);
				
				if (!empty($matches[2])) {
					$website->times = time();
					$website->uid = $this->user_relation['id'];
					if (empty($website->id)) {
						$website->weburl = $matches[2];
						$exists = $website->where(array('weburl'=>$website->weburl))->count('id');
						if ($exists > 0) {
							$this->ajaxReturn(array('status'=>500, 'info'=>'Error: 该网站已存在，不要重复添加！'));
						}
						unset($website->id);
						$result = $website->add();
					} else {
						$result = $website->save();
					}
					
					if ($result !== false) {
						$this->ajaxReturn(array('status'=>200, 'info'=>'保存成功！'));
					} else {
						$this->ajaxReturn(array('status'=>500, 'info'=>'Error: 写入数据失败！'));
					}
				} else {
					$this->ajaxReturn(array('status'=>500, 'info'=>'Error: 获取主域名失败，请确认输入域名是否正确！'));
				}
			} else {
				$this->ajaxReturn(array('status'=>500, 'info'=>'Error: 创建数据失败！'. $website->getError()));
			}
		}
	}
	
	public function add_webcate() {
		if (!in_array('add_webcate', $this->user_relation['permissions'])) {
			$this->not_found('403');
		}
	
		if ($this->isPost()) {
			C('TOKEN_ON',false);
			$webcate = M('webcate');
			if ($webcate->create()) {
				
				if (!empty($webcate->q_mail)) {//已确认
					$webcate->q_mail = json_encode($webcate->q_mail);
				}
				if (!empty($webcate->c_mail)) {//已付款
					$webcate->c_mail = json_encode($webcate->c_mail);
				}
				if (!empty($webcate->p_mail)) {//已发货
					$webcate->p_mail = json_encode($webcate->p_mail);
				}
				if (!empty($webcate->i_mail)) {//已取消
					$webcate->i_mail = json_encode($webcate->i_mail);
				}
				if (!empty($webcate->t_mail)) {//已退款
					$webcate->t_mail = json_encode($webcate->t_mail);
				}
				if (!empty($webcate->r_mail)) {//扣关重发
					$webcate->r_mail = json_encode($webcate->r_mail);
				}
				if (!empty($webcate->press1_mail)) {//催款1
					$webcate->press1_mail = json_encode($webcate->press1_mail);
				}
				if (!empty($webcate->press2_mail)) {//催款2
					$webcate->press2_mail = json_encode($webcate->press2_mail);
				}
				if (!empty($webcate->press3_mail)) {//催款3
					$webcate->press3_mail = json_encode($webcate->press3_mail);
				}
				if (!empty($webcate->press4_mail)) {//催款4
					$webcate->press4_mail = json_encode($webcate->press4_mail);
				}
				if (!empty($webcate->press5_mail)) {//催款5
					$webcate->press5_mail = json_encode($webcate->press5_mail);
				}
				
				if (empty($webcate->cid)) {
					unset($website->id);
					$result = $webcate->add();
				} else {
					$result = $webcate->save();
				}
					
				if ($result !== false) {
					$this->ajaxReturn(array('status'=>200, 'info'=>'保存成功！'));
				} else {
					$this->ajaxReturn(array('status'=>500, 'info'=>'Error: 写入数据失败！'));
				}
			} else {
				$this->ajaxReturn(array('status'=>500, 'info'=>'Error: 创建数据失败！'. $webcate->getError()));
			}
		}
	}
	
	public function add_account() {
		if (!in_array('create_account', $this->user_relation['permissions'])) {
			$this->not_found();
		}
		if ($this->isPost()) {
			C('TOKEN_ON',false);
			$account = M('account');
			if ($account->create()) {
				$exists = $account->where(array('username'=>$account->username))->count('id');
					
				if ($exists > 0) {
					$this->ajaxReturn(array('status'=>500, 'info'=>'Error: 该账号已存在，不要重复添加！'));
				}
				$account->random = rand_string(10);
				$account->userpass = transition_pass($account->userpass, $account->random);
				$account->permissions = json_encode($account->permissions);
				$account->regdate = date('Y-m-d H:i:s');
				$result = $account->add();
				if ($result > 0) {
					$this->ajaxReturn(array('status'=>200, 'info'=>'添加成功！'));
				} else {
					$this->ajaxReturn(array('status'=>500, 'info'=>'Error: 写入数据失败！'));
				}
			} else {
				$this->ajaxReturn(array('status'=>500, 'info'=>'Error: 创建数据失败！'. $account->getError()));
			}
		}
	}
	
	public function save_account() {
		if (!in_array('opera_account', $this->user_relation['permissions'])) {
			$this->not_found();
		}
		if ($this->isPost() && !empty($_POST['uid'])) {
			C('TOKEN_ON',false);
			$uid = intval($_POST['uid']);
			if ($uid === 1) {
				$this->ajaxReturn(array('status'=>500, 'info'=>'管理员账号不允许修改！'));
			}
			$account = M('account');
			if ($account->create()) {
				if (!empty($account->userpass)) {
					$account->random = rand_string(10);
					$account->userpass = transition_pass($account->userpass, $account->random);
				}
				$account->permissions = json_encode($account->permissions);
				$result = $account->where(array('id'=>$uid))->save();
				if ($result > 0) {
					$this->ajaxReturn(array('status'=>200, 'info'=>'修改成功！'));
				} else {
					$this->ajaxReturn(array('status'=>500, 'info'=>'Error: 保存数据失败！'));
				}
			} else {
				$this->ajaxReturn(array('status'=>500, 'info'=>'Error: 创建数据失败！'. $account->getError()));
			}
		}
	}
	
	public function get_account() {
		if (!in_array('opera_account', $this->user_relation['permissions'])) {
			$this->not_found();
		}
		if ($this->isPost()) {
			$uid = $this->_post('uid', 'intval', 0);
			if ($uid > 0) {
				$data = M('account')->field('username, permissions')->find($uid);
				if (!empty($data)) {
					$data['permissions'] = json_decode($data['permissions'], true);
					$this->ajaxReturn(array('status'=>200, 'info'=>$data));
				} else {
					$this->ajaxReturn(array('status'=>500, 'info'=>'账号不存在！'));
				}
			} else {
				$this->ajaxReturn(array('status'=>500, 'info'=>'非法提交!'));
			}
		}
	}
	
	
	public function get_webkey() {
		if (!in_array('get_website_key', $this->user_relation['permissions'])) {
			$this->not_found();
		}
		if ($this->isPost()) {
			$id = $this->_post('id', 'intval', 0);
			if ($id > 0) {
				$weburl = M('website')->where(array('id'=>$id))->getField('weburl');
				if (!empty($weburl)) {
					$webkey = $this->calculate_hash($weburl);
					
					$this->ajaxReturn(array('status'=>200, 'key'=>$webkey));
				} else {
					$this->ajaxReturn(array('status'=>500, 'info'=>'数据不存在！'));
				}
			} else {
				$this->ajaxReturn(array('status'=>500, 'info'=>'非法提交！'));
			}
		}
	}
	
	public function get_order_json() {
		if ($this->user_relation['username'] !== 'developer') {
			$this->not_found();
		}
		if ($this->isPost()) {
			$id = $this->_post('id', 'intval', 0);
			if ($id > 0) {
				$values = M('orders')->where(array('id'=>$id))->getField('values');
				if (!empty($values)) {
					$this->ajaxReturn(array('status'=>200, 'values'=>$values));
				} else {
					$this->ajaxReturn(array('status'=>500, 'info'=>'数据不存在！'));
				}
			} else {
				$this->ajaxReturn(array('status'=>500, 'info'=>'非法提交！'));
			}
		}
	}

	public function delete_orders() {
		if (!in_array('delete_orders', $this->user_relation['permissions'])) {
			$this->not_found();
		}
		if ($this->isPost()) {
			$id = $this->_post('id', 'intval', 0);
			if ($id > 0) {
				$values = M('orders')->where(array('id'=>$id))->setField('is_deleted', 1);
				if ($values) {
					$this->ajaxReturn(array('status'=>200, 'values'=>$values));
				} else {
					$this->ajaxReturn(array('status'=>500, 'info'=>'删除失败，请联系技术！', 'result'=>$values, 'error'=>M('orders')->getlasterror()));
				}
			} else {
				$this->ajaxReturn(array('status'=>500, 'info'=>'非法提交！'));
			}
		}
	}
	
	/**
	 * ajax user login entry
	 */
	public function login() {
		if (empty($_POST ['username']) || empty($_POST ['userpass'])) {
			$this->ajaxReturn(array('status'=>500, 'info'=>'用户名或密码不能为空！'));
		}
		
		//verify whether the IP restrictions
		$ip = $_SERVER['REMOTE_ADDR'];
		$logs = M('Logs');
		$time_start = time() - 7440;
		$where = array('ip'=>$ip, 'type'=>'account_error', 'timestamp'=>array('gt', $time_start));
		$account_error_num = $logs->where($where)->count('id');
		if ($account_error_num >= 3) {
			$this->ajaxReturn(array('status'=>500, 'info'=>'您已被限制两小时内不能登录！'));
		}
		
		$admin = M ( 'Account' );
		
		if (! $admin->autoCheckToken ( $_POST ) ) {
			$this->ajaxReturn(array('status'=>500, 'info'=>'令牌错误，请刷新页面重试！'));
		} else {
			import ( "ORG.Util.Input" );
			$uname = strtolower ( Input::forSearch ( addslashes( $_POST ['username'] ) ) );
			$upass = $_POST ['userpass'];
			$result = $admin->field ( 'id,username,userpass,random,permissions,islock' )->where ( "username='{$uname}'" )->find ();
				
			if (! empty ( $result )) {
                if ($result['islock'] == 1) {
                    $this->ajaxReturn(array('status'=>500, 'info'=>'账号已被锁定，无法登陆。'));
                }
				//password error count
				$where = array('uid'=>$result['id'], 'type'=>'pass_error', 'timestamp'=>array('gt', $time_start));
				$pass_error_num = $logs->where($where)->count('id');
				if ($pass_error_num >= 3) {
					$this->ajaxReturn(array('status'=>500, 'info'=>'错误密码超过3次，锁定两小时！'));
				}
				
				$postpass = transition_pass ( $upass, $result ['random'] );
				if ($postpass == $result ['userpass']) {
					import ( "ORG.Util.Session" );
					$admin->where(array('username'=>$uname))->save(array('lastlogin'=>date('Y-m-d H:i:s')));
	
					if (!empty($_POST['remember_me']) && $_POST['remember_me'] === 'yes') {
						safe_cookie('user', array('uid'=>$result ['id']), 31536000);
					}
					Session::set ( "user", json_encode ( $result ) );
					$this->ajaxReturn(array('status'=>200, 'info'=>'登录成功！', 'data'=>array('url'=>U('/', '', false, false, true))));
				} else {
					$surplus = 2 - $pass_error_num;
					$log_data = array('uid'=>$result['id'], 'type'=>'pass_error', 'values'=>$upass, 'timestamp'=>time(), 'ip'=>$_SERVER['REMOTE_ADDR']);
					$logs->add($log_data);
					$this->ajaxReturn(array('status'=>500, 'info'=>'密码错误，您还有' . $surplus . '次机会。'));
				}
			} else {
				$logs->add(array('type'=>'account_error', 'values'=>$uname, 'timestamp'=>time(), 'ip'=>$_SERVER['REMOTE_ADDR']));
				$this->ajaxReturn(array('status'=>500, 'info'=>'此账号没有被注册！'));
			}
		}
	}
	
	public function change_order_status() {
		if ($this->isPost()) {
			$order_id = $this->_post('order_id', 'intval', 0);
			$order_status = $this->_post('order_status');
			$input_oi_email = $this->_post('input_oi_email', 'htmlspecialchars,trim', "");
			$remark = $this->_post('remark', 'htmlspecialchars,strip_tags', '');
			if (in_array($order_status, $this->activate_order_status)) {
				$m_orders = M('orders');
				$order_info = $m_orders->field('system_type,domain,email,order_status,shipping_method,values')->where(array('id'=>$order_id))->find();
				
				if (empty($order_info)) {
					$this->ajaxReturn(array("status"=>500, "info"=>"非法访问"));
				}
				$order_info['values'] = json_decode($order_info['values'], true);
				if ($order_status !== 'press') { //状态不是催款才进行状态更新
					$result = $m_orders->where(array('id'=>$order_id))->setField('order_status', $order_status);
					$log_type = "modify_status";
				} else {
					$log_type = "reminders";
				}
				
				if ($result !== false) {
					//写状态更改日志
					$m_logs = M("logs");
					$log_data = array(
							'uid'=>$this->user_relation['id'], 'type'=>$log_type, 'order_id'=>$order_id, 'values'=>$order_status,
							'timestamp'=>time(), 'ip'=>$_SERVER['REMOTE_ADDR'], 'remark'=>$remark
					);
					$log_id = $m_logs->add($log_data);

					if ($order_status == 'ready') {//待发货状态，写入日志
						$current_ready_id = M("variables")->where(array("variable_type"=>"current_ready_id"))->getField("variable_value");
						$current_ready_id = json_decode($current_ready_id, true);
						if ($current_ready_id) {
							$current_ready_id[] = $order_id;
							M("variables")->where(array("variable_type"=>"current_ready_id"))->setField("variable_value", json_encode($current_ready_id));
							
							if ($order_info['system_type'] == 'cscart') {
								foreach ($order_info['values']['cart']['products'] as $key=>$val) {
									if (!empty($val['main_pair']['detailed']['image_path'])) {
										$picurl = $val['main_pair']['detailed']['image_path'];
										if (strpos($picurl, APP_DOMAIN) === false) {
											if (strpos($picurl, 'http') === false) {
												$picurl = "http://" . $order_info['domain'] . $picurl;
											}
											$local_array = local_image($picurl);
											if (!empty($local_array)) {
												$order_info['values']['cart']['products'][$key]['main_pair']['detailed']['image_path'] = APP_DOMAIN . $local_array['fullpath'];
											}
										}
									}
								}
								M("orders")->where(array("id"=>$order_id))->setField("values", json_encode($order_info['values']));
							}
						} else {
							$m_orders->where(array('id'=>$order_id))->setField("order_status", $order_info['order_status']);
							$m_logs->where(array("id"=>$log_id))->delete();
							$this->ajaxReturn(array('status'=>500, 'info'=>'解析数据出现错误，无法更改为待发货，请联系相关人员！', 'data'=>$current_ready_id));
						}
					}
					
					//不发送邮件的状态更改直接返回
					if (in_array($order_status, $this->not_send_mail_status_key)) {
						$this->ajaxReturn(array('status'=>200, 'info'=>'success'));
					}
					
					$site = M("website")->field('webname,smtp_info,sendmail_domain')->where(array('weburl'=>$order_info['domain']))->find();
					if (!empty($site)) {
						if (!empty($order_info)) {
							//判断提交过来的邮箱地址是否和数据库中的一致
							//如果不一致将更换用户邮箱
							if (!empty($input_oi_email) && $input_oi_email != $order_info['email']) {
								$order_info['email'] = $input_oi_email;
								$m_orders->where(array("id"=>$order_id))->setField("email", $input_oi_email);
								$m_logs->add(array(
									'uid'=>$this->user_relation['id'], 'type'=>'correct_email', 'order_id'=>$order_id, 
									'values'=>$input_oi_email, 'timestamp'=>time(), 'ip'=>$_SERVER['REMOTE_ADDR']
								));
							}
							
							$mail_content = I('mail_content');
							$ems_number = I('ems_number');
							//如果是更改为已发货，将正文的星号替换为EMS号
							if ($order_status == 'P') { //更新订单号数据
								$mail_content = str_replace("***********", $ems_number, $mail_content);
								$m_orders->where(array('id'=>$order_id))->save(array('ems_number'=>$ems_number, 'customs_hold'=>0));
								$result = M("delivery")->add(array(
										'order_id'=>$order_id, 'ems_number'=>$ems_number, 'shipping_method'=>$order_info["shipping_method"]
								), array(), true);
							} else if (in_array($order_status, array('I', 'T'))) {
								//如果是已退款或者已取消，将海关扣押状态解除
								$m_orders->where(array('id'=>$order_id))->save(array('customs_hold'=>0));
							}
                            
							$mail_info = array(
								'mailto'=>$order_info['email'], 'mail_title'=>I('mail_title'), 
								'mail_content'=>$mail_content, 'account_name'=>I('account_name'), 
								'domain'=>$order_info['domain'], 'webname'=>$site['webname'],
								'order_id'=>$order_id, 'order_status'=>$order_status, 
                                'sendmail_domain' => $site['sendmail_domain']
							);
							$smtp_info = json_decode($site['smtp_info'], true);
							$smtp_info = !empty($smtp_info) ? $smtp_info : array();
							$mail_result = $this->send_mail(array_merge($smtp_info, $mail_info));
							if ($mail_result !== true) {
								$m_logs->add(array(
										'uid'=>$this->user_relation['id'], 'type'=>'sendmail_error', 'order_id'=>$order_id, 
										'values'=>$mail_result, 'timestamp'=>time(), 'ip'=>$_SERVER['REMOTE_ADDR']
								));
								$m_orders->where(array('id'=>$order_id))->setField('order_status', $order_info['order_status']);
								$this->ajaxReturn(array('status'=>500, 'info'=>'邮件发送失败：'. $mail_result, 'mail_info'=>$mail_info, 'smtp_info'=>$smtp_info));
							} else {
								if ($order_status == 'press') {
									$m_orders->where(array("id"=>$order_id))->setInc("press");
								} else {
									M("order_remark")->where(array("order_id"=>$order_id))->setField("dispost", 1);
								}
							}
						}
					}
					$this->ajaxReturn(array('status'=>200, 'info'=>'success'));
				} else {
					$this->ajaxReturn(array('status'=>500, 'info'=>'更新状态时出错'));
				}
			} else {
				$this->ajaxReturn(array('status'=>500, 'info'=>'非法状态'));
			}
		} else {
			$this->ajaxReturn(array('status'=>500, 'info'=>'非法访问'));
		}
	}
	
	public function save_smtp_info() {
		if ($this->isPost()) {
			$sid = I('id', 0, 'intval');
			if ($sid > 0) {
				$smtp = array(
						'account_name'=>I('account_name'), 'smtp_host'=>I('smtp_host'), 'smtp_account'=>I('smtp_account'), 
						'smtp_password'=>I("smtp_password"), 'smtp_port'=>I('smtp_port', 25, 'intval'), 
						'smtp_auth'=>I('smtp_auth', '0', 'intval'), 'auth_type'=>I('auth_type')
				);
				empty($smtp['smtp_port']) && $smtp['smtp_port'] = 25;
				if (empty($smtp['auth_type']) || !in_array($smtp['auth_type'], array('tls', 'ssl'))) {
					$smtp['auth_type'] = 'tls';
				}
				$site = M("website")->field('id,smtp_info')->find($sid);
				if (!empty($site)) {
					$old_smtp = json_decode($site['smtp_info'], true);
					if (!empty($old_smtp['smtp_password']) && empty($smtp['smtp_password'])) {
						$smtp['smtp_password'] = $old_smtp['smtp_password'];
					}
				}
				$save_data = array('smtp_info'=>json_encode($smtp));
				$result = M("website")->where(array('id'=>$sid))->save($save_data);
				if ($result !== false) {
					$this->ajaxReturn(array('status'=>200, 'info'=>''));
				} else {
					$this->ajaxReturn(array('status'=>500, 'info'=>'保存数据失败', 'sql'=>M("website")->getLastSql()));
				}
			}
		}
		$this->ajaxReturn(array('status'=>500, 'info'=>'非法访问'));
	}
	
	public function get_mail_log($mail_id='') {
		$mail_id = intval($mail_id);
		if ($mail_id > 0) {
			$logs = M("mail_logs")->field("pubdate,event,value")->where(array('mail_id'=>$mail_id))->order("id DESC")->select();
			if (!empty($logs)) {
				foreach ($logs as $key=>$val) {
					if (!empty($val['value']) && $value = json_decode($val['value'], true)) {
						
						$data = array(
								'pubdate'=>$val['pubdate'], 'domain'=>$value['domain'], 'recipient'=>$value['recipient'], 'event'=>$value['event'],
								'json'=>$value
						);
						list($data['event_text'], $data['event_class'], $desc) = mail_event_to_text($value['event']);
						if (!empty($value['error'])) {
							$data['message'] = $value['error'];
						} else if (!empty($value['description'])) {
							$data['message'] = $value['description'];
						} else if ($value['event'] == 'resolved') {
							$data['message'] = "解决方式：" . get_resolutive_way_text($value['resolutive_way']) . "。";
							if (!empty($value['other_desc'])) {
								$data['message'] .= "描述：" . $value['other_desc'] . "。";
							}
							$data['message'] .= "操作人：" . $value['solver'];
						} else {
							$data['message'] = $desc;
						}
						$data['code'] = !empty($value['code']) ? $value['code'] : '200';
					} else {
						$data = array('pubdate'=>$val['pubdate'], 'message'=>$val['value']);
						if ($val['event'] == 'submit') {
							list($data['event_text'], $data['event_class'], $desc) = mail_event_to_text($val['event']);
							$data['event'] = $val['event'];
							$data['code'] = '200';
							$data['message'] = $desc;
						} else {
							$data['message'] = '数据损坏';
							$data['code'] = '404';
						}
					}
					$logs[$key] = $data;
				}
				$this->ajaxReturn(array('status'=>200, 'logs'=>$logs));
			} else {
				$this->ajaxReturn(array('status'=>500, 'info'=>'不存在日志'));
			}
		} else {
			$this->ajaxReturn(array('status'=>500, 'info'=>'参数有误！'));
		}
	}
	
	/**
	 * 解决问题邮件
	 */
	public function solve_problem_mail() {
		$mail_id = I("post.mail_id", 0, 'intval');
		$solver = I("post.solver");
		$resolutive_way = I("post.resolutive_way");
		$other_desc = I("post.other_desc");
		if ($mail_id > 0 && !empty($solver)) {
			safe_cookie('solver', $solver, 31536000);
			$value = json_encode(array('event'=>'resolved', 'solver'=>$solver, 'resolutive_way'=>$resolutive_way, 'other_desc'=>$other_desc));
			$log = array('mail_id'=>$mail_id, 'event'=>'resolved', 'value'=>$value);
			$result = M("mail_logs")->add($log);
			if (!empty($result)) {
				$save = array('status'=>'success', 'resolved'=>1, 'solver'=>$solver);
				$result = M("mail_list")->where(array('id'=>$mail_id))->save($save);
				if ($result !== false) {
					$this->ajaxReturn(array('status'=>200, 'info'=>'保存成功！'));
				} else {
					$this->ajaxReturn(array('status'=>500, 'info'=>'保存主表失败！'));
				}
			} else {
				$this->ajaxReturn(array('status'=>500, 'info'=>'添加日志失败！'));
			}
		} else {
			$this->ajaxReturn(array('status'=>500, 'info'=>'参数有误！'));
		}
	}
	
	public function get_mail_detail($mail_id='', $email='') {
		$mail_id = intval($mail_id);
		if ($mail_id > 0) {
			$mail = M("mail_list")->field('title,content')->find($mail_id);
			if (!empty($mail)) {
				$data = "<table class='table table-bordered table-condensed'>
				<tr><td width='15%'><strong>邮箱地址：</strong></td><td>$email</td></tr>
				<tr><td><strong>邮件标题：</strong></td><td>{$mail['title']}</td></tr>
				<tr><td><strong>邮件内容：</strong></td><td>{$mail['content']}</td></tr>
				</table>";
				$this->ajaxReturn(array('status'=>200, 'data'=>$data));
			}
		}
	}
	
	public function get_inbox_mail_detail($mail_id="") {
		$mail_id = intval($mail_id);
		if ($mail_id > 0) {
			$mail = M("mail_inbox")->field('is_deleted', true)->find($mail_id);
			if (!empty($mail)) {
				$mail['value'] = str_replace('\n', "<br>", $mail['value']);
// 				$mail['value'] = str_replace("\\\\", "\\", $mail['value']);
				if (get_magic_quotes_gpc()) {
					$mail['value'] = stripslashes($mail['value']);
				}
				$mail['data'] = json_decode($mail['value'], true);
				if (is_array($mail['data']) && !empty($mail['data'])) {
					$data = "<table class='table table-bordered table-condensed'>
					<tr><td width='15%'><strong>邮件标识：</strong></td><td>{$mail['mg_id']}</td></tr>
					<tr><td><strong>发件人：</strong></td><td>".htmlspecialchars($mail['fromer'])."</td></tr>
					<tr><td><strong>收件人：</strong></td><td>".htmlspecialchars($mail['toer'])."</td></tr>
					<tr><td><strong>User-Agent：</strong></td><td>".$mail['data']['User-Agent']."</td></tr>
					<tr><td><strong>Received：</strong></td><td>".htmlspecialchars($mail['data']['Received'])."</td></tr>
					<tr><td><strong>发件日期：</strong></td><td>{$mail['data']['Date']}</td></tr>
					<tr><td><strong>接收日期：</strong></td><td>{$mail['pubdate']}</td></tr>
					<tr><td><strong>邮件标题：</strong></td><td>{$mail['subject']}</td></tr>
					<tr><td><strong>邮件内容：</strong></td><td>{$mail['data']['body-plain']}</td></tr>
					</table>";
					$this->ajaxReturn(array('status'=>200, 'data'=>$data, 'mail'=>$mail));
				} else {
					switch(json_last_error()) {
						case JSON_ERROR_DEPTH: $error = 'Maximum stack depth exceeded'; break;
						case JSON_ERROR_CTRL_CHAR: $error = 'Unexpected control character found';
							break;
						case JSON_ERROR_SYNTAX:
							$error = 'Syntax error, malformed JSON';
							break;
						case JSON_ERROR_NONE:
							$error = 'No errors';
							break;
					}
					$this->ajaxReturn(array('status'=>500, 'data'=>"邮件内容无法解析，请联系相关人员。", "mail"=>$mail, "error"=>json_last_error()));
				}
			}
		}
	}
	
	public function publish_order_remark() {
		$remark_content = I("post.remark_content", '', 'htmlspecialchars');
		$beeptimer = I("post.beeptimer", false);
		$beeptimer = $beeptimer == 'yes' ? 1 : 0;
		$beeptimer_num = I("post.beeptimer_num", 0, 'intval');
		$des_date = date("Y-m-d", time() + 86400 * $beeptimer_num);
		$order_id = I("post.order_id", 0, "intval");
		if (!empty($order_id) && !empty($remark_content)) {
			$data = array(
					"uid"=>$this->user_relation['id'], "order_id"=>$order_id, "beeptimer"=>$beeptimer,
					"beeptimer_date"=>$des_date, "remark"=>$remark_content
			);
			$result = M("order_remark")->add($data);
			if (!empty($result)) {
				M("orders")->where(array("id"=>$order_id))->setField("remark", 1);
				$this->ajaxReturn(array('status'=>200, 'info'=>'success'));
			} else {
				$this->ajaxReturn(array('status'=>500, 'info'=>'保存备注失败'));
			}
		}
		$this->ajaxReturn(array('status'=>500, 'info'=>'failure'));
	}

	public function add_product() {
		if (!empty($_POST['order_id'])) {
			$order_id = I("post.order_id", 0, "intval");
			$product_id = uniqid();
			$product_title = I("post.product_title", "");
			$product_image = I("post.product_image", "");
			$product_options = I("post.product_options", "");
			$product_sjje = I("post.product_sjje", "");
			$product_yhje = I("post.product_yhje", "");
			$product_zjg = I("post.product_zjg", "");
			$product_qty = I("post.product_qty", 0, "intval");
			$product_bh = I("post.product_bh", '');
			$data_type = "add_product";

			$obj_orders = M("orders");
			$obj_order_history = M("order_history");
			$old_order = $obj_orders->field(true)->find($order_id);
			$product_options = str_replace("\n", "<br>", $product_options);

			if (!empty($old_order)) {
				$old_order["values"] = json_decode($old_order["values"], true);
				$order_copy = $old_order;
				switch ($order_copy["system_type"]) {
					case 'cscart':
						$product_data = array();
						$local_array = local_image($product_image);
						$product_data["product_options_info"] = $product_options;
						$product_data["amount"] = $product_qty;
						$product_data["product_title"] = $product_title;
						$product_data["main_pair"]["detailed"]["image_path"] = APP_DOMAIN . $local_array['fullpath'];
						$product_data["price"] = $product_sjje;
						$product_data["discount"] = $product_yhje;
						$product_data["product_code"] = $product_bh;
						$order_copy["values"]["cart"]["products"][$product_id] = $product_data;
						break;
					case 'magento':
						$product_data = array();
						$local_array = local_image($product_image);
						$product_data["product_options"]["options"] = $product_options;
						$product_data["qty_ordered"] = $product_qty;
						$product_data["name"] = $product_title;
						$product_data["product_image"] = APP_DOMAIN . $local_array['fullpath'];
						$product_data["price"] = $product_sjje;
						$product_data["old_price"] = $product_sjje + $product_yhje;
						$product_data["sku"] = $product_bh;
						$order_copy["values"]["product"][$product_id] = $product_data;
						break;
					case 'wordpress':
						$product_data = array();
						$local_array = local_image($product_image);
						$product_data["product_options_info"] = $product_options;
						$product_data["quantity"] = $product_qty;
						$product_data["product_name"] = $product_title;
						$product_data["product_image"] = APP_DOMAIN . $local_array['fullpath'];
						$product_data["unit_price"] = $product_sjje;
						$product_data["discount"] = $product_yhje;
						$product_data["sku"] = $product_bh;
						$order_copy["values"]["order_info"]["cart_items"][$product_id] = $product_data;
						break;
					case 'zencart':
						$product_data = array();
						$local_array = local_image($product_image);
						$product_data["name"] = $product_title;
						$product_data["product_image"] = APP_DOMAIN . $local_array['fullpath'];
						$product_data["final_price"] = $product_sjje;
						$product_data["product_is_free"] = $product_yhje;
						$product_data["product_options_info"] = $product_options;
						$product_data["qty"] = $product_qty;
						$product_data["model"] = $product_bh;
						$order_copy["values"]["products"][$product_id] = $product_data;
						break;
					default:
						$this->ajaxReturn(array("status"=>500, "info"=>"非法的订单类型！"));
						break;
				}

				$new_values = json_encode($order_copy["values"]);
				$result = $obj_orders->where(array("id"=>$order_id))->setField("values", $new_values);
				if ($result !== false) {
					$history_value = json_encode($old_order);
					$history = array("order_id"=>$order_id, "uid"=>$this->user_relation["id"], "action_type"=>$data_type, "value"=>$history_value);
					$obj_order_history->add($history);
					$this->ajaxReturn(array("status"=>200, "info"=>"success"));
				} else {
					$this->ajaxReturn(array("status"=>500, "info"=>"数据保存失败！"));
				}
			} else {
				$this->ajaxReturn(array("status"=>500, "info"=>"订单实例无法对应！"));
			}
		}
	}
	
	public function modify_product() {
		if (!empty($_POST['order_id']) && !empty($_POST['product_id'])) {
			$order_id = I("post.order_id", 0, "intval");
			$product_id = I("post.product_id", 0);
			$product_image = I("post.product_image", "");
			$product_options = I("post.product_options", "");
			$product_qty = I("post.product_qty", 0, "intval");
			$data_type = "modify_product";
			if (!empty($this->url[2])) {
				$data_type = $this->url[2];
			}
			$obj_orders = M("orders");
			$obj_order_history = M("order_history");
			$old_order = $obj_orders->field(true)->find($order_id);
			$product_options = str_replace("\n", "<br>", $product_options);
			if (!empty($old_order)) {
				$old_order["values"] = json_decode($old_order["values"], true);
				$order_copy = $old_order;
				switch ($order_copy["system_type"]) {
					case 'cscart':
						if (!empty($order_copy["values"]["cart"]["products"][$product_id])) {
							if ($data_type == 'modify_product') {
								if ($order_copy["values"]["cart"]["products"][$product_id]["main_pair"]["detailed"]["image_path"] != $product_image) {
									$local_array = local_image($product_image);
									if (!empty($local_array)) {
										$order_copy["values"]["cart"]["products"][$product_id]["main_pair"]["detailed"]["image_path"] = APP_DOMAIN . $local_array['fullpath'];
									}
								} else {
									$order_copy["values"]["cart"]["products"][$product_id]["main_pair"]["detailed"]["image_path"] = $product_image;
								}
								
								$order_copy["values"]["cart"]["products"][$product_id]["product_options_info"] = $product_options;
								$order_copy["values"]["cart"]["products"][$product_id]["amount"] = $product_qty;
							} elseif ($data_type == 'delete_product') {
								unset($order_copy["values"]["cart"]["products"][$product_id]);
							}
						} else {
							$this->ajaxReturn(array("status"=>500, "info"=>"产品实例无法对应！"));
						}
						break;
					case 'magento':
						if (!empty($order_copy["values"]["product"])) {
							$products = array();
							foreach ($order_copy["values"]["product"] as $key => $value) {
								if ($value["item_id"] == $product_id) {
									if ($data_type == 'modify_product') {
										if ($order_copy["values"]["product"][$key]['product_image'] != $product_image) {
											$local_array = local_image($product_image);
											if (!empty($local_array)) {
												$order_copy["values"]["product"][$key]['product_image'] = APP_DOMAIN . $local_array['fullpath'];
											}
										} else {
											$order_copy["values"]["product"][$key]['product_image'] = $product_image;
										}
										
										$order_copy["values"]["product"][$key]["product_options"]['options'] = $product_options;
										$order_copy["values"]["product"][$key]['qty_ordered'] = $product_qty;
									} elseif ($data_type == 'delete_product') {
										unset($order_copy["values"]["product"][$key]);
									}
									$find = true;
								}
							}
							if (!isset($find)) {
								$this->ajaxReturn(array("status"=>500, "info"=>"产品实例无法对应！"));
							}
						}
						break;
					case 'wordpress':
						if (!empty($order_copy["values"]["order_info"]["cart_items"])) {
							$products = array();
							foreach ($order_copy["values"]["order_info"]["cart_items"] as $key => $value) {
								if ($value["product_id"] == $product_id) {
									if ($data_type == 'modify_product') {
										if ($order_copy["values"]["order_info"]["cart_items"][$key]["product_image"] != $product_image) {
											$local_array = local_image($product_image);
											if (!empty($local_array)) {
												$order_copy["values"]["order_info"]["cart_items"][$key]["product_image"] = APP_DOMAIN . $local_array['fullpath'];
											}
										} else {
											$order_copy["values"]["order_info"]["cart_items"][$key]["product_image"] = $product_image;
										}
										
										$order_copy["values"]["order_info"]["cart_items"][$key]["product_options_info"] = $product_options;
										$order_copy["values"]["order_info"]["cart_items"][$key]['quantity'] = $product_qty;
									} elseif ($data_type == 'delete_product') {
										unset($order_copy["values"]["order_info"]["cart_items"][$key]);
									}
									$find = true;
								}
							}
							if (!isset($find)) {
								$this->ajaxReturn(array("status"=>500, "info"=>"产品实例无法对应！"));
							}
						}
						break;
					case 'zencart':
						if (!empty($order_copy["values"]["products"])) {
							$products = array();
							foreach ($order_copy["values"]["products"] as $key => $value) {
								if ($value["id"] == $product_id) {
									if ($data_type == 'modify_product') {
										if ($order_copy["values"]["products"][$key]["product_image"] != $product_image) {
											$local_array = local_image($product_image);
											if (!empty($local_array)) {
												$order_copy["values"]["products"][$key]["product_image"] = APP_DOMAIN . $local_array['fullpath'];
											}
										} else {
											$order_copy["values"]["products"][$key]["product_image"] = $product_image;
										}
										
										$order_copy["values"]["products"][$key]["product_options_info"] = $product_options;
										$order_copy["values"]["products"][$key]['qty'] = $product_qty;
									} elseif ($data_type == 'delete_product') {
										unset($order_copy["values"]["products"][$key]);
									}
									$find = true;
								}
							}
							if (!isset($find)) {
								$this->ajaxReturn(array("status"=>500, "info"=>"产品实例无法对应！"));
							}
						}
						break;
					default:
						$this->ajaxReturn(array("status"=>500, "info"=>"非法的订单类型！"));
						break;
				}

				$new_values = json_encode($order_copy["values"]);
				$result = $obj_orders->where(array("id"=>$order_id))->setField("values", $new_values);
				if ($result !== false) {
					$history_value = json_encode($old_order);
					$history = array("order_id"=>$order_id, "uid"=>$this->user_relation["id"], "action_type"=>$data_type, "value"=>$history_value);
					$obj_order_history->add($history);
					$this->ajaxReturn(array("status"=>200, "info"=>"success"));
				} else {
					$this->ajaxReturn(array("status"=>500, "info"=>"数据保存失败！"));
				}
			} else {
				$this->ajaxReturn(array("status"=>500, "info"=>"订单实例无法对应！"));
			}
		} else {
			$this->ajaxReturn(array("status"=>500, "info"=>"参数不完整！"));
		}
	}

	public function mark_order_stockout() {
		$order_id = I("post.order_id", 0, 'intval');
		$obj_orders = M("orders");
		if (!empty($order_id)) {
			//更改订单状态
			$order_info = $obj_orders->field('values', true)->find($order_id);
			$result = $obj_orders->where(array('id'=>$order_id))->setField('order_status', 'stockout');
			if ($result !== false) {
				//写入状态更改日志
				$obj_logs = M("logs");
				$log_data = array(
						'uid'=>$this->user_relation['id'], 'type'=>'modify_status', 'order_id'=>$order_id, 
						'values'=>'stockout', 'timestamp'=>time(), 'ip'=>$_SERVER['REMOTE_ADDR']
				);
				$obj_logs->add($log_data);
				$this->ajaxReturn(array("status"=>200, 'info'=>"success"));
			} else {
				$this->ajaxReturn(array("status"=>500, 'info'=>"订单不存在！"));
			}
		}
	}
	
	public function toggle_order_visible() {
		if (!empty($_POST['order_id']) && !empty($_POST['query'])) {
			$order_id = I("post.order_id", 0, "intval");
			$query = I("post.query", "");
			$toggle_value = I("post.toggle_value", "");
			$field = "variable_id,variable_value";
			$where = array("variable_type"=>"generate_pending_delivery", "variable_key"=>$query);
			$data = M("variables")->field($field)->where($where)->find();
			if (!empty($data)) {
				$value = json_decode($data['variable_value'], true);
				if (is_array($value) && !empty($value) && !empty($value['ids'][$order_id])) {
					$value['ids'][$order_id] = array("visible"=>$toggle_value);
					M("variables")->where(array("variable_id"=>$data['variable_id']))->setField("variable_value", json_encode($value));
					$this->ajaxReturn(array("status"=>200, "info"=>"success"));
				} else {
					$this->ajaxReturn(array("status"=>500, "info"=>"数据不存在！"));
				}
			} else {
				$this->ajaxReturn(array("status"=>500, "info"=>"变量不存在！", "sql"=>M("variable")->getLastSql()));
			}
		}
		$this->ajaxReturn(array("status"=>500, "info"=>"参数错误！"));
	}
}
