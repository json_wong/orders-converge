<?php

class OrderAction extends BaseAction {
	
	private $order;
	
	public function _initialize($auth=true) {
		parent::_initialize();
	}

	public function user_analysis() {
		if (!in_array('view_user_analysis', $this->user_relation['permissions'])) {
			$this->not_found();
		}
		$email_draw = M("email_draw")->field('email,count')->order('`count` DESC')->select();
		$email_operator = array();
		foreach ($email_draw as $key => $value) {
			$email_arr = explode('@', $value['email']);
			if (count($email_arr) == 2) {
				if (isset($email_operator[$email_arr[1]])) {
					$email_operator[$email_arr[1]]++;
				} else {
					$email_operator[$email_arr[1]] = 1;
				}
			}
		}
		arsort($email_operator);
		$this->base_assign();
		$this->assign(array(
			'email_draw'=>$email_draw, 'email_operator'=>$email_operator, 'menu'=>'user_analysis', 'page_title'=>'用户分析'
		));
		$this->display();
	}
	
	public function analysis() {
		if (!in_array('view_orders_analysis', $this->user_relation['permissions'])) {
			$this->not_found();
		}
		
		//查询订单绘制图表
		$order = M("orders");
		$single_day = false;
		//分离当前日期的年、月、日
		list($cy, $cm, $cd) = explode('-', date('Y-m-d'));
		if (!empty($_GET['morris_date_start']) && !empty($_GET['morris_date_end'])) {
			$date_start = strtotime($_GET['morris_date_start'] . ' 00:00:00');//设置起始时间
			$date_end = strtotime($_GET['morris_date_end'] . ' 23:59:59');//设置结束时间
				
			//if the url parameter format error, redirect index page.
			if (!$date_start || !$date_end) {
				U('/', '', false, true, true);
			}
				
			list($sy, $sm, $sd) = explode('-', date('Y-m-d', $date_start)); //从URL中提取开始时间的年、月、日
			list($ey, $em, $ed) = explode('-', date('Y-m-d', $date_end)); //从URL中提取结束时间的年、月、日
				
			//if query data within two days, $single_day variable's value is true.
			if ($date_end - $date_start < 259200) {
				$single_day = true;
			}
		} else {//default date region
			$sy = $ey = $cy;//draw start and end year for current system time
			$sm = $em = $cm;//draw start and end month for current system time
			$date_start = mktime(0, 0, 0, $sm, 1, $sy);//transform unix time for standard start date format
			$date_end = mktime(23, 59, 59, $sm + 1, 0, $ey);//transform unix time for standard end date format
			
			//if today is 1, $single_day variable's value is true.
			if (in_array(date('j'), array(1,2,3))) {
				$single_day = true;
			}
		}
		
		$where = array('timestamp'=>array('between', array($date_start, $date_end)));
		if (!empty($_GET['domain'])) {
			$where['domain'] = $_GET['domain'];
		}
		$where['is_deleted'] = 0;
		//查询订单
		$tmp = $order->field('id,domain,system_type,order_status,timestamp,values')->where($where)->order('`timestamp` ASC')->select();
		$sum_count = count($tmp);//计算总数
		//init some array
		$orders_info = $websites = $pop_products = $order_status = array();
		if (!empty($tmp)) {//if query data not is empty
			$sum_prices = 0;//sum prices
			foreach ($tmp as $val) {
				if ($single_day === false) {
					$keys = date('Y-m-d', $val['timestamp']);
				} else {
					$keys = date('Y-m-d H:00', $val['timestamp']);
				}

				$values = json_decode($val['values'], true);
				if ($values) {
					//draw price by system type
					switch ($values['system_type']) {
						case 'cscart':
							//draw products for cscart order
							if (!empty($values['cart']['products'])) {
								foreach ($values['cart']['products'] as $k=>$v) {
									$beacon = md5($values['domain'] . '_' . $v['product_code'], true);
									if (empty($pop_products[$beacon])) {
										$pop_products[$beacon] = array('count'=>0, 'name'=>'', 'price'=>0);
									}
									$pop_products[$beacon]['count'] += $v['amount'];
									$pop_products[$beacon]['name'] = $v['product_title'];
									$pop_products[$beacon]['url'] = "http://{$values['domain']}/{$v['seo_name']}.html";
                                    $pop_products[$beacon]['price'] += $v['price'] * $v['amount'];
								}
							}
							//sub price
							$total = floatval($values['cart']['total']);
							break;
						case 'magento':
							//draw products for magento order
							if (!empty($values['product'])) {//push buggy magento before order, only a single product
								if (!empty($values['product']['item_id'])) {
									$beacon = md5($values['product']['product_url'] . '_' . $values['product']['sku'], true);
									if (empty($pop_products[$beacon])) {
										$pop_products[$beacon] = array('count'=>0, 'name'=>'', 'price'=>0);
									}
									$pop_products[$beacon]['count'] += $values['product']['qty_ordered'];
									$pop_products[$beacon]['name'] = $values['product']['name'];
									$pop_products[$beacon]['url'] = $values['product']['product_url'];
                                    $pop_products[$beacon]['price'] += $values['price'] * $values['qty_ordered'];
								} else {//loop the products array
									foreach ($values['product'] as $k=>$v) {
										$beacon = md5($v['product_url'] . '_' . $v['sku'], true);
										if (empty($pop_products[$beacon])) {
											$pop_products[$beacon] = array('count'=>0, 'name'=>'', 'price'=>0);
										}
										$pop_products[$beacon]['count'] += $v['qty_ordered'];
										$pop_products[$beacon]['name'] = $v['name'];
										$pop_products[$beacon]['url'] = $v['product_url'];
                                        $pop_products[$beacon]['price'] += $v['price'] * $v['qty_ordered'];
									}
								}
							}
							$total = floatval($values['grand_total']);
							break;
						case 'zencart':
							break;
						case 'wordpress':
							//draw products for cscart order
							if (!empty($values['order_info']['cart_items'])) {
								foreach ($values['order_info']['cart_items'] as $k=>$v) {
									$beacon = md5($values['domain'] . '_' . $v['sku'], true);
									if (empty($pop_products[$beacon])) {
										$pop_products[$beacon] = array('count'=>0, 'name'=>'', 'price'=>0);
									}
									$pop_products[$beacon]['count'] += $v['quantity'];
									$pop_products[$beacon]['name'] = $v['product_name'];
									$pop_products[$beacon]['url'] = "{$values['product_url']}";
                                    $pop_products[$beacon]['price'] += $v['total_price'];
								}
							}
							//sub price
							$total = floatval($values['order_info']['total_price']);
							break;
					}
					//init a array if the order info is empty
					//the pandect
					empty($orders_info[$keys]['count']) && $orders_info[$keys] = array('count'=>0, 'price'=>0);
					$orders_info[$keys]['count']++;
					$orders_info[$keys]['price'] += $total;

                    if (!isset($order_status[$val['order_status']])) {
                        $order_status[$val['order_status']] = array("count"=>0, "price"=>0);
                    }
                    $order_status[$val['order_status']]['count']++;
                    $order_status[$val['order_status']]['price'] += $total;
                    list(
                        $order_status[$val['order_status']]['name'],
                        $btn_class,
                        $order_status[$val['order_status']]['labelclass']
                        ) = get_order_status($val['order_status']);

					//shares of website
					empty($websites[$val['domain']]) && $websites[$val['domain']] = array('count'=>0, 'price'=>0);
					$websites[$val['domain']]['count']++;
					$websites[$val['domain']]['price'] += $total;
					$sum_prices += $total;
					//popular products charts
					if (!empty($pop_products)) {
						$sort_key = array();
						foreach ($pop_products as $key=>$val) {
							$sort_key[$key] = $val['count'];
						}
						array_multisort($sort_key, SORT_DESC, $pop_products);
					}
				}
			}
			//recomputation shares of website
			foreach ($websites as $key=>$val) {
				$websites[$key]['percent_count'] = round(($val['count'] / $sum_count) * 100, 2);
				$websites[$key]['percent_price'] = round(($val['price'] / $sum_prices) * 100, 2);
			}
				
			//check the missing days and array to sort again
			// 			if (!empty($orders_info) && $single_day === false) {
			// 				for ($y=$sy; $y<=$ey; $y++) {
			// 					for ($m=$sm; $m<=$em; $m++) {
			// 						for ($d=date('j', $date_start); $d<=date('j', $date_end); $d++) {
			// 							if ($d < 10) $day = "0{$d}";
			// 							else $day = $d;
			// 							if (checkdate($m, $day, $y)) {
			// 								$date = $y.'-'.$m.'-'.$day;
			// 								if (!isset($orders_info[$date]) && $date <= date('Y-m-d') && $date <= date('Y-m-d', $date_end)) {
			// 									$orders_info[$date] = array('count'=>0, 'price'=>0);
			// 								}
			// 							}
			// 						}
			// 					}
			// 				}
			// 				ksort($orders_info);
			// 			}
		}
		if (empty($websites) && !empty($_GET['domain'])) {
			$websites[$_GET['domain']] = array('price'=>0, 'count'=>0);
		}
		
		//this week time region
		$timestamp = strtotime("{$cy}-{$cm}-{$cd}");
		$weeknum = date('N', $timestamp);
		$w = date("w", $timestamp);
		$d = $w ? $w - 1 : 6;
		// $s_week = date('Y-m-d', strtotime("{$cy}-{$cm}-" . ($cd - ($weeknum - 1))));
		$s_week = date("Y-m-d", strtotime("$date -".$d." days"));
		$e_week = date('Y-m-d', strtotime("{$cy}-{$cm}-" . ($cd + (7 - $weeknum))));
		$this->assign(array(
			'orders_info'=>$orders_info, 'website_info'=>$websites, 'date_start'=>date('Y-m-d', $date_start),
            'single_day'=>$single_day, 'date_end'=>date('Y-m-d', $date_end), 'page_title'=>'订单分析',
            'menu'=>'analysis', 'pop_products'=>array_slice($pop_products, 0, 100), 'sum_count'=>$sum_count,
            'sum_prices'=>$sum_prices, 'today'=>"{$cy}-{$cm}-{$cd}", 'yesterday'=>date('Y-m-d', strtotime("{$cy}-{$cm}-" . --$cd)),
			's_week'=>$s_week, 'e_week'=>$e_week, 's_year'=>"{$cy}-01-01", 'e_year'=>"{$cy}-12-31",
            'order_status' => $order_status
		));
		$this->base_assign();
		$this->display();
	}
	
	
	/**
	 * 订单列表
	 * 
	 * @param array $search 查询条件
	 * @param bool $return_data 是否返回数据，为真时不渲染模版直接返回数组
	 */
	public function lists($search=array(), $return_data=false) {
		if (!in_array('view_orders', $this->user_relation['permissions'])) {
			$data = array();
			$page['show'] = '';
		} else {
			$orders = M('orders');
			$where = array('is_deleted'=>0);
			if (!empty($search)) {
				$where = array_merge($search, $where);
			}
			$count = $orders->where($where)->count('id');
			$page = get_page($count, 20);
			$fields = '`id`,`system_type`,`domain`,`order_id`,`order_status`,`ems_number`,`shipping_method`, `customs_hold`, `first_name`,`last_name`,`email`,`prices`,`timestamp`,`ip`';
			//查询条件包含id in，查询结果按照id队列顺序排序
			if (!empty($where['id']) && $where['id'][0] == 'in') {
				if ($return_data === true) { //返回数据将不分页
					$data = M('orders')->field(true)->where($where)
							->order("find_in_set(id,'{$where['id'][1]}'), `timestamp` DESC, `id` DESC")
							->select();
				} else {
					$data = M('orders')->field(true)->where($where)->page($page['p'], $page['single'])
							->order("find_in_set(id,'{$where['id'][1]}'), `timestamp` DESC, `id` DESC")
							->select();
				}
			} else {
				//返回数据将不分页
				if ($return_data === true) {
					$data = M('orders')->field(true)->where($where)
							->order("`timestamp` DESC, `id` DESC")
							->select();
				} else {
					$data = M('orders')->field(true)->where($where)
							->page($page['p'], $page['single'])
							->order("`timestamp` DESC, `id` DESC")
							->select();
				}
			}
			
			$m_remark = M("order_remark");
			
			foreach ($data as $key=>$val) {
				if (!empty($val['values'])) {
					$data[$key]['values'] = json_decode($val['values'], true);
					$domain = explode('.', $data[$key]['domain']);
					if (count($domain) > 2) {
						array_shift($domain);
					}
					$data[$key]['domain'] = join('.', $domain);
					switch ($data[$key]['values']['system_type']) {
						case 'cscart':
							$data[$key]['total'] = number_format($data[$key]['values']['cart']['total']);
							break;
						case 'magento':
							$data[$key]['total'] = number_format($data[$key]['values']['grand_total']);
							break;
						case 'landingpage':
							$data[$key]['total'] = number_format($data[$key]['prices']);
							break;
					}
					
					if (!empty($val["ems_number"])) {
						$data[$key]["ems_number"] = multi_explode(array('，', ',',' ','　','|','、'), $val["ems_number"]);
					}
// 					if ($val["id"] == 25219) {
// 						var_dump($val["ems_number"]);
// 						var_dump($val["shipping_method"]);
// 					}
				}
				
				//查找订单备注
				$data[$key]['order_remark'] = $m_remark->field(true)->where(array("order_id"=>$val["id"]))->order("pubdate DESC")->find();
				
// 				if (!empty($val['ems_number'])) { //查找物流状态
// 					$where = array('order_id'=>$val['id'], 'ems_number'=>array('in', $val['ems_number']));
// 					$delivery_failure = M("delivery")->field('ems_number')->where($where)->select();
// 					$data[$key]['ems_status'];
// 				}
			}
		}
		$order_data = array_map('overwrite_order_data', $data);
		
		if ($return_data === true) {
			return $order_data;
		}
		
		$this->assign(array(
				'orders'=>$order_data, 'page_title'=>!empty($search) ? '订单搜索' : '订单列表', 'page'=>$page['show'], 
				'menu'=>'orders', 'search_condition'=>$search, 'count'=>$count
		));
		$this->base_assign();
		$this->display('lists');
	}
	
	/**
	 * orders recycle bin
	 */
	public function recycle() {
		if (!in_array('view_recycle', $this->user_relation['permissions'])) {
			$data = array();
			$page['show'] = '';
		} else {
			$orders = M('orders');
			$where = array('is_deleted'=>1);
			$count = $orders->where($where)->count('id');
			$page = get_page($count, 20);
			$data = M('orders')->field(true)->where($where)->page($page['p'], $page['single'])->order("`timestamp` DESC, `id` DESC")->select();
			foreach ($data as $key=>$val) {
				if (!empty($val['values'])) {
					$data[$key]['values'] = json_decode($val['values'], true);
					$domain = explode('.', $data[$key]['domain']);
					array_shift($domain);
					$data[$key]['domain'] = join('.', $domain);
					switch ($data[$key]['values']['system_type']) {
						case 'cscart':
							$data[$key]['total'] = number_format($data[$key]['values']['cart']['total']);
							break;
						case 'magento':
							$data[$key]['total'] = number_format($data[$key]['values']['grand_total']);
							break;
					}
				}
			}
		}
		$order_data = array_map('overwrite_order_data', $data);
		$this->assign(array(
				'orders'=>$order_data, 'page_title'=>'订单回收站', 'page'=>$page['show'],
				'menu'=>'recycle', 'count'=>$count
		));
		$this->base_assign();
		$this->display('lists');
	}
	
	/**
	 * order recover
	 */
	public function recover() {
		if (!empty($this->url[2]) && is_numeric($this->url[2])) {
			if (!in_array('recover_orders', $this->user_relation['permissions'])) {
				$this->not_found();
			}
			$order_id = intval($this->url[2]);
			M("orders")->where(array('id'=>$order_id))->setField('is_deleted', 0);
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
	
	/**
	 * 修改订单
	 */
	public function modify() {
		if (!in_array('modify_orders', $this->user_relation['permissions'])) {
			$this->not_found();
		}
		
		if (!empty($this->url[2]) && is_numeric($this->url[2])) {
			$this->order = M('orders')->field(true)->find($this->url[2]);
			if (!empty($this->order)) {
				$this->order['values'] = json_decode($this->order['values'], true);
				$this->dispost_data($this->order['system_type']);
				$vo = array_map('overwrite_order_data', array($this->order));
				$this->order = $vo[0];
				$this->assign(array('vo'=>$this->order, 'page_title'=>'订单修改', 'menu'=>'orders'));
				$this->base_assign();
		
				$order_remark = M("order_remark")->field(true)->where(array("order_id"=>$this->url[2]))->order("pubdate DESC")->select();
				$this->assign("order_remark", $order_remark);
				$this->display('update');
			} else {
				$this->not_found();
			}
		} else {
			$this->not_found();
		}
	}
	
	/**
	 * order detail page
	 */
	public function detail() {
		if (!in_array('view_orders', $this->user_relation['permissions'])) {
			$this->not_found();
		}
		
		if (!empty($this->url[2]) && is_numeric($this->url[2])) {
			$history_pubdate = 0;
			$order_id = intval($this->url[2]);
			$this->order = M('orders')->field(true)->find($order_id);
			if (!empty($this->order)) {//打印历史记录
				if (!empty($_GET['history'])) {
					$times = I("get.history");
					$history_pubdate = date("Y-m-d H:i:s", $times);
					$data = M("order_history")->where(array("order_id"=>$order_id, 'pubdate'=>$history_pubdate))->getField('value');
					if (!empty($data)) {
						$page_title = " （存储于 {$history_pubdate} 的历史数据副本）";
						$this->order = json_decode($data, true);
					} else {
						$page_title = " （历史记录数据不存在，显示最新数据）";
					}
				} else {
					$this->order['values'] = json_decode($this->order['values'], true);
				}
				$this->dispost_data($this->order['system_type']);
				$vo = array_map('overwrite_order_data', array($this->order));
				$this->order = $vo[0];
				$this->assign(array('vo'=>$this->order, 'page_title'=>'订单详情' . $page_title, 'menu'=>'orders'));
				$this->base_assign();
				$temp = 'detail';
				switch ($this->order['system_type']) {
					case 'magento':$temp = 'magento_detail';break;
					case 'landingpage':$temp = 'landingpage_detail';break;
					case 'wordpress':$temp = 'wordpress_detail';break;
					case 'zencart':$temp = 'zencart_detail';break;
					case 'opencart':$temp = 'opencart_detail';break;
                    case 'ccshop':$temp = 'ccshop_detail';break;
				}
				//订单备注
				$order_remark = M("order_remark")->field(true)->where(array("order_id"=>$order_id))->order("pubdate DESC")->select();
				//订单历史记录
				$order_history = M("order_history")->field("pubdate")->where(array("order_id"=>$order_id))->order("pubdate DESC")->select();
				$this->assign(array("order_remark"=>$order_remark, "order_history"=>$order_history, 'history_pubdate'=>$history_pubdate));
				$this->display($temp);
			} else {
				$this->not_found();
			}
		} else {
			$this->not_found();
		}
	}
	
	public function search() {
		if (!in_array('advanced_search', $this->user_relation['permissions'])) {
			$this->not_found();
		}
		
		if ($this->isPost()) {
			unset($_POST['hash']);
			foreach ($_POST as $key=>$val) {
				if (empty($val)) {
					unset($_POST[$key]);
				}
			}
			if (!empty($_POST['start_date'])) {
				$_POST['start_date'] = strtotime($_POST['start_date'] . ' 00:00:00');
			}
			if (!empty($_POST['end_date'])) {
				$_POST['end_date'] = strtotime($_POST['end_date'] . ' 23:59:59');
			}
			if (!empty($_POST)) {
				redirect(__SELF__ . '?' . http_build_query($_POST));
			} else {
				redirect(APP_DOMAIN);
			}
		} else {
			$condition = array();
			$fields = array('cid', 'wid', 'email', 'remark', 'uname', 'address', 'project_name', 'order_id', 'order_status', 'shipping_method', 'prices', 'domain', 'ip', 'customs_hold');
			foreach ($fields as $val) {
				if (!empty($_GET[$val])) {
					$condition[$val] = $_GET[$val];
				}
			}
			
			//package search condition of user's name
			if (!empty($condition['uname'])) {
				$condition['first_name|last_name'] = array('LIKE', '%' . $condition['uname'] . '%');
			}

			if (!empty($condition['address'])) {
				$condition['address'] = urldecode($condition['address']);
				$address = addslashes(str_replace('"', "", json_encode($condition['address'])));
				$condition['values'] = array('LIKE', '%' . $address . '%');
			}

			if (!empty($condition['project_name'])) {
				$condition['address'] = urldecode($condition['address']);
				$project_name = addslashes(str_replace('"', "", json_encode($condition['project_name'])));
				$condition['values'] = array('LIKE', '%' . $project_name . '%');
			}
			
			if (!empty($condition['prices'])) {
				$condition['display_prices'] = $condition['prices'];
				if (strpos($condition['prices'], '-') > 0) {
					$prices_arr = explode('-', $condition['prices']);
					$condition['prices'] = array('between', $prices_arr);
				} else {
					$condition['prices'] = array('LIKE', '%' . $condition['prices'] . '%');
				}
			}
			
			//package search condition of date region
			if (!empty($_GET['start_date']) && !empty($_GET['end_date'])) {
				$condition['start_date'] = $_GET['start_date'];
				$condition['end_date'] = $_GET['end_date'];
				$condition['timestamp'] = array('between', array($_GET['start_date'], $_GET['end_date']));
			} else {
				if (!empty($_GET['start_date'])) {
					$condition['start_date'] = $_GET['start_date'];
					$condition['timestamp'] = array('gt', $_GET['start_date']);
				} else if ($_GET['end_date']) {
					$condition['end_date'] = $_GET['end_date'];
					$condition['timestamp'] = array('lt', $_GET['end_date']);
				}
			}
// 			var_dump($condition);exit;
			$this->assign("search_condition", $condition);
			$this->lists($condition);
		}
	}
	
	/**
	 * 海关扣押
	 */
	public function customs_hold() {
		$delivery_data = M("delivery")->field("order_id,checkdate")->where(array("status"=>"failure"))->order("checkdate DESC")->select();
		$order_ids = array();
		$check_date = array();
		foreach ($delivery_data as $key=>$val) {
			$order_ids[] = $val['order_id'];
			$check_date[$val['order_id']] = $val['checkdate'];
		}
		$order_ids = join(",", $order_ids);
		
		$this->assign(array("customs"=>true, "checkdate"=>$check_date));
		$this->lists(array("id"=>array("in", $order_ids), "customs_hold"=>1));
	}
	
	/**
	 * 定时提醒
	 */
	public function beeptimer() {
		$where = array("beeptimer"=>1, 'beeptimer_date'=>array('elt', date('Y-m-d')), 'dispost'=>0);
		$this->beeptimer = M("order_remark")->field("order_id")->where($where)->group("order_id")->select();
		if (!empty($this->beeptimer)) {
			if (count($this->beeptimer) == 1) {
				U("/order/detail/" . $this->beeptimer[0]['order_id'], '', true, true, true);
			}
			$order_ids = array();
			foreach ($this->beeptimer as $val) {
				$order_ids[] = $val['order_id'];
			}
			
			$this->lists(array("id"=>array("in", $order_ids)));
		}
	}
	
	/**
	 * 打印订单
	 */
	public function prints() {
		if (!in_array('view_order_archive', $this->user_relation['permissions'])) {
			$this->not_found();
		}

		$query = I("get.query", date('Y-m-d'));

		$obj_variables = M("variables");
		$where = array("variable_type"=>"generate_pending_delivery");
		$datas = $obj_variables->field("variable_key,variable_value")->where($where)->order("variable_key DESC")->select();
		if (!empty($datas)) {
			$gpds = array();
			foreach ($datas as $key => $value) {
				$gpds[$value['variable_key']] = $value['variable_value'];
			}
		}
		$orders = array();
		if (!empty($query)) {
			if (!empty($gpds[$query])) {
				$value = json_decode($gpds[$query], true);
				if (!empty($value['ids'])) {
					$condition = array('id'=>array('in', join(',', array_keys($value['ids']))));
					$orders = $this->lists($condition, true);
				}
				$gc_hash = $value['gc_hash'];
				$kd_hash = $value['kd_hash'];
			}
		}
		import("ORG.Crypt.Crypt");
		$crypt = new Crypt();
		$date_encode = $crypt->encrypt($query, C("SYSTEM_RANDOM"), true);

		foreach ($orders as $key=>$val) {
			$this->order = $val;
			$this->dispost_data($this->order['system_type']);
			$orders[$key] = $this->order;
		}
		$this->assign(array(
				"orders"=>$orders, 'query'=>$query, 'gpds'=>$gpds, 'gc_hash'=>$gc_hash, 'kd_hash'=>$kd_hash, 
				'date_encode'=>$date_encode, 'ids'=>$value['ids']
		));
		$this->display();
	}
	
	/**
	 * 生成Excel
	 * 
	 * @param arr $orders
	 * @param arr $condition
	 */
	public function generate_excel($orders=array(), $condition=array()) {
		if (!empty($orders)) {
			$uniqid = uniqid("order_");
			$description = "订单导出数据，导出时间：".date('Y-m-d H:i:s') . "。导出条件：" . join(',', $condition) . "。";
			$description = "总数：" . count($orders);
			require LIB_PATH . 'Class/PHPExcel/PHPExcel.php';
			$phpexcel = new PHPExcel();
			$phpexcel->getDefaultStyle()->getFont()->setName('Calibri')->setSize(10);
			$phpexcel->getProperties()->setCreator(APP_NAME)
									  ->setLastModifiedBy(APP_NAME)
									  ->setTitle("订单导出" . $uniqid)
									  ->setSubject("订单导出" . $uniqid)
									  ->setDescription($description);
			
			//header first line
			$phpexcel->setActiveSheetIndex(0)->setCellValue("A1", "客户地址")
											 ->setCellValue("B1", "订单号和客服备注")
											 ->setCellValue("C1", "订购产品");
			
			$j = 1;
			foreach ($orders as $val) {
				$j++;
				$address = "";
				$cols = array("C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");
				$order_id = (!empty($val["prefix"]) ? $val["prefix"] . '-' : '') . $val["order_id"];
				$orderidRichText = $productsRichText = new PHPExcel_RichText();
				$objOrderId = $orderidRichText->createTextRun($order_id);
				$objOrderId->getFont()->setColor( new PHPExcel_Style_Color( PHPExcel_Style_Color::COLOR_RED ) );
				
				if (!empty($val["order_remark"])) {
					$remark_content = "\n\n客服备注：\n";
					$m = 0;
					foreach ($val["order_remark"] as $remark) {
						$m++;
						$remark_content .= $m . "." . $remark["pubdate"] . ' ' . $remark["remark"] . "\n";
					}
					$objRemark = $orderidRichText->createTextRun($remark_content);
					$objRemark->getFont()->setColor( new PHPExcel_Style_Color( PHPExcel_Style_Color::COLOR_RED ) );
				}
				
				$phpexcel->getActiveSheet()->setCellValue("B{$j}", $orderidRichText);
				
				switch ($val["system_type"]) {
					case "cscart":
						$address .= $val["first_name"] . " " . $val["last_name"] . "\n";
						$address .= $val["values"]["user_address"] . "\n";
						if (!empty($val["values"]["user_address_2"])) {
							$address .= $val["values"]["user_address_2"] . "\n";
						}
						$address .= $val["values"]["user_city"] . ", " . $val["values"]["user_state"] . ", " . $val["values"]["user_zipcode"] . "\n";
						$address .= $val["values"]["user_country"] . "\n";
						$address .= "T:" . $val["values"]["user_phone"] . "\n\n";
						$address .= "用户备注：" . (!empty($val["values"]["cart"]["notes"]) ? $val["values"]["cart"]["notes"] : '无');
						
						$phpexcel->getActiveSheet()->setCellValue("A{$j}", $address);
						
						$i = 0;
						foreach ($val["values"]["cart"]["products"] as $product) {
							$product_info = "";
							$product_info .= $product["product_title"] . " \n" . number_format($product["amount"]) . " 件\n";
							$options = array();
							foreach ($product["product_options_info"] as $option) {
								foreach ($option as $opt_k=>$opt_v) {
									$options[] = $opt_k . '： ' . $opt_v;
								}
							}
							$product_info .= join('\n', $options);
							$phpexcel->getActiveSheet()->setCellValue($cols[$i] . $j, $product_info);
							$phpexcel->getActiveSheet()->getStyle($cols[$i] . $j)->getAlignment()->setWrapText(true);
							$phpexcel->getActiveSheet()->getStyle($cols[$i] . $j)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
							
// 							if (!empty($product["main_pair"]["detailed"]["image_path"])) {
// 								$hasimg = false;
// 								$img = "http://" . $val["values"]["domain"] . $product["main_pair"]["detailed"]["image_path"];
// 								$suffix = array_pop(explode(".", basename($product["main_pair"]["detailed"]["image_path"])));
// 								$img_file = DATA_PATH . 'img_tmp/' . md5($img) . ".{$suffix}";
// 								if (!file_exists($img_file)) {
// 									$img_data = file_get_contents($img);
// 									if ($img_data) {
// 										$result = file_put_contents($img_file, $img_data);
// 										if ($result) {
// 											$hasimg = true;
// 										}
// 									}
// 								} else {
// 									$hasimg = true;
// 								}
							
// 								if ($hasimg === true) {
// 									$objDrawing = new PHPExcel_Worksheet_Drawing();
// 									$objDrawing->setName("product_img_" . $cols[$i] . $j);
// 									$objDrawing->setDescription("product_img_" . $cols[$i] . $j);
// 									$objDrawing->setPath($img_file);
// 									$objDrawing->setCoordinates($cols[$i] . $j);
// 									$objDrawing->setOffsetX(100);
// 									$objDrawing->setWidth(120);
// 									$objDrawing->setHeight(120);
// 									$objDrawing->setWorksheet($phpexcel->getActiveSheet());
// 								}
// 							}
							$i++;
						}
						break;
					case "magento":
						$m_address = $val["values"]["shipping_address"];
						$m_address = str_replace(array("<br>", "<br/>", "<br />"), array("\n", "\n", "\n"), $m_address);
						$m_address = str_replace("\n\n", "\n", $m_address);
						$address .= $m_address . "\n\n";
						$address .= "用户备注：" . (!empty($val["values"]["comment"]["customercomment"]) ? $val["values"]["comment"]["customercomment"] : '无');
						if (!empty($val["values"]["comment"]["customerfeedback"])) {
							$address .= "\n郵便局留＆別のお届け先を指定：" . $val["values"]["comment"]["customerfeedback"];
						}
						$phpexcel->getActiveSheet()->setCellValue("A{$j}", $address);
						
						if (!empty($val['values']['product']['item_id'])) {
							$product_info = $val["values"]["product"]["name"] . " \n" . number_format($val["values"]["product"]["qty_ordered"]) . " 件\n";
							$options = array();
							$product_options = $val['values']['product']['product_options'];
							foreach ($product_options['options'] as $value) {
								$options[] = $value['label'] . '：' . $value['value'];
							}
							$product_info .= join('<br/>', $options);
							$phpexcel->getActiveSheet()->setCellValue($cols[0] . $j, $product_info);
							$phpexcel->getActiveSheet()->getStyle($cols[0] . $j)->getAlignment()->setWrapText(true);
							$phpexcel->getActiveSheet()->getStyle($cols[0] . $j)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
						} else {
							$i = 0;
							foreach ($val["values"]["product"] as $product) {
								$product_info = $product["name"] . "\n" . number_format($product["qty_ordered"]) . " 件\n";
								$options = array();
								$product_options = $product['product_options'];
								foreach ($product_options['options'] as $value) {
									$options[] = $value['label'] . '：' . $value['value'];
								}
								$product_info .= join('\n', $options);
								$phpexcel->getActiveSheet()->setCellValue($cols[$i] . $j, $product_info);
								$phpexcel->getActiveSheet()->getStyle($cols[$i] . $j)->getAlignment()->setWrapText(true);
								$phpexcel->getActiveSheet()->getStyle($cols[$i] . $j)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
								$i++;
							}
						}
						break;
					case "zencart":
						$address .= $val["first_name"] . " " . $val["last_name"] . "\n";
						$address .= $val["values"]["customer"]["street_address"] . " " . $val["values"]["customer"]["suburb"] . "\n";
						$address .= $val["values"]["customer"]["city"] . ", " . $val["values"]["customer"]["state"] . ", " . $val["values"]["customer"]["postcode"];
						$address .= $val["values"]["customer"]["country"]["title"] . "\n";
						$address .= "T:" . $val["values"]["customer"]["telephone"] . "\n\n";
						$address .= "用户备注：" . (!empty($val["values"]["info"]["comments"]) ? $val["values"]["info"]["comments"] : '无');
						$phpexcel->getActiveSheet()->setCellValue("A{$j}", $address);
						
						$i = 0;
						foreach ($val["values"]["products"] as $product) {
							$product_info = $product["name"] . " \n" . number_format($product["qty"]) . " 件\n";
							$options = array();
							foreach ($product['product_options_info'] as $value) {
								foreach ($value as $op_k => $op_v) {
									$options[] = $op_k . '：' . $op_v;
								}
							}
							$product_info .= join('\n', $options);
							$phpexcel->getActiveSheet()->setCellValue($cols[$i] . $j, $product_info);
							$phpexcel->getActiveSheet()->getStyle($cols[$i] . $j)->getAlignment()->setWrapText(true);
							$phpexcel->getActiveSheet()->getStyle($cols[$i] . $j)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
							$i++;
						}
						break;
					case "wordpress":
						$address .= $val["first_name"] . " " . $val["last_name"] . "\n";
						$address .= $val["values"]["user_info"][4] . " " . $val["values"]["user_info"][5] . "\n";
						$address .= $val["values"]["user_info"][6] . ", " . $val["values"]["user_info"][8] . "\n";
						$address .= $val["values"]["user_info"][7][0] . "\n";
						$address .= "T:" . $val["values"]["user_info"][18];
						$phpexcel->getActiveSheet()->setCellValue("A{$j}", $address);
						
						$i = 0;
						foreach ($val["values"]["order_info"]["cart_items"] as $product) {
							$product_info = $product["product_name"] . "\n" . number_format($product["quantity"]) . " 件\n";
							$options = array();
							foreach ($product["product_options_info"] as $value) {
								foreach ($value as $op_k => $op_v) {
									$options[] = $op_k . '：' . $op_v;
								}
							}
							$product_info .= join('\n', $options);
							$phpexcel->getActiveSheet()->setCellValue($cols[$i] . $j, $product_info);
							$phpexcel->getActiveSheet()->getStyle($cols[$i] . $j)->getAlignment()->setWrapText(true);
							$phpexcel->getActiveSheet()->getStyle($cols[$i] . $j)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
							$i++;
						}
						break;
				}
				
				$phpexcel->getActiveSheet()->getStyle("A{$j}")->getAlignment()->setWrapText(true);
				$phpexcel->getActiveSheet()->getStyle("A{$j}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
				$phpexcel->getActiveSheet()->getStyle("B{$j}")->getAlignment()->setWrapText(true);
				$phpexcel->getActiveSheet()->getStyle("B{$j}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			}
			//auto width
			$phpexcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
			$phpexcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
			foreach ($cols as $val) {
				$phpexcel->getActiveSheet()->getColumnDimension($val)->setAutoSize(true);
			}
			
			$phpexcel->getActiveSheet()->setTitle("导出订单");
			$excel_writer = PHPExcel_IOFactory::createWriter($phpexcel, 'Excel2007');
			$filename = "export_orders_" . date("YmdHis") . ".xlsx";
			$excel_writer->save(ENTRY_PATH . '/_export_orders_/' . $filename);
			
			$this->ajaxReturn(array("status"=>200, 'url'=>$filename));
			
			// Redirect output to a client’s web browser (Excel2007)
// 			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
// 			header('Content-Disposition: attachment;filename="'.$filename.'"');
// 			header('Cache-Control: max-age=0');
// 			// If you're serving to IE 9, then the following may be needed
// 			header('Cache-Control: max-age=1');
			
// 			// If you're serving to IE over SSL, then the following may be needed
// 			header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
// 			header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
// 			header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
// 			header ('Pragma: public'); // HTTP/1.0
			
// 			$excel_writer->save('php://output');
		}
	}
	
	public function dispost_data($system='cscart') {
		switch ($system) {
			case 'cscart':
				if (!is_array($this->order['values'])) {
					$this->order['values'] = json_decode($this->order['values'], true);
				}
	
				if ($this->order['values'] === false) {
					return false;
				}
	
				//draw user's phone
				if (!empty($this->order['values']['cart']['user_data']['s_phone'])) {
					$this->order['values']['user_phone'] = $this->order['values']['cart']['user_data']['s_phone'];
				} else if (!empty($this->order['values']['cart']['user_data']['phone'])) {
					$this->order['values']['user_phone'] = $this->order['values']['cart']['user_data']['phone'];
				}
	
				//draw 都道府県
				if (!empty($this->order['values']['cart']['user_data']['s_state'])) {
					$this->order['values']['user_state'] = $this->order['values']['cart']['user_data']['s_state'];
				} else if (!empty($this->order['values']['cart']['user_data']['b_state'])) {
					$this->order['values']['user_state'] = $this->order['values']['cart']['user_data']['b_state'];
				}
	
				//draw 市区町村
				if (!empty($this->order['values']['cart']['user_data']['b_city'])) {
					$this->order['values']['user_city'] = $this->order['values']['cart']['user_data']['s_city'];
				} else if (!empty($this->order['values']['cart']['user_data']['s_city'])) {
					$this->order['values']['user_city'] = $this->order['values']['cart']['user_data']['b_city'];
				}
	
				//draw ビル・建物名
				if (!empty($this->order['values']['cart']['user_data']['b_address_2'])) {
					$this->order['values']['user_address_2'] = $this->order['values']['cart']['user_data']['s_address_2'];
				} else if (!empty($this->order['values']['cart']['user_data']['s_address_2'])) {
					$this->order['values']['user_address_2'] = $this->order['values']['cart']['user_data']['b_address_2'];
				}
	
				//draw 番地
				if (!empty($this->order['values']['cart']['user_data']['b_address'])) {
					$this->order['values']['user_address'] = $this->order['values']['cart']['user_data']['s_address'];
				} else if (!empty($this->order['values']['cart']['user_data']['s_address'])) {
					$this->order['values']['user_address'] = $this->order['values']['cart']['user_data']['b_address'];
				}
	
				//draw 郵便番号
				if (!empty($this->order['values']['cart']['user_data']['b_zipcode'])) {
					$this->order['values']['user_zipcode'] = $this->order['values']['cart']['user_data']['s_zipcode'];
				} else if (!empty($this->order['values']['cart']['user_data']['s_zipcode'])) {
					$this->order['values']['user_zipcode'] = $this->order['values']['cart']['user_data']['b_zipcode'];
				}
	
				//draw 国名
				if (!empty($this->order['values']['cart']['user_data']['b_country'])) {
					$this->order['values']['user_country'] = $this->order['values']['cart']['user_data']['s_country'];
				} else if (!empty($this->order['values']['cart']['user_data']['s_country'])) {
					$this->order['values']['user_country'] = $this->order['values']['cart']['user_data']['b_country'];
				}
	
				if ($this->order['values']['user_country'] == 'JP') {
					$this->order['values']['user_country'] = 'Japan';
				}
				break;
		}
	}
}