<?php

use Mailgun\Mailgun;

class BaseAction extends Action {
    
    /* url parameters */
    protected $url;
    
    /* product's domain */
    protected $domain;
    
    /* user relation info */
    protected $user_relation;
    
    /* don't need to verify the login operation */
    protected $except_verified_login = array (
            'login',
            'logout',
            'testpass'
    );
    
    /* paging configuration */
    protected $cfg_page;
    
    /* order status */
    protected $activate_order_status;

    protected $mailgun_key = "key-1l8ojgn4fo1tygwsb38xl13ltdhtqyi2";
    protected $mailgun_pubkey = "pubkey-3yk1v8hlgz7pljqrjulasri4uxf325u5";
    
    protected $beeptimer = array();
    
    protected $not_send_mail_status;
    protected $not_send_mail_status_key;
    
    /**
     * base class constructor
     *
     */
    public function _initialize() {
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        header('Pragma: no-cache');
        header('Expires: -1');
        
        $this->check_login();
    
        if (!empty($_GET["_URL_"]))
            $this->url = $_GET ["_URL_"];
        
        $this->domain = APP_DOMAIN;
        
        $this->activate_order_status = array('N', 'O', 'Q', 'C', 'P', 'I', 'T', 'R', 'press', 'ready', 'wait', 'stockout');
        
        $this->not_send_mail_status = array("待发货", "等货", "缺货");
        $this->not_send_mail_status_key = array("ready", "wait", "stockout");
    }
    
    public function base_assign() {
        $website = array();
        $sites = M("website")->field('id,webname,prefix,weburl,system')->where(array('is_lock'=>0))->select();
        if (!empty($sites)) {
            foreach ($sites as $key=>$val) {
                $website[$val['system']][] = $val;
            }
        }
        
        //网站分类
        $webcate = M("webcate")->field("cid,cate_name")->select();
        
        //海关扣押
        $customs_hold = M("orders")->where(array('customs_hold'=>1))->count("id");
        //缺货
        $stockout = M("orders")->where(array('order_status'=>'stockout', 'is_deleted'=>0))->count("id");
        //邮件发送失败
        $failure_mail = M("mail_list")->where(array('status'=>'failure'))->count("id");
        //收件箱总数
        $inbox_count = M("mail_inbox")->where(array("is_deleted"=>0))->count("id");
        //定时提醒
        if (!empty($this->beeptimer)) {
            $beeptimer = $this->beeptimer;
        } else {
            $where = array("beeptimer"=>1, 'beeptimer_date'=>array('elt', date('Y-m-d')), 'dispost'=>0);
            $currentdate = date('Y-m-d');
            $beeptimer = M("order_remark")->where($where)->group("order_id")->field("remark_id")->select();
        }
        
        $this->assign(array(
                'home_url'=>APP_DOMAIN,
                'app_name'=>APP_NAME,
                'webcate'=>$webcate,
                'website'=>$website, 'inbox_count'=>$inbox_count,
                'username'=>$this->user_relation['username'],
                'customs_hold'=>$customs_hold,
                'failure_mail'=>$failure_mail, 'beeptimer'=>count($beeptimer),
                'not_send_mail_status'=>json_encode($this->not_send_mail_status),
                'stockout'=>$stockout
        ));
    }
    
    /**
     * verify the user is logged in
     *
     */
    public function check_login() {
        if (! in_array ( ACTION_NAME, $this->except_verified_login )) {
            //var_dump($_SESSION);exit;
            //SESSION login
            if (!empty ( $_SESSION ['user'] )) {
                $this->user_relation = json_decode ( $_SESSION ['user'], true );
                if ($this->user_relation === false) {
                    $this->error ( '提取用户信息出错，请尝试重新登录！', U('/account/login') );
                }
                $this->user_relation['permissions'] = json_decode(get_value($this->user_relation['id'], 'permissions'), true);
                $this->assign('profile', $this->user_relation);
                return true;
            } else if(!empty ( $_COOKIE [C('COOKIE_PREFIX') . 'user'] )) {
                //COOKIE login
                $cookie = safe_cookie('user');
                if (!empty($cookie['uid']) && !empty($cookie['u']) && !empty($cookie['p'])) {
                    $user = D ( 'User' );
                    $aid = $user->field('id')->where(array('id'=>$cookie['uid'], 'username'=>$cookie['u'], 'userpass'=>$cookie['p']))->find();
                    
                    if (!empty($aid)) {
                        import ( "ORG.Util.Session" );
                        $user->where ( array ('id' => $cookie['uid']) )->save ( array ('last_login' => date ( 'Y-m-d H:i:s', time () )) );
                        $data = $user->get_relation_data (true, $cookie ['uid']);
                        unset ( $data ['userpass'] );
                        Session::set ( "user", json_encode ( $data ) );
                        $this->user_relation = json_decode ( $_SESSION ['user'], true );
                        $this->user_relation['permissions'] = json_decode(get_value($this->user_relation['id'], 'permissions'), true);
                        $this->assign('profile', $data);
                        return;
                    }
                }
            }
            if ($this->isAjax()) {
                $this->ajaxReturn(array('status'=>104, 'info'=>'当前授权已失效，请重新登录！'));
            } else {
                U('/account/login', '', true, true, true);
            }
        }
    }
    
    public function not_found($code=0) {
        if (!empty($_POST['values'])) {
            $values = json_decode($_POST['values'], true);
        }
        $post = !empty($_POST) ? json_encode($_POST) : json_encode(array('uri'=>$_SERVER['REQUEST_URI']));
        $data = array(
                'code'=>intval($code), 
                'times'=>time(), 
                'domain'=>!empty($values['domain']) ? $values['domain'] : $_SERVER['HTTP_HOST'], 
                'order_id'=>!empty($values['order_id']) ? $values['order_id'] : $values['order_info']['order_id'], 
                'post_data'=>$post, 
                'user_agent'=>$_SERVER['HTTP_USER_AGENT']
        );
        M('error_logs')->add($data);
        $this->show('<h1>Not Found</h1><p>The requested URL was not found on this server.</p>');
        exit;
    }
    
    /**
     * calculate the hash key
     * 
     * @param string $url domain
     * @param string $key key
     */
    public function calculate_hash($url='', $key='default') {
        if ($key == 'default') {
            $key = C('SYSTEM_RANDOM');
        }
        $url = json_encode(str_split($url));
        return crypt ( substr ( sha1 ( $url ) . md5 ( $key ), 7, 32 ), '$6$rounds=5000$'.$key.'$');
    }
    
    public function send_mail($data) {
        require dirname(__FILE__) . '/vendor/autoload.php';
        $mg = new Mailgun($this->mailgun_key);

        $domain = !empty($data['sendmail_domain']) ? $data['sendmail_domain'] : $data['domain'];
        $domain_arr = explode(".", $domain);
        if (count($domain_arr) == 3 && $domain_arr[0] == 'www') {
            array_shift($domain_arr);
        }
        $domain = join('.', $domain_arr);
        $fromname = !empty($data['account_name']) ? $data['account_name'] : $data['webname'];
        $data['mail_content'] = str_replace("\n", "<br/>", $data['mail_content']);
        $data['mail_content'] = str_replace(" ", "&nbsp;", $data['mail_content']);
        $result = $mg->sendMessage($domain, array(
                    'from'=>$fromname . ' <postmaster@'.$domain.'>', 'to'=>$data['mailto'], 
                    'subject'=>$data['mail_title'], 'html'=>$data['mail_content'], 'o:dkim'=>'yes',
                    'o:tracking'=>'yes', 'o:tracking-clicks'=>'yes', 'o:tracking-opens'=>'yes'
                ));

        if (!empty($result->http_response_code) && $result->http_response_code == 200) {
            $mg_id = $result->http_response_body->id;
            $mg_id = str_replace(array('<', '>'), array('', ''), $mg_id);
            $mg_message = $result->http_response_body->message;
            $mail_id = M("mail_list")->add(array(
                'order_id'=>$data['order_id'], 'mg_id'=>$mg_id, 'order_status'=>$data['order_status'],
                'title'=>$data['mail_title'], 'content'=>$data['mail_content']
            ));
            if (!empty($mail_id) && $mail_id > 0) {
                M("mail_logs")->add(array(
                    'mail_id'=>$mail_id, 'value'=>$mg_message
                ));
            }
            return true;
        } else {
            return $result->http_response_body->message;
        }
        
        // import("@.Class.PHPMailerAutoload");
        // $mail = new PHPMailer;
        // $mail->CharSet = "UTF-8";
        // $mail->Encoding = "base64";
        // $mail->isSMTP();                                      // Set mailer to use SMTP
        // //$mail->SMTPDebug = 2;
        // //$mail->Debugoutput = 'html';
        // $mail->Host = $data['smtp_host'];  // Specify main and backup server
        // $mail->Port = $data['smtp_port'];
        // $mail->SMTPAuth = $data['smtp_auth'] == 1 ? true : false;                               // Enable SMTP authentication
        // $mail->Username = $data['smtp_account'];                            // SMTP username
        // $mail->Password = $data['smtp_password'];                           // SMTP password
        // $mail->SMTPSecure = $data['auth_type'];                            // Enable encryption, 'ssl' also accepted
        
        // $mail->From = $data['smtp_account'];
        // $mail->FromName = "=?UTF-8?B?" . base64_encode($data['account_name']) . "?=";
        // $mail->addAddress($data['mailto'], '');  // Add a recipient
        
        // $mail->WordWrap = 50;
        // $mail->isHTML(true);
        
        // $mail->Subject = "=?UTF-8?B?" . base64_encode($data['mail_title']) . "?=";
        // $data['mail_content'] = str_replace("\n", "<br/>", $data['mail_content']);
        // $mail->Body = $data['mail_content'];
        
        // $mail->AltBody = $mail->Body;
        
        // if(!$mail->send()) {
        //  return $mail->ErrorInfo;
        // } else {
        //  return true;
        // }
    }

    /**
     * get variable data
     * @param str $type
     * @param str $key
     */
    public function get_variable($type='', $key='') {
        $where = array();
        if (!empty($type)) {
            $where['variable_type'] = $type;
        }

        if (!empty($key)) {
            $where['variable_key'] = $key;
        }

        if (empty($where)) {
            return false;
        }

        return M("variables")->where($where)->getField('variable_value');
    }
    
    /**
     * get order detail info of field
     * 
     * @param string $field detail field
     * @param array $order order data of array
     */
    public function get_order_detail_field($field='', $order='') {
        if (!empty($order['values'])) {
            $values = json_decode($order['values'], true);
            switch ($order['system_type']) {
                case 'magento':
                    switch ($field) {
                        case 'order_shipping':
                            if (!empty($values['shipping_amount'])) {
                                return number_format($values['shipping_amount']);
                            } else {
                                return false;
                            }
                            break;
                        case 'order_products':
                            if (!empty($values['product'])) {
                                $string = "";
                                foreach ($values['product'] as $key=>$val) {
                                    $string .= "ご注文商品：".$val['name']."   ¥".number_format($val['price'])." x ".intval($val['qty_ordered'])."点\n";
                                }
                                return $string;
                            }
                            break;
                    }
                    break;
                case 'cscart':
                    switch ($field) {
                        case 'order_shipping':
                            if (!empty($values['cart']['shipping_cost'])) {
                                return number_format($values['cart']['shipping_cost']);
                            } else {
                                return 0;
                            }
                            break;
                        case 'order_products':
                            if (!empty($values['cart']['products'])) {
                                $string = "";
                                foreach ($values['cart']['products'] as $key=>$val) {
                                    $string .= "ご注文商品：".$val['product_title']."   ¥".number_format($val['price'])." x ".intval($val['amount'])."点\n";
                                }
                                return $string;
                            }
                            break;
                    }
                    break;
                case 'wordpress':
                    switch ($field) {
                        case 'order_shipping':
                            if (!empty($values['order_info']['base_shipping'])) {
                                return number_format($values['order_info']['base_shipping']);
                            } else {
                                return 0;
                            }
                            break;
                        case 'order_products':
                            if (!empty($values['order_info']['cart_items'])) {
                                $string = "";
                                foreach ($values['order_info']['cart_items'] as $key=>$val) {
                                    $string .= "ご注文商品：".$val['product_name']."   ¥".number_format($val['unit_price'])." x ".intval($val['quantity'])."点\n";
                                }
                                return $string;
                            }
                            break;
                    }
                    break;
                case 'ccshop':
                    switch ($field) {
                        case 'order_shipping':
                            if (!empty($values['order']['freight'])) {
                                return number_format($values['order']['freight']);
                            } else {
                                return 0;
                            }
                            break;
                        case 'order_products':
                            if (!empty($values['products'])) {
                                $string = "";
                                foreach ($values['products'] as $key=>$val) {
                                    $string .= "ご注文商品：".$val['name']."   ¥".number_format($val['price'])." x ".intval($val['qty'])."点\n";
                                }
                                return $string;
                            }
                            break;
                    }
                    break;
            }
        }
    }
    
    public function mark_order_p() {
        $order_id = I("post.order_id", 0, 'intval');
        $logistics = I("post.logistics", array());
        $obj_orders = M("orders");
        if (!empty($order_id) && !empty($logistics)) {
            //更改订单状态
            $order_info = $obj_orders->field('values', true)->find($order_id);
            $result = $obj_orders->where(array('id'=>$order_id))->setField('order_status', 'P');
            if ($result !== false) {
                //写入状态更改日志
                $obj_logs = M("logs");
                $log_data = array(
                        'uid'=>$this->user_relation['id'], 'type'=>'modify_status', 'order_id'=>$order_id,
                        'values'=>'P', 'timestamp'=>time(), 'ip'=>$_SERVER['REMOTE_ADDR']
                );
                $obj_logs->add($log_data);

                //写入物流单号
                $deliverys = array();
                foreach ($logistics as $key => $value) {
                    $deliverys[] = array('order_id'=>$order_id, 'ems_number'=>$value, 'shipping_method'=>$order_info["shipping_method"]);
                }
    
                $obj_orders->where(array('id'=>$order_id))->save(array('ems_number'=>join(" ", $logistics), 'customs_hold'=>0));
                M("delivery")->addAll($deliverys);
    
                //发送EMAIL
                $site = M("website")->field('webname,smtp_info')->where(array('weburl'=>$order_info['domain']))->find();
                if (!empty($site)) {
                    $smtp_info = json_decode($site['smtp_info'], true);
                    $mail_temp = $this->get_mail_temp($order_id, "已发货", $smtp_info["account_name"], true, false);
                    if ($mail_temp['status'] == 200) {
                        $mail_temp['mail_content'] = str_replace("***********", join(" ", $logistics), $mail_temp['mail_content']);

                        $mail_info = array(
                                'mailto'=>$order_info['email'], 'mail_title'=>$mail_temp['mail_title'],
                                'mail_content'=>$mail_temp['mail_content'], 'account_name'=>$smtp_info["account_name"],
                                'domain'=>$order_info['domain'], 'webname'=>$site['webname'],
                                'order_id'=>$order_id, 'order_status'=>'P'
                        );
                        $smtp_info = json_decode($site['smtp_info'], true);
                        $smtp_info = !empty($smtp_info) ? $smtp_info : array();
                        $mail_result = $this->send_mail(array_merge($smtp_info, $mail_info));
    
                        if ($mail_result !== true) {//邮件发送失败
                            //写入失败日志
                            $obj_logs->add(array(
                                    'uid'=>$this->user_relation['id'], 'type'=>'sendmail_error', 'order_id'=>$order_id,
                                    'values'=>$mail_result, 'timestamp'=>time(), 'ip'=>$_SERVER['REMOTE_ADDR']
                            ));
                            $obj_orders->where(array('id'=>$order_id))->setField('order_status', $order_info['order_status']);
                            return array('status'=>500, 'info'=>'邮件发送失败：'. $mail_result, 'mail_info'=>$mail_info);
                        } else {
                            //如果成功，将订单备注对应记录改为已处理
                            M("order_remark")->where(array("order_id"=>$order_id))->setField("dispost", 1);
                            return array("status"=>200, 'info'=>"success");
                        }
                    }
                }
            }
        }
    }

    public function get_mail_temp($order_id=0, $des_status='', $account_name='', $return=false, $validate_email=true) {
        if ($order_id > 0) {
            $fields = 'system_type,domain,order_id,first_name,last_name,email,prices,timestamp,values';
            $order = M("orders")->field($fields)->find($order_id);
            if (!empty($order)) {
                if (in_array($des_status, $this->not_send_mail_status)) {
                    if ($return === true) {
                        return array("status"=>200, 'info'=>'success');
                    } else {
                        $this->ajaxReturn(array("status"=>200, 'info'=>'success'));
                    }
                }
                
                $website = M("website")->field('id,cid,prefix,service_email,blank_account')->where(array('weburl'=>$order['domain']))->find();
                $mail_field = '';
                //通过 mailgun API验证邮件地址有效性
                if ($validate_email === true) {
                    $mg_validate_email = $this->validate_email($order['email']);
                } else {
                    $mg_validate_email = true;
                }

                switch ($des_status) {
                    case '已确认':$mail_field='q_mail';break;
                    case '已付款':$mail_field='c_mail';break;
                    case '已发货':$mail_field='p_mail';break;
                    case '已取消':$mail_field='i_mail';break;
                    case '已退款':$mail_field='t_mail';break;
                    case '发送催款邮件':$mail_field='press1_mail';break;
                }
                if (!empty($website) && !empty($mail_field)) {
                    $mail_temp = M("webcate")->where(array('cid'=>$website['cid']))->getField($mail_field);
                    if (!empty($mail_temp) && $mail_temp = json_decode($mail_temp, true)) {
                        $order_numid = $website['prefix'] . '-' . $order['order_id'];
                        $mail_temp['title'] = str_replace('{$order_id}', $order_numid, $mail_temp['title']);
                        $mail_temp['title'] = str_replace('{$site_domain}', $order['domain'], $mail_temp['title']);
                        $mail_temp['title'] = str_replace('{$site_name}', $account_name, $mail_temp['title']);
                        $mail_temp['title'] = str_replace('{$order_price}', number_format($order['prices']), $mail_temp['title']);
                        
                        //判断是否包含Cialis
                        $has_cialis = false;
                        if (strpos($mail_temp['content'], "<!--{bank_start}-->") >= 0) {
                             $values = json_decode($order['values'], true);
                             if (!empty($values['cart']['products'])) {
                                  foreach ($values['cart']['products'] as $key=>$val) {
                                      if (strpos($val['product_title'], "シアリス") !== false || strpos($val['product_title'], "レビトラ") !== false || strpos($val['product_title'], "Cialis") !== false || strpos($val['product_title'], "Levitra") !== false) {
                                          $has_cialis = true;
                                          break;
                                      }
                                  }
                             } else if (!empty($values['product'])) {
                              foreach ($values['product'] as $key=>$val) {
                                  if (strpos($val['name'], "シアリス") !== false || strpos($val['name'], "レビトラ") !== false || strpos($val['name'], "Cialis") !== false || strpos($val['name'], "Levitra") !== false) {
                                      $has_cialis = true;
                                      break;
                                  }
                              }
                             } else if (!empty($values['order_info']['cart_items'])) {
                                  foreach ($values['order_info']['cart_items'] as $key=>$val) {
                                      if (strpos($val['product_name'], "シアリス") !== false || strpos($val['product_name'], "レビトラ") !== false || strpos($val['product_name'], "Cialis") !== false || strpos($val['product_name'], "Levitra") !== false) {
                                          $has_cialis = true;
                                          break;
                                      }
                                  }
                             }

                            $new_bank = '';
                            if (in_array($website["prefix"], array('E03'))) {
                                $new_bank = "名義人：【チャイナプロジェクト株式会社ネットショップ事務局 】\n銀行名：【ジャパンネット銀行】\n支店名： 【すずめ支店（スズメ） 店番号：002】\n口座番号： 【5749430】";
                               if ($has_cialis === true) {
//                                   $new_bank = "銀行名：【三菱東京UFJ銀行】 \n支店名：【福岡支店】\n口座種類：【普通預金】\n口座番号：【 2463469 】\n名義人：【ヤナガワ　マサカズ】";
//                                   $new_bank = "一部の商品が在庫不足のため、商品の発送が遅れております。\n二日間以内にご入金ができないことをご了承くださいますようお願い申し上げます。";
                               }
                                preg_match("/<!--{bank_start}-->(.*)<!--{bank_end}-->/is", $mail_temp['content'], $match);
                                if (!empty($match[1]) && !empty($new_bank)) {
                                    $mail_temp['content'] = str_replace($match[1], $new_bank, $mail_temp['content']);
                                }
                            } else if ($website["id"] == 78) {
                                $new_bank = "銀行名：【ジャパンネット銀行】 \n支店名：【すずめ支店 店番号：002】\n口座種類：【普通預金】\n口座番号：【 5749430 】\n名義人：【チャイナプロジェクト株式会社ネットショップ事務局】";
                                preg_match("/<!--{bank_start}-->(.*)<!--{bank_end}-->/is", $mail_temp['content'], $match);
                                if (!empty($match[1]) && !empty($new_bank)) {
                                    $mail_temp['content'] = str_replace($match[1], $new_bank, $mail_temp['content']);
                                }
                            } else if (in_array($website["prefix"], array("SV"))) {
                                $new_bank = "銀行名：【ジャパンネット銀行】 \n支店名：【すずめ支店 店番号：002】\n口座種類：【普通預金】\n口座番号：【 5735929 】\n名義人：【佐藤 孝徳 （サトウ タカノリ）】";
                                preg_match("/<!--{bank_start}-->(.*)<!--{bank_end}-->/is", $mail_temp['content'], $match);
                                if (!empty($match[1]) && !empty($new_bank)) {
                                    $mail_temp['content'] = str_replace($match[1], $new_bank, $mail_temp['content']);
                                }
                            } else if (in_array($website["prefix"], array("S07", "S06", "S08", "SV04", "S12"))) {
                                $new_bank = "銀行名: 【ゆうちょ銀行】\n支店名: 【七四八店】\n口座番号: 【8122867（普通預金）】\n口座名義人: 【森　晴樹（モリ　ハルキ）】";
                                preg_match("/<!--{bank_start}-->(.*)<!--{bank_end}-->/is", $mail_temp['content'], $match);
                                if (!empty($match[1]) && !empty($new_bank)) {
                                    $mail_temp['content'] = str_replace($match[1], $new_bank, $mail_temp['content']);
                                }
                            } else if ($website["prefix"] == 'IP01') {
                                $new_bank = "銀 行 名：【楽天銀行】\n支 店 名：【第二営業支店】（支店番号252）\n口座種類：【普通預金】\n口座番号：【7150909】\n名 義 人：【株式会社　Ｎｅｔ　Ｍａｒｋｅｔｉｎｇ　ベスト雑貨激安通販ショップ】";
                                preg_match("/<!--{bank_start}-->(.*)<!--{bank_end}-->/is", $mail_temp['content'], $match);
                                if (!empty($match[1]) && !empty($new_bank)) {
                                    $mail_temp['content'] = str_replace($match[1], $new_bank, $mail_temp['content']);
                                }
                            } else if ($website["prefix"] == 'Q30') {
                                $new_bank = "銀行名: 楽天銀行\n支店名: ソナタ支店\n口座番号: 1217950（普通預金）\n口座名義人: 佐藤未輝也 (サトウミキヤ)";
                                preg_match("/<!--{bank_start}-->(.*)<!--{bank_end}-->/is", $mail_temp['content'], $match);
                                if (!empty($match[1]) && !empty($new_bank)) {
                                    $mail_temp['content'] = str_replace($match[1], $new_bank, $mail_temp['content']);
                                }
                            }  else if ($website["prefix"] == 'JF') {
                                $new_bank = "銀行名: 住信ＳＢＩネット銀行\n支店名: リンゴ支店 支店番号:105\n口座番号: 普通5321649\n口座名義人: 清水 紗希(シミズサキ）";
                                preg_match("/<!--{bank_start}-->(.*)<!--{bank_end}-->/is", $mail_temp['content'], $match);
                                if (!empty($match[1]) && !empty($new_bank)) {
                                    $mail_temp['content'] = str_replace($match[1], $new_bank, $mail_temp['content']);
                                }
                            } else if (in_array($website["prefix"], array("S02","S05","S08","S10","S11","S13","S14","S16","S17","S18","S19","S20","S34","S36"))) {
//                                $new_bank = "銀行名：【三菱東京UFJ銀行】 \n支店名：【福岡支店】\n口座種類：【普通預金】\n口座番号：【 2463469 】\n名義人：【ヤナガワ　マサカズ】";
                                preg_match("/<!--{bank_start}-->(.*)<!--{bank_end}-->/is", $mail_temp['content'], $match);
                                if (!empty($match[1]) && !empty($new_bank)) {
                                    $mail_temp['content'] = str_replace($match[1], $new_bank, $mail_temp['content']);
                                }
                            } else if ($website["id"] == 63) {
                                $new_bank = "銀行名：【三菱東京UFJ銀行】 \n支店名：【福岡支店】\n口座種類：【普通預金】\n口座番号：【 2463469 】\n名義人：【ヤナガワ　マサカズ】";
                                preg_match("/<!--{bank_start}-->(.*)<!--{bank_end}-->/is", $mail_temp['content'], $match);
                                if (!empty($match[1]) && !empty($new_bank)) {
                                    $mail_temp['content'] = str_replace($match[1], $new_bank, $mail_temp['content']);
                                }
                            }
                            
                            $mail_temp['content'] = str_replace(array("<!--{bank_start}-->", "<!--{bank_end}-->"), array("", ""), $mail_temp['content']);
                        }

                        if (strpos($mail_temp['content'], "<!--{phone_start}-->") >= 0) {
                            if ($website["prefix"] == 'Y06') {
                                $random = mt_rand(1, 100);
                                if ($random <= 50) {
                                    $new_phone = "03-4578-9099 　03-4579-0712　03-4578-1477";
                                } elseif ($random > 50 && $random <= 80) {
                                    $new_phone = "03-4578-1477 　03-4579-0712　03-4578-9099";
                                } else {
                                    $new_phone = "03-4579-0712 　03-4578-1744　03-4578-1477";
                                }
                                
                                preg_match("/<!--{phone_start}-->(.*)<!--{phone_end}-->/is", $mail_temp['content'], $match);
                                if (!empty($match[1])) {
                                    $mail_temp['content'] = str_replace($match[0], $new_phone, $mail_temp['content']);
                                }
                            } else if (in_array($website["prefix"], array("Q30", "Q20"))) {
                                $random = mt_rand(1, 100);
                                if ($random <= 50) {
                                    $new_phone = "03-4578-1477 　03-4578-1744　03-4579-0712";
                                } elseif ($random > 50 && $random <= 80) {
                                    $new_phone = "03-4578-9099 　03-4578-1477　03-4578-1744";
                                } else {
                                    $new_phone = "03-4578-1744 　03-4578-9099　03-4578-1477";
                                }
                                
                                preg_match("/<!--{phone_start}-->(.*)<!--{phone_end}-->/is", $mail_temp['content'], $match);
                                if (!empty($match[1])) {
                                    $mail_temp['content'] = str_replace($match[0], $new_phone, $mail_temp['content']);
                                }
                            }
                            $mail_temp['content'] = str_replace(array("<!--{phone_start}-->", "<!--{phone_end}-->"), array("", ""), $mail_temp['content']);
                        }

                        if (strpos($mail_temp['content'], "<!--{email_start}-->") >= 0) {
                            if ($website["prefix"] == 'Y06') {
                                $random = mt_rand(1, 100);
                                if ($random <= 50) {
                                    $new_email = "y06service1@epayhk.com";
                                } elseif ($random > 50 && $random <= 80) {
                                    $new_email = "y06service2@epayhk.com";
                                } else {
                                    $new_email = "y06service1@epayhk.com";
                                }
                            } else if ($website["prefix"] == 'Q20') {
                                $random = mt_rand(1, 100);
                                if ($random <= 50) {
                                    $new_email = "q20service1@epayhk.com";
                                } elseif ($random > 50 && $random <= 80) {
                                    $new_email = "q20service2@epayhk.com";
                                } else {
                                    $new_email = "q20service1@epayhk.com";
                                }
                            } else if ($website["prefix"] == 'Q30') {
                                $new_email = "q30service@epayhk.com";
                            } else if ($website['prefix'] == 'JF') {
                                $new_email = "sale@japanfifasport.com";
                            }
                            preg_match("/<!--{email_start}-->(.*)<!--{email_end}-->/is", $mail_temp['content'], $match);
                            if (!empty($match[1])) {
                                $mail_temp['content'] = str_replace($match[0], $new_email, $mail_temp['content']);
                            }
                            $mail_temp['content'] = str_replace(array("<!--{email_start}-->", "<!--{email_end}-->"), array("", ""), $mail_temp['content']);
                        }
                        
                        $mail_temp['content'] = str_replace('{$order_id}', $order_numid, $mail_temp['content']);
                        $mail_temp['content'] = str_replace('{$site_domain}', $order['domain'], $mail_temp['content']);
                        $mail_temp['content'] = str_replace('{$site_name}', $account_name, $mail_temp['content']);
                        $mail_temp['content'] = str_replace('{$order_price}', number_format($order['prices']), $mail_temp['content']);
                        $mail_temp['content'] = str_replace('{$order_date}', date("Y/m/d", $order['timestamp']), $mail_temp['content']);
                        $mail_temp['content'] = str_replace('{$service_email}', $website['service_email'], $mail_temp['content']);
                        $mail_temp['content'] = str_replace('{$blank_account}', $website['blank_account'], $mail_temp['content']);
                        $mail_temp['content'] = str_replace('{$user_name}', $order['first_name'] . ' ' . $order['last_name'], $mail_temp['content']);
                        $mail_temp['content'] = str_replace('{$order_shipping}', $this->get_order_detail_field('order_shipping', $order), $mail_temp['content']);
                        $mail_temp['content'] = str_replace('{$order_products}', $this->get_order_detail_field('order_products', $order), $mail_temp['content']);
                        
                        $return_data = array(
                            'status'=>200, 'mail_title'=>$mail_temp['title'], 'mail_content'=>$mail_temp['content'], "mg_validate_email"=>$mg_validate_email
                        );
                    } else {
                        $return_data = array('status'=>500, 'info'=>'邮件模版无法解析，请手动填写。', "mg_validate_email"=>$mg_validate_email);
                    }
                } else {
                    $return_data = array('status'=>500, 'info'=>'订单状态错误，请手动填写邮件内容。', "mg_validate_email"=>$mg_validate_email);
                }
            } else {
                $return_data = array('status'=>501, 'info'=>'订单不存在，请手动填写邮件内容。');
            }
        } else {
            $return_data = array('status'=>502, 'info'=>'订单ID错误，请手动填写邮件内容。');
        }

        if ($return === true) {
            return $return_data;
        } else {
            $this->ajaxReturn($return_data);
        }
    }
    
    public function validate_email($email="", $return=true) {
        if (!empty($email)) {
            //通过 mailgun API验证邮件地址有效性
            require dirname(__FILE__) . '/vendor/autoload.php';
            $validateAddress = $email;
            $mgClient = new Mailgun($this->mailgun_pubkey);
            $mg_validate_email = $mgClient->get(
                    "address/validate", array('address' => $validateAddress)
            );
            if ($return !== true) {
                $this->ajaxReturn(array("status"=>200, "mg_validate_email"=>$mg_validate_email));
            } else {
                return $mg_validate_email;
            }
        } else {
            if ($return !== true) {
                $this->ajaxReturn(array("status"=>500, "mg_validate_email"=>false));
            } else {
                return false;
            }
        }
    }
}
