<?php

class ApiAction extends BaseAction {
	
	private $action = null;
	private $hash = null;
	private $values = null;
	private $system_type = null;
	
	public function _initialize() {
		switch (true) {
			case empty($_POST['hash']):
			case empty($_SERVER['HTTP_USER_AGENT']):
			case empty($_POST['action']):
			case empty($_POST['values']):
			case $_SERVER['HTTP_USER_AGENT'] !== 'Mozilla/5.0 (Win94; I)':
				$this->not_found('501');
				break;
		}
		
		$this->action = $this->_post('action');
		$this->hash = $_POST['hash'];
	}
	
	public function _empty($name) {
		$this->not_found('500');
	}
	
	public function mark_p() {
		$result = $this->mark_order_p();
		if (!empty($result)) {
			$this->ajaxReturn($result);
		} else {
			$this->ajaxReturn(array("status"=>500, "info"=>"null"));
		}
	}

	public function get_orders() {
		if (!empty($this->hash)) {
			$s = I("post.values", "");
			$referer = I("post.referer", "factory");
			$visible = I("post.visible", "show");
			import("ORG.Crypt.Crypt");
			$crypt = new Crypt();
			$variable_key = $crypt->decrypt($s, C("SYSTEM_RANDOM"), true);
			$order_data = array();
			$where = array("variable_type"=>"generate_pending_delivery", "variable_key"=>$variable_key);
			$exists = M("variables")->field("variable_value")->where($where)->find();
			if (empty($exists)) {
				$this->ajaxReturn(array(
					"status"=>500, "info"=>"auth failure! data is not exists!"
				));
			} else {
				$variable_value = json_decode($exists["variable_value"], true);
				$hash_key = "";
				switch ($referer) {
					case 'factory':$hash_key = "gc_hash";break;
					case 'express':$hash_key = "kd_hash";break;
				}
				if (!empty($variable_value[$hash_key]) && $variable_value[$hash_key] == $this->hash) {
					if (!empty($variable_value['ids'])) {
						$order_ids = array();
						foreach ($variable_value['ids'] as $key=>$val) {
							if ($visible == 'show') {
								if ($val['visible'] != 'show') {
									continue;
								}
							}
							$order_ids[] = $key;
						}
						$where = array(
								'id'=>array('in', join(',', $order_ids)), 'system_type'=>'cscart', 
								"order_status"=>"ready", "is_deleted"=>0
						);
						$data = M('orders')->field(true)->where($where)
							->order("`timestamp` DESC, `id` DESC")
							->select();
						if (!empty($data)) {
							foreach ($data as $key=>$val) {
								if (!empty($val['values'])) {
									$data[$key]['values'] = json_decode($val['values'], true);
									switch ($data[$key]['values']['system_type']) {
										case 'cscart':
											$data[$key]['total'] = number_format($data[$key]['values']['cart']['total']);
											break;
										case 'magento':
											$data[$key]['total'] = number_format($data[$key]['values']['grand_total']);
											break;
										case 'landingpage':
											$data[$key]['total'] = number_format($data[$key]['prices']);
											break;
									}
								}
							}

							$order_data = array_map('overwrite_order_data', $data);
						}
					}
					$this->ajaxReturn(array("status"=>200, "info"=>"auth success!", 'orders'=>$order_data, 'date'=>$variable_key));
				} else {
					$this->ajaxReturn(array("status"=>500, "info"=>"auth failure!"));
				}
			}
		}
	}
	
	public function index() {
		//the hash key format error
		if (strlen($this->hash) !== 118) {
			//$this->not_found('502');
		}
		
// 		if (!empty($_POST['hash_serialize'])) {
// 			$this->hash = unserialize($this->hash);
// 		}
		//try decode serialized data values
        if (!is_array($_POST['values'])) {
            $this->values = json_decode($_POST['values'], true);
            if (empty($this->values)) {
                $this->values = unserialize($_POST['values']);
                if ($this->values === false) {
                    $this->not_found('5021');
                }
            }
        } else {
            $this->values = $_POST['values'];
        }

		//the data values cannot resolve or domain field is empty.
		if (empty($this->values) || empty($this->values['domain'])) {
			$this->not_found('503');
		}

		/** match the data source **/
		preg_match("/^(http:\/\/)?([^\/]+)/i", $this->values['domain'], $matches);
		if (empty($matches[2])) $this->not_found('504');
		$this->values['domain'] = $matches[2];
		//query authorization
		$website = M('website')->field('weburl,system,islock')->where(array('weburl'=>$matches[2]))->find();
		if (empty($website)) $this->not_found('505');
		if (!empty($website['islock'])) $this->not_found('506');
		
		//contrast to submit domain and referer domain are consistent
// 		preg_match("/^(http:\/\/)?([^\/]+)/i", $_SERVER['HTTP_REFERER'], $matches2);
// 		var_dump($_SERVER['HTTP_REFERER']);exit;
// 		if (empty($matches2[2]) || $matches2[2] !== $website['weburl']) $this->not_found('507');
		
		//contrast to submit system type and local database are consistent
		if (strtolower($this->values['type']) !== $website['system']) $this->not_found('508');
		//the hash key cannot match
//		if ($this->calculate_hash($this->values['domain']) != $this->hash) $this->not_found('509');
		
		$this->system_type = $this->values['system_type'] = $website['system'];
		$this->values['domain'] = $website['weburl'];

		switch ($this->action) {
			case 'insert_order':
				$data = $this->dispost_data('insert_order', $this->system_type);
				$result = M('orders')->add($data);
				if ($result > 0) {
					$this->ajaxReturn(array('status'=>200, 'info'=>'success'));
				}
				break;
			case 'change_status':
				$order = M('orders');
				$where = array('domain'=>$this->values['domain'], 'order_id'=>$this->values['order_info']['order_id']);
				$values = M('orders')->where($where)->getField('values');
				if (!empty($values) && $values = json_decode($values, true)) {
					$values['order_status'] = $this->values['status_to'];
				} else {
					$this->ajaxReturn(array('status'=>500, 'info'=>'not_exists_order'));
				}
				$result = $order->where($where)->save(array('order_status'=>$this->values['status_to'], 'values'=>json_encode($values)));
				if ($result !== false) {
					$this->ajaxReturn(array('status'=>200, 'info'=>'success'));
				} else if ($result === 0) {
					$this->ajaxReturn(array('status'=>200, 'info'=>'nothing'));
				} else {
					$this->ajaxReturn(array('status'=>500, 'info'=>'error'));
				}
				break;
		}
	}
	
	public function dispost_data($type='insert_order', $system='cscart') {
		switch ($system) {
			case 'cscart':
				if ($type == 'insert_order') {
					//draw first name
					if (!empty($this->values['cart']['user_data']['firstname'])) {
						$first_name = $this->values['cart']['user_data']['firstname'];
					} else if (!empty($this->values['cart']['user_data']['b_firstname'])) {
						$first_name = $this->values['cart']['user_data']['b_firstname'];
					} else if (!empty($this->values['cart']['user_data']['s_firstname'])) {
						$first_name = $this->values['cart']['user_data']['s_firstname'];
					} else {
						$first_name = '空';
					}
					
					//darw last name
					if (!empty($this->values['cart']['user_data']['lastname'])) {
						$lastname = $this->values['cart']['user_data']['lastname'];
					} else if (!empty($this->values['cart']['user_data']['b_lastname'])) {
						$lastname = $this->values['cart']['user_data']['b_lastname'];
					} else if (!empty($this->values['cart']['user_data']['s_lastname'])) {
						$lastname = $this->values['cart']['user_data']['s_lastname'];
					} else {
						$lastname = '空';
					}
					
					$shipping_method = "EMS";
					if (is_array($this->values["cart"]["chosen_shipping"]) && !empty($this->values["cart"]["chosen_shipping"])) {
						foreach ($this->values["cart"]["chosen_shipping"] as $shipping) {
							if (strpos($shipping["shipping"], "DHL") !== false) {
								$shipping_method = "DHL";
							}
						}
					}
					
					return array(
							'domain'=>$this->values['domain'],
							'order_id'=>$this->values['order_id'],
							'order_status'=>$this->values['order_status'],
							'shipping_method'=>$shipping_method,
							'first_name'=>$first_name,
							'last_name'=>$lastname,
							'email'=>$this->values['cart']['user_data']['email'],
							'prices'=>$this->values['cart']['total'],
							'timestamp'=>time(),
							'values'=>json_encode($this->values),
							'ip'=>$this->values['ip']
					);
				}
				break;
			case 'magento':
				if ($type == 'insert_order') {
					switch ($this->values['status']) {
						case 'pending_payment':$order_status = 'O';break;
						default:$order_status = 'O';break;
					}
					return array(
							'system_type'=>$this->system_type,
							'domain'=>$this->values['domain'],
							'order_id'=>$this->values['increment_id'],
							'order_status'=>$order_status,
							'first_name'=>$this->values['customer_name']['customer_firstname'],
							'last_name'=>$this->values['customer_name']['customer_lastname'],
							'email'=>$this->values['customer_email'],
							'prices'=>$this->values['grand_total'],
							'timestamp'=>time(),
							'values'=>json_encode($this->values),
							'ip'=>$this->values['remote_ip']
					);
				}
				break;
			case 'landingpage':
				if ($type == 'insert_order') {
					return array(
							'system_type'=>$this->system_type,
							'domain'=>$this->values['domain'],
							'order_id'=>$this->values['order_id'],
							'order_status'=>$this->values['order_status'],
							'first_name'=>$this->values['first_name'],
							'last_name'=>$this->values['last_name'],
							'email'=>$this->values['email'],
							'prices'=>$this->values['sum_price'],
							'timestamp'=>time(),
							'values'=>json_encode($this->values),
							'ip'=>$this->values['ip']
					);
				}
				break;
			case 'wordpress':
				if ($type == 'insert_order') {
					return array(
							'system_type'=>$this->system_type,
							'domain'=>$this->values['domain'],
							'order_id'=>$this->values['order_info']['order_id'],
							'order_status'=>'O',
							'first_name'=>$this->values['user_info'][2],
							'last_name'=>$this->values['user_info'][3],
							'email'=>$this->values['user_info'][9],
							'prices'=>$this->values['order_info']['total_price'],
							'timestamp'=>time(),
							'values'=>json_encode($this->values),
							'ip'=>$this->values['user_info']['ip']
					);
				}
				break;
			case 'zencart':
				if ($type == 'insert_order') {
					return array(
							'system_type'=>$this->system_type,
							'domain'=>$this->values['domain'],
							'order_id'=>$this->values['order_number'],
							'order_status'=>'O',
							'first_name'=>$this->values['customer']['firstname'],
							'last_name'=>$this->values['customer']['lastname'],
							'email'=>$this->values['customer']['email_address'],
							'prices'=>$this->values['info']['total'],
							'timestamp'=>time(),
							'values'=>json_encode($this->values),
							'ip'=>$this->values['customers_ip_address']
					);
				}
				break;
			case 'opencart':
				if ($type == 'insert_order') {
					return array(
							'system_type'=>$this->system_type,
							'domain'=>$this->values['domain'],
							'order_id'=>$this->values['order_id'],
							'order_status'=>'O',
							'shipping_method'=>$this->values["shipping_method"],
							'first_name'=>$this->values['shipping_firstname'],
							'last_name'=>$this->values['shipping_lastname'],
							'email'=>$this->values['email'],
							'prices'=>$this->values['total'],
							'timestamp'=>time(),
							'values'=>json_encode($this->values),
							'ip'=>$this->values['ip']
					);
				}
				break;
            case 'ccshop':
                if ($type == 'insert_order') {
                    return array(
                        'system_type'=>$this->system_type,
                        'domain'=>$this->values['domain'],
                        'order_id'=>$this->values['order']['sn'],
                        'order_status'=>'O',
                        'shipping_method'=>$this->values['order']["shipping_method"],
                        'first_name'=>$this->values['order']['firstname'],
                        'last_name'=>$this->values['order']['lastname'],
                        'email'=>$this->values['order']['email'],
                        'prices'=>$this->values['order']['total_price'],
                        'timestamp'=>time(),
                        'values'=>json_encode($this->values),
                        'ip'=>$this->values['order']['ip']
                    );
                }
                break;
		}
	}
}