<?php

class AccountAction extends BaseAction {
	
	private $account_permissions = array();
	
	public function _initialize() {
		parent::_initialize();
	}
	
	public function index() {
		if (!in_array('view_account_list', $this->user_relation['permissions'])) {
			$this->not_found();
		}
		
		$this->account_permissions = array(
				'view_orders'=>'查看订单', 
				'view_orders_analysis'=>'查看订单分析',
				'view_order_price'=>'查看订单价格', 
				'advanced_search'=>'高级搜索', 
				'change_order_status'=>'修改订单状态',
                'send_press_mail' => '发送催款邮件',
				'delete_orders'=>'删除订单',
				'view_webcate'=>'查看网站分类',
				'add_webcate'=>'添加网站分类',
				'modify_webcate'=>'修改网站分类',
				'view_user_analysis'=>'查看用户分析',
				'view_mail_list'=>'查看邮件发件箱',
				'view_mail_in'=>'查看邮件收件箱',
				'modify_orders'=>'修改订单',
				'modify_order_products'=>'修改订单产品',
				'delete_order_products'=>'删除订单产品',
				'view_order_archive'=>'查看发货归档',
				'view_website'=>'查看网站接入',
				'add_website'=>'添加网站接入',
				'get_website_key'=>'获取通信密匙',
				'view_account_list'=>'查看系统账号',
				'create_account'=>'添加新账号',
				'opera_account'=>'禁用其他账号',
				'view_recycle'=>'查看订单回收站',
				'recover_orders'=>'恢复已删除订单'
		);
		
		$account = M('account');
		$data = $account->field('userpass,random', true)->select();
		if (!empty($data)) {
			$account = array();
			foreach ($data as $key=>$val) {
				$this->transition_permissions($data[$key]['permissions']);
			}
		}
		$this->assign(array('page_title'=>'账号管理', 'data'=>$data, 'menu'=>'account', 'account_permissions'=>$this->account_permissions));
		$this->base_assign();
		$this->display();
	}

    public function disable()
    {
        if (empty($this->url[2]) || $this->url[2] == 1) {
            exit('非法操作！');
        }
        M("account")->where(array('id'=>$this->url[2]))->setField("islock", 1);
        redirect("/account");
    }

    public function enable()
    {
        if (empty($this->url[2]) || $this->url[2] == 1) {
            exit('非法操作！');
        }
        M("account")->where(array('id'=>$this->url[2]))->setField("islock", 0);
        redirect("/account");
    }
	
	/**
	 * user login
	 */
	public function login() {
		if ($this->check_login() === true) {
			U('/', '', false, true, true);
		}
		$this->base_assign();
		$this->display();
	}
	
	public function logout() {
		unset($_SESSION['user']);
		U('/account/login', '', true, true, true);
	}
	
	private function transition_permissions(&$permissions='') {
		if (!empty($permissions)) {
			$permissions = json_decode($permissions, true);
			foreach ($permissions as $key=>$val) {
				$permissions[$key] = $this->account_permissions[$val];
			}
		}
	}
}