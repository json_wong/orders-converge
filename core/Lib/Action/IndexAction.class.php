<?php
use Mailgun\Mailgun;
class IndexAction extends BaseAction {
	
	public function _initialize() {
		parent::_initialize();
	}
	
	public function index() {
		//query order to painting run chart
		$order = M("orders");
		//query today orders
		list ($ty, $tm, $td) = explode('-', date('Y-m-d'));
		$start_times = strtotime(date("{$ty}-{$tm}-{$td} 00:00:00"));
		$end_times = strtotime(date("{$ty}-{$tm}-{$td} 23:59:59"));
		$fields = 'id,domain,order_id,order_status,first_name,last_name,prices,timestamp,values';
		$where = array('timestamp'=>array('between', array($start_times, $end_times)), 'is_deleted'=>0);
		$today_orders = $order->field($fields)->where($where)->order("`timestamp` DESC, `id` DESC")->select();
		$today_prices = 0.00;
		if (!empty($today_orders)) {
			foreach ($today_orders as $val) {
				$today_prices += floatval($val['prices']);
			}
		}
		$today_orders = array_map('overwrite_order_data', $today_orders);
		//query yesterday orders
		$yd = $td - 1;
		$y_date = "{$ty}-{$tm}-{$yd}";
		$where = array('timestamp'=>array('between', array(strtotime($y_date . ' 00:00:00'), strtotime($y_date . ' 23:59:59'))), 'is_deleted'=>0);
		$yesterday_orders = $order->field('prices,timestamp')->where($where)->order("`timestamp` DESC, `id` DESC")->select();
		$yesterday_prices = 0.00;
		if (!empty($yesterday_orders)) {
			foreach ($yesterday_orders as $val) {
				$yesterday_prices += floatval($val['prices']);
			}
		}
		$this->assign(array(
				'page_title'=>'Dashboard', 'menu'=>'home', 'today_count'=>count($today_orders), 'today_prices'=>$today_prices,
				'yesterday_count'=>count($yesterday_orders), 'yesterday_prices'=>$yesterday_prices, 'today_orders'=>$today_orders
		));
		$this->base_assign();
		$this->display();
	}

	public function test() {
		$where = array("wid"=>28, "timestamp"=>array("between", array(1398873600, 1401551999)), 'order_status'=>array('in', "C,P"), 'is_deleted'=>0);
		$order = M("orders")->field("first_name,last_name,email")->where($where)->select();
		echo "<pre>";
		print_r($order);
		echo "</pre>";
	}
	
	public function mg() {
// 		require dirname(__FILE__) . '/vendor/autoload.php';
// 		$mgClient = new Mailgun('key-1l8ojgn4fo1tygwsb38xl13ltdhtqyi2');
// 		$domain = 'clvmall.com';
// 		$queryString = array('event' => 'opened','limit'=>2);
// 		$result = $mgClient->get("$domain/events", $queryString);
// 		echo "<pre>";
// 		print_r($result);
// 		echo "</pre>";
	}

	public function testmail() {
//        $order = M("orders")->field('email')->where(["email" => ['neq', '']])->group('email')->select();
//        var_dump(count($order));
//        $emails = [];
//        foreach ($order as $item) {
//            $emails[] = $item['email'];
//        }
//
//        file_put_contents(RUNTIME_PATH . "emails.txt", join("\n", $emails));
	}

	public function testpass() {
		// var_dump($this->calculate_hash('http://vglove'));
//		var_dump(transition_pass ( "wQ6F7FBcG1mmtW62awYj", "rpAGaVwgTc" ));
	}
}