<?php

class CronAction extends BaseAction {

	public function _initialize() {
		if (!empty($_GET["_URL_"]))
			$this->url = $_GET ["_URL_"];
	}
	
	public function halfhour_working() {
		$this->draw_email();
	}
	
	/**
	 * 归类订单，分配分类及网站ID
	 */
	public function classify_order() {
		$classify_order_last_id = $this->get_variable('classify_order_last_id');
		if (empty($classify_order_last_id)) {
			$classify_order_last_id = 0;
		}
		$m_orders = M("orders");
		$m_website = M("website");
		$fields = 'id,domain';
		$where = array('id'=>array('gt', $classify_order_last_id), 'is_deleted'=>0);
		$orders = $m_orders->field($fields)->where($where)->order("`id` ASC")->select();
		
		if (!empty($orders)) {
			$order_ids = array();
			foreach ($orders as $key => $value) {
				$order_ids[] = intval($value['id']);
				$website = $m_website->where(array('weburl'=>$value['domain']))->find();
				if (!empty($website)) {
					$savedata = array("cid"=>$website["cid"], "wid"=>$website["id"]);
					$m_orders->where(array("id"=>$value["id"]))->save($savedata);
				}
			}
			$last_id = max($order_ids);
			M("variables")->where(array('variable_type'=>'classify_order_last_id'))->setField('variable_value', $last_id);
		}
	}

	/**
	 * 提取用户EMAIL，去重统计
	 */
	public function draw_email() {
		$last_draw_id = $this->get_variable('email_draw_last_id');
		if (empty($last_draw_id)) {
			$last_draw_id = 0;
		}
		$fields = 'id,first_name,last_name,email';
		$where = array('id'=>array('gt', $last_draw_id), 'is_deleted'=>0);
		$order = M("orders")->field($fields)->where($where)->order("`id` ASC")->select();

		if (!empty($order)) {
			$email_draw = M("email_draw");
			foreach ($order as $key => $value) {
				$last_draw_id = $value['id'];
				$order_id = $email_draw->where(array('email'=>$value['email']))->getField('id');
				if (!empty($order_id)) {
					$email_draw->where(array('id'=>$order_id))->setInc('count');
				} else {
					$email_draw->add(array('email'=>$value['email'], 'count'=>1));
				}
			}

			$variable_data = array(
				'variable_value'=>$last_draw_id
			);
			M("variables")->where(array('variable_type'=>'email_draw_last_id'))->setField('variable_value', $last_draw_id);
		}
	}

	public function generate_pending_delivery() {
		$obj_variables = M("variables");
		$where = array("variable_type"=>"current_ready_id", "variable_key"=>"order_id");
		$current_ready_id = get_value($where, "variable_value", $obj_variables);
		if (!empty($current_ready_id)) {
			$order_ids = json_decode($current_ready_id, true);
			if ($order_ids) {
				$zore_key = array_search(0, $order_ids);
				unset($order_ids[$zore_key]);
				$order_ids_data = array();
				$order_ids = array_values($order_ids);
				foreach ($order_ids as $val) {
					$order_ids_data[$val] = array("visible"=>"show");
				}
				$value = array("gc_hash"=>rand_string(10), "kd_hash"=>rand_string(10), "ids"=>$order_ids_data);
				$today_data = array(
					"variable_type"=>"generate_pending_delivery",
					"variable_key"=>date("Y-m-d"),
					"variable_value"=>json_encode($value)
				);
				$obj_variables->add($today_data, array(), true);
				$obj_variables->where(array("variable_type"=>'current_ready_id'))->setField("variable_value", "[0]");
			}
		}

		// $where_order = array("id"=>array("gt", $last_id), "order_status"=>"ready", "is_deleted"=>0);
		// $datas = M("orders")->field("id")->where($where_order)->order("id ASC")->select();
		// if (!empty($datas)) {
		// 	$order_ids = array();
		// 	foreach ($datas as $key => $value) {
		// 		$order_ids[] = $last_id = $value["id"];
		// 	}

		// 	if (!empty($order_ids)) {
		// 		$value = array("hash"=>rand_string(10), "ids"=>$order_ids);
		// 		$today_data = array(
		// 			"variable_type"=>"generate_pending_delivery",
		// 			"variable_key"=>date("Y-m-d"),
		// 			"variable_value"=>json_encode($value)
		// 		);
		// 		$obj_variables->add($today_data, array(), true);
		// 		$obj_variables->where($where)->setField("variable_value", $last_id);
		// 	}
		// }
	}
}