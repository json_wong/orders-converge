<?php

class WebsiteAction extends BaseAction {
	
	public function _initialize() {
		parent::_initialize();
	}
	
	public function index() {
		if (!in_array('view_website', $this->user_relation['permissions'])) {
			$this->not_found();
		}
		$data = M('website')->field(true)->select();
		$data = array_map('analysis_smtp', $data);
		// var_dump($data);exit;
		$webcates = $this->get_webcate();
		$this->assign(array('page_title'=>'网站接入', 'websites'=>$data, 'menu'=>'website', 'webcates'=>$webcates));
		$this->base_assign();
		$this->display();
	}
	
	public function category() {
		if (!in_array('view_webcate', $this->user_relation['permissions'])) {
			$this->not_found();
		}
		$data = M('webcate')->field(true)->select();
		$data = array_map(array(__CLASS__, 'overwrite_cates'), $data);
		$this->assign(array('page_title'=>'网站分类', 'webcates'=>$data, 'menu'=>'webcate'));
		$this->base_assign();
		$this->display();
	}
	
	public function get_webcate() {
		return M('webcate')->field(true)->select();
	}
	
	private function overwrite_cates($data) {
		foreach (array('q', 'c', 'p', 'i', 't', 'r', 'press1', 'press2', 'press3', 'press4', 'press5') as $val) {
			if (!empty($data[$val . '_mail'])) {
				$data[$val . '_mail'] = json_decode($data[$val . '_mail'], true);
			} else {
				$data[$val . '_mail'] = array('title'=>'', 'content'=>'');
			}
		}
		return $data;
	}
}