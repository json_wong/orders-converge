<?php
/**
 * 递归创建目录
 *
 * @param string $path 目录路径
 */
function recursion_mkdir($path) {
	if (! file_exists ( $path )) {
		recursion_mkdir ( dirname ( $path ) );
		mkdir ( $path );
	}
}

/**
 * 加密用户密码
 *
 * @param string $str 原始密码
 * @param string $random 随机码
 * @return string 加密后的密码
 */
function transition_pass($str, $random) {
	return md5 ( substr ( md5 ( $str ) . md5 ( $random ), 7, 32 ) );
}

/**
 * 重新格式化日期
 *
 * @param string $date 标准日期格式字符串
 * @param string $format 格式化格式
 * @return string
 */
function format_date($date, $format = 'Y-m-d') {
	return date ( $format, strtotime ( $date ) );
}

/**
 * 获取列表分页
 *
 * @param int $count 数据总数
 * @param int $single_nums 单页显示多少条
 */
function get_page($count = 0, $single = 20) {
	$cfg_page = array (
			'prev' => "‹",
			'next' => "›",
			'first' => "«",
			'last' => "»",
			'theme' => '<ul>%first% %upPage% %prePage% %linkPage% %nextPage% %downPage% %end%</ul>'
	);
	$p = ceil ( $count / $single );
	import ( "ORG.Util.Page" );
	$page = new Page ( $count, $single );
	$page->rollPage = 20;
	foreach ( $cfg_page as $key => $value ) {
		$page->setConfig ( $key, $value );
	}
	if (!empty ($_GET['p'])) $p = intval($_GET ['p']);
	else $p = 1;
	$p == 0 && $p = 1;
	return array ('single' => $single,'show' => $page->show (),'p' => $p);
}

/**
 * 获取列表分页
 *
 * @param int $count 数据总数
 * @param int $single_nums 单页显示多少条
 */
function get_forward_page($count = 0, $single = 20) {
	
	$cfg_page = array (
			'prev' => "",
			'next' => "",
			'theme' => '%upPage% %linkPage% %downPage%'
	);
	$p = ceil ( $count / $single );
	import ( "@.Class.Page" );
	$page = new Page ( $count, $single );
	foreach ( $cfg_page as $key => $value ) {
		$page->setConfig ( $key, $value );
	}
	if (!empty ($_GET['p'])) $p = intval($_GET ['p']);
	else $p = 1;
	$p == 0 && $p = 1;
	return array ('single' => $single,'show' => $page->show (),'p' => $p);
}


/**
 * 设置或获取安全的cookie内容
 *
 * @param string $name COOKIE名
 * @param string $value COOKIE值
 * @param int $expire 有效期
 * @param string $path 保存路径
 * @param string $domain 有效域名
 * @return Ambigous <boolean, string>
 */
function safe_cookie($name, $value = null, $expire = 3600, $path = '/', $domain = '') {
	import ( "ORG.Util.Cookie" );
	import ( "ORG.Crypt.Base64" );
	if (! empty ( $value )) {
		$randrom = base64_encode ( C ( 'SYSTEM_RANDOM' ) );
		$value = Base64::encrypt ( Base64::encrypt ( json_encode ( $value ), $randrom ), $randrom );
		Cookie::set ( $name, $value, $expire, $path, $domain );
	} else {
		$cookie = Cookie::get ( $name );
		if (! empty ( $cookie )) {
			$randrom = base64_encode ( C ( 'SYSTEM_RANDOM' ) );
			return json_decode ( Base64::decrypt ( Base64::decrypt ( $cookie, $randrom ), $randrom ), true );
		} else {
			return false;
		}
	}
}


/**
 * 根据主键获取某一类数据的字段值，适合查询单一字段值
 * 一般是缓存入口
 * 
 * @param int $id 主键
 * @param str $field 字段名
 * @param str/obj $model 一般为模型类对象或数据库表名
 */
function get_value($id, $field='username', $model='account') {
	if (empty($id) || empty ( $field ) || empty ( $model ))
		return false;
	
	$data = array();
	$object = is_object( $model ) ? $model : M ( $model );
	if (is_array($id)) {
		$data = $object->field ( $field )->where ( $id )->find ();
	} else {
		$id = intval ( $id );
		if ($id > 0) $data = $object->field ( $field )->find ( $id );
	}
	
	if (! empty ( $data [$field] )) return $data [$field];
	else return false;
}


/**
 * 获取URL地址
 *
 * @param int $id ID
 * @param str $name 名称
 */
function get_url($type='', $key=0 ) {
	switch ($type) {
		case 'order_detail':return U('/order/detail/' . $key, '', true, false, true);break;
		case 'order_list':return U('/order/lists/', '', false, false, true);break;
		case 'order_analysis':return U('/order/analysis/', '', false, false, true);break;
		case 'order_recycle':return U('/order/recycle/', '', false, false, true);break;
		case 'order_modify':return U('/order/modify/' . $key, '', false, false, true);break;
	}
}


/**
 * 处理字符串，以便可以正常进行搜索
 * @param unknown_type $string
 * @return mixed
 */
function for_search($string) {
	return str_replace( array('%','_'), array('\%','\_'), $string );
}

/**
 * 正常显示字符串
 * @param unknown_type $string
 * @return mixed
 */
function for_show($string) {
	return preg_replace("/(\015\012)|(\015)|(\012)/", "<br />", preg_replace(array("/&amp;/i", "/&nbsp;/i"), array('&', '&amp;nbsp;'), htmlspecialchars($string, ENT_QUOTES)));
}


function get_real_ip() {
	if(!empty($_SERVER["HTTP_CLIENT_IP"])){
		$ip = $_SERVER["HTTP_CLIENT_IP"];
	}
	if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){//获取代理ip
		$ips = explode(',',$_SERVER['HTTP_X_FORWARDED_FOR']);
	}
	if($ip) $ips = array_unshift($ips,$ip);
	
	$count = count($ips);
	for($i=0;$i<$count;$i++){
		if(!preg_match("/^(10|172\.16|192\.168)\./i",$ips[$i])){//排除局域网ip
			$ip = $ips[$i];
			break;
		}
	}
	$tip = empty($_SERVER['REMOTE_ADDR']) ? $ip : $_SERVER['REMOTE_ADDR'];
	return $tip;
}


/**
 * 字符串截取，支持中文和其他编码
 * @static
 * @access public
 * @param string $str 需要转换的字符串
 * @param string $start 开始位置
 * @param string $length 截取长度
 * @param string $charset 编码格式
 * @param string $suffix 截断显示字符
 * @return string
 */
function msubstr($str, $start=0, $length, $charset="utf-8", $suffix=true) {
	if(function_exists("mb_substr"))
		$slice = mb_substr($str, $start, $length, $charset);
	elseif(function_exists('iconv_substr')) {
		$slice = iconv_substr($str,$start,$length,$charset);
	}else{
		$re['utf-8']   = "/[\x01-\x7f]|[\xc2-\xdf][\x80-\xbf]|[\xe0-\xef][\x80-\xbf]{2}|[\xf0-\xff][\x80-\xbf]{3}/";
		$re['gb2312'] = "/[\x01-\x7f]|[\xb0-\xf7][\xa0-\xfe]/";
		$re['gbk']    = "/[\x01-\x7f]|[\x81-\xfe][\x40-\xfe]/";
		$re['big5']   = "/[\x01-\x7f]|[\x81-\xfe]([\x40-\x7e]|\xa1-\xfe])/";
		preg_match_all($re[$charset], $str, $match);
		$slice = join("",array_slice($match[0], $start, $length));
	}
	return $suffix ? $slice.'...' : $slice;
}



/**
 * 产生随机字串，可用来自动生成密码 默认长度6位 字母和数字混合
 * @param string $len 长度
 * @param string $type 字串类型
 * 0 字母 1 数字 其它 混合
 * @param string $addChars 额外字符
 * @return string
 */
function rand_string($len=6,$type='',$addChars='') {
	$str ='';
	switch($type) {
		case 0:
			$chars='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.$addChars;
			break;
		case 1:
			$chars= str_repeat('0123456789',3);
			break;
		case 2:
			$chars='ABCDEFGHIJKLMNOPQRSTUVWXYZ'.$addChars;
			break;
		case 3:
			$chars='abcdefghijklmnopqrstuvwxyz'.$addChars;
			break;
		case 4:
			$chars = "们以我到他会作时要动国产的一是工就年阶义发成部民可出能方进在了不和有大这主中人上为来分生对于学下级地个用同行面说种过命度革而多子后自社加小机也经力线本电高量长党得实家定深法表着水理化争现所二起政三好十战无农使性前等反体合斗路图把结第里正新开论之物从当两些还天资事队批点育重其思与间内去因件日利相由压员气业代全组数果期导平各基或月毛然如应形想制心样干都向变关问比展那它最及外没看治提五解系林者米群头意只明四道马认次文通但条较克又公孔领军流入接席位情运器并飞原油放立题质指建区验活众很教决特此常石强极土少已根共直团统式转别造切九你取西持总料连任志观调七么山程百报更见必真保热委手改管处己将修支识病象几先老光专什六型具示复安带每东增则完风回南广劳轮科北打积车计给节做务被整联步类集号列温装即毫知轴研单色坚据速防史拉世设达尔场织历花受求传口断况采精金界品判参层止边清至万确究书术状厂须离再目海交权且儿青才证低越际八试规斯近注办布门铁需走议县兵固除般引齿千胜细影济白格效置推空配刀叶率述今选养德话查差半敌始片施响收华觉备名红续均药标记难存测士身紧液派准斤角降维板许破述技消底床田势端感往神便贺村构照容非搞亚磨族火段算适讲按值美态黄易彪服早班麦削信排台声该击素张密害侯草何树肥继右属市严径螺检左页抗苏显苦英快称坏移约巴材省黑武培著河帝仅针怎植京助升王眼她抓含苗副杂普谈围食射源例致酸旧却充足短划剂宣环落首尺波承粉践府鱼随考刻靠够满夫失包住促枝局菌杆周护岩师举曲春元超负砂封换太模贫减阳扬江析亩木言球朝医校古呢稻宋听唯输滑站另卫字鼓刚写刘微略范供阿块某功套友限项余倒卷创律雨让骨远帮初皮播优占死毒圈伟季训控激找叫云互跟裂粮粒母练塞钢顶策双留误础吸阻故寸盾晚丝女散焊功株亲院冷彻弹错散商视艺灭版烈零室轻血倍缺厘泵察绝富城冲喷壤简否柱李望盘磁雄似困巩益洲脱投送奴侧润盖挥距触星松送获兴独官混纪依未突架宽冬章湿偏纹吃执阀矿寨责熟稳夺硬价努翻奇甲预职评读背协损棉侵灰虽矛厚罗泥辟告卵箱掌氧恩爱停曾溶营终纲孟钱待尽俄缩沙退陈讨奋械载胞幼哪剥迫旋征槽倒握担仍呀鲜吧卡粗介钻逐弱脚怕盐末阴丰雾冠丙街莱贝辐肠付吉渗瑞惊顿挤秒悬姆烂森糖圣凹陶词迟蚕亿矩康遵牧遭幅园腔订香肉弟屋敏恢忘编印蜂急拿扩伤飞露核缘游振操央伍域甚迅辉异序免纸夜乡久隶缸夹念兰映沟乙吗儒杀汽磷艰晶插埃燃欢铁补咱芽永瓦倾阵碳演威附牙芽永瓦斜灌欧献顺猪洋腐请透司危括脉宜笑若尾束壮暴企菜穗楚汉愈绿拖牛份染既秋遍锻玉夏疗尖殖井费州访吹荣铜沿替滚客召旱悟刺脑措贯藏敢令隙炉壳硫煤迎铸粘探临薄旬善福纵择礼愿伏残雷延烟句纯渐耕跑泽慢栽鲁赤繁境潮横掉锥希池败船假亮谓托伙哲怀割摆贡呈劲财仪沉炼麻罪祖息车穿货销齐鼠抽画饲龙库守筑房歌寒喜哥洗蚀废纳腹乎录镜妇恶脂庄擦险赞钟摇典柄辩竹谷卖乱虚桥奥伯赶垂途额壁网截野遗静谋弄挂课镇妄盛耐援扎虑键归符庆聚绕摩忙舞遇索顾胶羊湖钉仁音迹碎伸灯避泛亡答勇频皇柳哈揭甘诺概宪浓岛袭谁洪谢炮浇斑讯懂灵蛋闭孩释乳巨徒私银伊景坦累匀霉杜乐勒隔弯绩招绍胡呼痛峰零柴簧午跳居尚丁秦稍追梁折耗碱殊岗挖氏刃剧堆赫荷胸衡勤膜篇登驻案刊秧缓凸役剪川雪链渔啦脸户洛孢勃盟买杨宗焦赛旗滤硅炭股坐蒸凝竟陷枪黎救冒暗洞犯筒您宋弧爆谬涂味津臂障褐陆啊健尊豆拔莫抵桑坡缝警挑污冰柬嘴啥饭塑寄赵喊垫丹渡耳刨虎笔稀昆浪萨茶滴浅拥穴覆伦娘吨浸袖珠雌妈紫戏塔锤震岁貌洁剖牢锋疑霸闪埔猛诉刷狠忽灾闹乔唐漏闻沈熔氯荒茎男凡抢像浆旁玻亦忠唱蒙予纷捕锁尤乘乌智淡允叛畜俘摸锈扫毕璃宝芯爷鉴秘净蒋钙肩腾枯抛轨堂拌爸循诱祝励肯酒绳穷塘燥泡袋朗喂铝软渠颗惯贸粪综墙趋彼届墨碍启逆卸航衣孙龄岭骗休借".$addChars;
			break;
		default :
			// 默认去掉了容易混淆的字符oOLl和数字01，要添加请使用addChars参数
			$chars='ABCDEFGHIJKMNPQRSTUVWXYZabcdefghijkmnpqrstuvwxyz23456789'.$addChars;
			break;
	}
	if($len>10 ) {//位数过长重复字符串一定次数
		$chars= $type==1? str_repeat($chars,$len) : str_repeat($chars,5);
	}
	if($type!=4) {
		$chars   =   str_shuffle($chars);
		$str     =   substr($chars,0,$len);
	}else{
		// 中文随机字
		for($i=0;$i<$len;$i++){
			$str.= msubstr($chars, floor(mt_rand(0,mb_strlen($chars,'utf-8')-1)),1);
		}
	}
	return $str;
}


function get_order_status($code='') {
	$result = array();
	switch ($code) {
		case 'N':$result = array('未完善', 'btn-inverse', 'label-inverse');break;
		case 'O':$result = array('已下单', '', '');break;
		case 'Q':$result = array('已确认', 'btn-info', 'label-info');break;
		case 'C':$result = array('已付款', 'btn-warning', 'label-warning');break;
		case 'P':$result = array('已发货', 'btn-success', 'label-success');break;
		case 'I':$result = array('已取消', 'btn-inverse', 'label-inverse');break;
		case 'T':$result = array('已退款', 'btn-inverse', 'label-inverse');break;
		case 'R':$result = array('扣关重发', 'btn-inverse', 'label-inverse');break;
		case 'press':$result = array('催款', '', '');break;
		case 'ready':$result = array('待发货', 'btn-warning', 'label-warning');break;
		case 'wait':$result = array('等货', 'btn-danger', 'label-danger');break;
		case 'stockout':$result = array('缺货', 'btn-danger', 'label-danger');break;
		default:$result = array($code, '', '');break;
	}
	return $result;
}

/**
 * 重写订单数据
 * 
 * @param string $data
 * @return string
 */
function overwrite_order_data($data='') {
	
	if (!empty($data['values']) && !is_array($data['values'])) {
		$data['values'] = json_decode($data['values'], true);
	}
	list($data['order_status'], $data['order_status_btn_class'], $data['order_status_label_class']) = get_order_status($data['order_status']);
	$siteinfo = M('website')->field('prefix,smtp_info')->where(array('weburl'=>$data['values']['domain']))->find();
	
	if (!empty($siteinfo)) {
		($siteinfo['prefix'] == false) ? $data['prefix'] = '' : $data['prefix'] = $siteinfo['prefix'];
		$smtp = json_decode($siteinfo['smtp_info'], true);
		if (!empty($smtp) && !empty($smtp['smtp_host']) && !empty($smtp['smtp_account']) && !empty($smtp['smtp_password'])) {
			$data['smtp'] = 1;
			$data['account_name'] = $smtp['account_name'];
		} else {
			$data['smtp'] = $data['account_name'] = '';
		}
	} else {
		$data['prefix'] = $data['smtp'] = '';
	}
	return $data;
}

function analysis_smtp($website) {
	$default = array('smtp_host'=>'', 'smtp_account'=>'', 'smtp_password'=>'', 'smtp_port'=>'', 'smtp_auth'=>'', 'auth_type'=>'');
	if (!empty($website['smtp_info'])) {
		$smtp_info = json_decode($website['smtp_info'], true);
		if (!empty($smtp_info)) {
			foreach ($smtp_info as $k=>$v) {
				if ($k == 'smtp_password' && !empty($v)) {
					$website[$k] = 1;
				} else {
					$website[$k] = $smtp_info[$k];
				}
			}
		} else {
			$website = array_merge($website, $default);
		}
	} else {
		$website = array_merge($website, $default);
	}
	return $website;
}

function replace_mail_temp($content) {
	
}

function mail_event_to_text($event='') {
	switch ($event) {
		case 'opened':return array("已打开", 'label-info', "用户打开了邮件");break;
		case 'submit':return array("已队列", 'label-info', "提交了邮件队列");break;
		case 'delivered':return array("已投递", 'label-success', "成功投递到对方服务器");break;
		case 'bounced':return array("发送失败", 'label-important', "");break;
		case 'clicked':return array("已点击", 'label-info', "用户点击了邮件中的链接");break;
		case 'unsubscribed':return array("已退订", 'label-warning', "用户取消了邮件订阅");break;
		case 'complained':return array("已投诉", 'label-warning', "做出了垃圾邮件投诉");break;
		case 'dropped':return array("遇到问题", 'label-important', "");break;
		case 'resolved':return array("二次解决", 'label-success', "");break;
		default:return array("未知状态", '', '');break;
	}
}

function get_resolutive_way_text($code='') {
	switch ($code) {
		case 'manual_send':return "手动发送";break;
		case 'mail_error':return "邮箱不正确";break;
		case 'other':return "其他";break;
		default:return "未知";
	}
}

/**
 * 多个条件分隔字符为数组
 * 
 * @param string/array $char
 * @param string $string
 */
function multi_explode($char, $string='') {
	if (is_string($char)) {
		return explode($char, $string);
	} elseif (is_array($char)) {
		foreach ($char as $val) {
			$string = str_replace($char, " ", $string);
		}
		return explode(" ", $string);
	}
}

function local_image($url='') {

	if (empty($url) || strpos($url, 'http') === false) {
		return false;
	}

	$ch_local_img = curl_init();
	curl_setopt($ch_local_img, CURLOPT_URL, $url);
	curl_setopt($ch_local_img, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($ch_local_img, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch_local_img, CURLOPT_HEADER, 0);
	curl_setopt($ch_local_img, CURLOPT_HTTPHEADER, array("Accept-Language:ja"));
	curl_setopt($ch_local_img, CURLOPT_USERAGENT, "Googlebot/2.1; +http://www.google.com/bot.html");
	$result = curl_exec($ch_local_img);
	$error = curl_error($ch_local_img);
	$errno = curl_errno($ch_local_img);
	$info = curl_getinfo($ch_local_img);
	curl_close($ch_local_img);

	if (!empty($error)) {
		return false;
	}
	$file_type = '.jpg';
	switch ($info['content_type']) {
		case 'image/png':$file_type = '.png';break;
		case 'image/gif':$file_type = '.gif';break;
		case 'image/bmp':$file_type = '.bmp';break;
	}

	$image_md5 = md5($result);
	$pathname = '/images/' . date('Ym') . '/';
	$exists = M("image_index")->field("image_path,image_name")->where(array("image_md5"=>$image_md5))->find();
	if (!empty($exists)) {
		return array("path"=>$exists['image_path'], 'name'=>$exists['image_name'], 'fullpath'=>$exists['image_path'] . $exists['image_name']);
	}
	$image_path = ENTRY_PATH . $pathname;
	recursion_mkdir($image_path);

	$filename = uniqid() . $file_type;
	$hd = fopen($image_path . $filename, "wb");
	fwrite($hd, $result);
	fclose($hd);
	M("image_index")->add(array("image_md5"=>$image_md5, 'image_path'=>$pathname, 'image_name'=>$filename));
	return array("path"=>$pathname, 'name'=>$filename, 'fullpath'=>$pathname . $filename);
}
?>