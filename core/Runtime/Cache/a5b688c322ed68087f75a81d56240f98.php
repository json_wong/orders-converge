<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>    <html class="lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>    <html class="lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html>
<!--<![endif]-->
<head>
<title><?php echo ($app_name); ?></title>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<?php  if (APP_DEBUG === true) { ?>
<link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css" />
<link rel="stylesheet" href="/bootstrap/css/bootstrap-responsive.min.css" />
<link rel="stylesheet" href="/bootstrap/extend/jasny-bootstrap/css/jasny-bootstrap.min.css">
<link rel="stylesheet" href="/bootstrap/extend/jasny-bootstrap/css/jasny-bootstrap-responsive.min.css">
<link rel="stylesheet" href="/theme/scripts/jquery-ui-1.9.2.custom/css/smoothness/jquery-ui-1.9.2.custom.min.css" />
<link rel="stylesheet" href="/theme/css/glyphicons.css" />
<link rel="stylesheet" href="/bootstrap/extend/bootstrap-select/bootstrap-select.css" />
<link rel="stylesheet" href="/bootstrap/extend/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
<link rel="stylesheet" href="/theme/scripts/pixelmatrix-uniform/css/uniform.default.css" />
<link rel="stylesheet" href="/theme/scripts/notyfy/jquery.notyfy.css"/>
<link rel="stylesheet" href="/theme/scripts/notyfy/themes/default.css"/>
<link rel="stylesheet" href="/theme/scripts/Gritter/css/jquery.gritter.css"/>
<link rel="stylesheet" href="/theme/scripts/google-code-prettify/prettify.css" type="text/css" />
<link rel="stylesheet" href="/theme/css/style.min.css?1362656609" />
<script type="text/javascript" src="/theme/scripts/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="/theme/scripts/modernizr.custom.76094.js"></script>
<script type="text/javascript" src="/theme/scripts/notyfy/jquery.notyfy.js"></script>
<script type="text/javascript" src="/theme/scripts/Gritter/js/jquery.gritter.min.js"></script>
<script type="text/javascript" src="/theme/scripts/less-1.3.3.min.js"></script>
<?php  } else { ?>
<link rel="stylesheet" href="/bootstrap/??css/bootstrap.min.css,css/bootstrap-responsive.min.css,extend/jasny-bootstrap/css/jasny-bootstrap.min.css,extend/jasny-bootstrap/css/jasny-bootstrap-responsive.min.css,extend/bootstrap-select/bootstrap-select.css,extend/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
<link rel="stylesheet" href="/theme/scripts/??jquery-ui-1.9.2.custom/css/smoothness/jquery-ui-1.9.2.custom.min.css,pixelmatrix-uniform/css/uniform.default.css,notyfy/jquery.notyfy.css,notyfy/themes/default.css,Gritter/css/jquery.gritter.css,google-code-prettify/prettify.css" />
<link rel="stylesheet" href="/theme/css/glyphicons.css" />
<link rel="stylesheet" href="/theme/css/style.min.css?1362656609" />
<script type="text/javascript" src="/theme/scripts/??jquery-1.8.2.min.js,modernizr.custom.76094.js,notyfy/jquery.notyfy.js,Gritter/js/jquery.gritter.min.js,less-1.3.3.min.js"></script>
<?php  } ?>
<link rel="shortcut icon" href="/theme/images/favicon.png">
<style type="text/css">
.table-condensed{font-size: 12px}
.label-cancal{color:#777;background: #333}
.navbar.main .topnav>li.open .advanced-search{width:350px;}
.navbar.main .topnav>li.open .advanced-search input{color:#333;}
.navbar.main .topnav>li.open .advanced-search .control-group{margin:0;}
.navbar.main .topnav>li.open .advanced-search .control-label{width:65px;height:30px;line-height: 30px}
.navbar.main .topnav>li.open .advanced-search .controls{margin-left:85px}
input.nmb,select.nmb,form.nmb{margin-bottom: 0}
.dropdown-menu>li>a{text-align: left}
input[type=text], input[type=password], select, textarea{color:#666;}
div#add_webcate,.lg-modal{width:900px;margin-left: -450px;top:3%;}
#add_account.modal{width:90%;margin-left: -45%;top:3%;}
div#add_webcate div.widget-body{max-height: 500px;overflow-y:auto}
div#change_order_status_modal div.modal-body{max-height: 81%}
div#change_order_status_modal div.modal-body .lg{width:95%;}
div#change_order_status_modal .ems_number_wrap{display: none}
div#change_order_status_modal .ems_number_wrap input{margin-bottom: 0;}
.navbar.main .topnav>li .notif_except li>a.glyphicons.white i:before{color:#fff;}
div.click_go{cursor: pointer;}
.notyfy_error{background-color: #B94A48}
.relative{position: relative}
.hover_display{position: relative}
.cell_modify_button{position: absolute;left: 5px; bottom:3px;}
</style>
</head>
<body>

<!-- Start Content -->
<div class="container-fluid"> 	<div class="navbar main">
		<a href="/" class="appbrand"><span><?php echo ($app_name); ?></span></a>
		<button type="button" class="btn btn-navbar">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<ul class="topnav pull-right">
			<?php if(in_array('view_order_archive', $profile['permissions'])): ?><li><a href="/order/prints" class="glyphicons calendar" target="_blank"><i></i><span class="hidden-phone">发货归档</span></a></li><?php endif; ?>
			<li><a href="/order/search/?order_status=stockout" class="glyphicons circle_remove"><i></i><span class="hidden-phone">缺货</span>(<?php echo ($stockout); ?>)</a></li>
			<li><a href="/order/customs_hold" class="glyphicons skull"><i></i><span class="hidden-phone">海关扣押</span>(<?php echo ($customs_hold); ?>)</a></li>
			<li><a href="/mail/lists?status=failure" class="glyphicons message_ban"><i></i><span class="hidden-phone">失败邮件</span>(<?php echo ($failure_mail); ?>)</a></li>
			<li><a href="/order/beeptimer" class="glyphicons alarm"><i></i><span class="hidden-phone">定时提醒</span>(<?php echo ($beeptimer); ?>)</a></li>
			<?php if(in_array('advanced_search', $profile['permissions'])): ?><li class="dropdown visible-desktop">
				<a href="" data-toggle="dropdown" class="glyphicons search"><i></i>订单搜索 <span class="caret"></span></a>
				<div class="dropdown-menu advanced-search">
					<form class="form-horizontal" id="validateSubmitForm" method="POST" action="/order/search/">
						<div class="control-group">
							<label class="control-label" for="email">邮箱</label>
							<div class="controls"><input placeholder="account@example.com" id="email" name="email" type="text" value="<?php if(!empty($search_condition["email"])): echo ($search_condition["email"]); endif; ?>"></div>
						</div>
						<div class="control-group">
							<label class="control-label" for="uname">姓名</label>
							<div class="controls">
								<input placeholder="姓名" id="uname" name="uname" type="text" value="<?php if(!empty($search_condition["uname"])): echo ($search_condition["uname"]); endif; ?>">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="address">地址</label>
							<div class="controls">
								<input placeholder="客户地址" id="address" name="address" type="text" value="<?php if(!empty($search_condition["address"])): echo ($search_condition["address"]); endif; ?>">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="project_name">产品</label>
							<div class="controls">
								<input placeholder="产品名称" id="project_name" name="project_name" type="text" value="<?php if(!empty($search_condition["project_name"])): echo ($search_condition["project_name"]); endif; ?>">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="order_id">订单号</label>
							<div class="controls"><input placeholder="订单号，不带前缀" id="order_id" name="order_id" value="<?php if(!empty($search_condition["order_id"])): echo ($search_condition["order_id"]); endif; ?>" type="text"></div>
						</div>
						<div class="control-group">
							<label class="control-label" for="order_status">订单状态</label>
							<div class="controls">
								<select id="order_status" name="order_status">
									<option value="">全部订单状态</option>
									<option value="O" <?php if(!empty($search_condition['order_status']) AND $search_condition['order_status'] == 'O'): ?>selected="selected"<?php endif; ?>>已下单</option>
									<option value="Q" <?php if(!empty($search_condition['order_status']) AND $search_condition['order_status'] == 'Q'): ?>selected="selected"<?php endif; ?>>已确认</option>
									<option value="C" <?php if(!empty($search_condition['order_status']) AND $search_condition['order_status'] == 'C'): ?>selected="selected"<?php endif; ?>>已付款</option>
									<option value="ready" <?php if(!empty($search_condition['order_status']) AND $search_condition['order_status'] == 'ready'): ?>selected="selected"<?php endif; ?>>待发货</option>
									<option value="wait" <?php if(!empty($search_condition['order_status']) AND $search_condition['order_status'] == 'wait'): ?>selected="selected"<?php endif; ?>>等货</option>
									<option value="stockout" <?php if(!empty($search_condition['order_status']) AND $search_condition['order_status'] == 'stockout'): ?>selected="selected"<?php endif; ?>>缺货</option>
									<option value="P" <?php if(!empty($search_condition['order_status']) AND $search_condition['order_status'] == 'P'): ?>selected="selected"<?php endif; ?>>已发货</option>
									<option value="I" <?php if(!empty($search_condition['order_status']) AND $search_condition['order_status'] == 'I'): ?>selected="selected"<?php endif; ?>>已取消</option>
									<option value="T" <?php if(!empty($search_condition['order_status']) AND $search_condition['order_status'] == 'T'): ?>selected="selected"<?php endif; ?>>已退款</option>
								</select>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="include_kf_remark">客服备注</label>
							<div class="controls"><input type="checkbox" id="include_kf_remark" name="remark" value="1" <?php if(!empty($search_condition['remark'])): ?>checked="checked"<?php endif; ?>></div>
						</div>
						<?php if(in_array('view_order_price', $profile['permissions'])): ?><div class="control-group">
							<label class="control-label" for="prices">订单金额</label>
							<div class="controls"><input placeholder="5000，或者1000-5000" id="prices" name="prices" type="text" value="<?php if(!empty($search_condition["display_prices"])): echo ($search_condition["display_prices"]); endif; ?>"></div>
						</div><?php endif; ?>
						<div class="control-group">
							<label class="control-label" for="cid">网站分类</label>
							<div class="controls">
								<select name="cid">
									<option value="">选择全部分类</option>
									<?php if(is_array($webcate)): $i = 0; $__LIST__ = $webcate;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$wc): $mod = ($i % 2 );++$i;?><option value="<?php echo ($wc["cid"]); ?>" <?php if(!empty($search_condition['cid']) AND $wc['cid'] == $search_condition['cid']): ?>selected="selected"<?php endif; ?>><?php echo ($wc["cate_name"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
								</select>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="wid">网站</label>
							<div class="controls">
								<select name="wid">
									<option value="">选择全部网站</option>
									<?php if(is_array($website)): $i = 0; $__LIST__ = $website;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$ws): $mod = ($i % 2 );++$i;?><optgroup label="<?php echo ($key); ?>">
										<?php if(is_array($ws)): $i = 0; $__LIST__ = $ws;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?><option value="<?php echo ($v["id"]); ?>" <?php if(!empty($search_condition['wid']) AND $v['id'] == $search_condition['wid']): ?>selected="selected"<?php endif; ?>><?php if(!empty($v["prefix"])): echo ($v["prefix"]); ?> -&nbsp;<?php endif; echo ($v["weburl"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
									</optgroup><?php endforeach; endif; else: echo "" ;endif; ?>
								</select>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="shipping_method">物流方式</label>
							<div class="controls">
								<select name="shipping_method">
									<option value="">选择全部物流</option>
									<option value="EMS" <?php if(!empty($search_condition['shipping_method']) AND 'EMS' == $search_condition['shipping_method']): ?>selected="selected"<?php endif; ?>>EMS</option>
									<option value="DHL" <?php if(!empty($search_condition['shipping_method']) AND 'DHL' == $search_condition['shipping_method']): ?>selected="selected"<?php endif; ?>>DHL</option>
								</select>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="ip">IP</label>
							<div class="controls"><input placeholder="IP地址，例：127.0.0.1" id="ip" name="ip" type="text" value="<?php if(!empty($search_condition["ip"])): echo ($search_condition["ip"]); endif; ?>"></div>
						</div>
						<div class="control-group">
							<label class="control-label" for="first_name">日期</label>
							<div class="controls">
								<input class="input-small" placeholder="开始日期" id="start_date" name="start_date" type="text" value="<?php if(!empty($search_condition["start_date"])): echo (date('Y-m-d',$search_condition["start_date"])); endif; ?>" /> &nbsp;
								<input class="input-small" placeholder="结束日期" id="end_date" name="end_date" type="text" value="<?php if(!empty($search_condition["end_date"])): echo (date('Y-m-d',$search_condition["end_date"])); endif; ?>"/>
							</div>
						</div>
						<div class="control-group">
							<div class="controls">
								<button type="submit" class="btn btn-primary">走你</button>
							</div>
						</div>
					</form>
				</div>
			</li><?php endif; ?>
			<li class="account"> <a data-toggle="dropdown" href="javascript:void(0);" class="glyphicons logout lock"><span class="hidden-phone text"><?php echo ($username); ?></span><i></i></a>
				<ul class="dropdown-menu pull-right">
					<li><a href="javascript:void(0);" class="glyphicons cogwheel">Settings<i></i></a></li>
					<li class="highlight profile"> <span> <span class="heading">Profile <a href="javascript:void(0);" class="pull-right">edit</a></span> <span class="img"></span> <span class="details"> <a href="javascript:void(0);"><?php echo ($username); ?></a> contact@mosaicpro.biz </span> <span class="clearfix"></span> </span> </li>
					<li> <span> <a class="btn btn-default btn-small pull-right" style="padding: 2px 10px; background: #fff;" href="/account/logout">退出登录</a> </span> </li>
				</ul>
			</li>
		</ul>
	</div>
	<div id="wrapper"> 		<div id="menu" class="hidden-phone"> <span class="profile"> <a class="img" href="javascript:void(0);"><img src="/theme/images/photo.gif" alt="<?php echo ($username); ?>" /></a> <span> <strong><?php echo ($username); ?></strong> <a href="javascript:void(0);">edit account</a> </span> </span>
			<div id="search">
				<input type="text" placeholder="Quick search ..." />
				<button class="glyphicons search"><i></i></button>
			</div>
			<ul>
			<?php if(in_array('view_orders', $profile['permissions'])): ?><li class="glyphicons home <?php if($menu == "home"): ?>active<?php endif; ?>"> <a href="/"><i></i><span>首页</span></a></li><?php endif; ?>
			<?php if(in_array('view_orders', $profile['permissions'])): ?><li class="glyphicons list <?php if($menu == "orders"): ?>active<?php endif; ?>"> <a href="/order/lists"><i></i><span>订单列表</span></a></li><?php endif; ?>
			<?php if(in_array('view_orders_analysis', $profile['permissions'])): ?><li class="glyphicons charts <?php if($menu == "analysis"): ?>active<?php endif; ?>"> <a href="/order/analysis"><i></i><span>订单分析</span></a></li><?php endif; ?>
			<?php if(in_array('view_mail_list', $profile['permissions'])): ?><li class="glyphicons message_out <?php if($menu == "mail"): ?>active<?php endif; ?>"> <a href="/mail/lists"><i></i><span>发件箱</span></a></li><?php endif; ?>
			<?php if(in_array('view_mail_in', $profile['permissions'])): ?><li class="glyphicons message_in <?php if($menu == "inbox"): ?>active<?php endif; ?>">
					<a href="/mail/inbox"><i></i><span>收件箱</span></a>
					<span class="count"><?php echo ($inbox_count); ?></span>
				</li><?php endif; ?>
			<?php if(in_array('view_user_analysis', $profile['permissions'])): ?><li class="glyphicons parents <?php if($menu == "user_analysis"): ?>active<?php endif; ?>"> <a href="/order/user_analysis"><i></i><span>用户分析</span></a></li><?php endif; ?>
			<?php if(in_array('view_website', $profile['permissions'])): ?><li class="glyphicons paperclip <?php if($menu == "website"): ?>active<?php endif; ?>"> <a href="/website"><i></i><span>网站接入</span></a></li><?php endif; ?>
			<?php if(in_array('view_webcate', $profile['permissions'])): ?><li class="glyphicons show_big_thumbnails <?php if($menu == "webcate"): ?>active<?php endif; ?>"> <a href="/website/category"><i></i><span>网站分类</span></a></li><?php endif; ?>
			<?php if(in_array('view_account_list', $profile['permissions'])): ?><li class="glyphicons user <?php if($menu == "account"): ?>active<?php endif; ?>"> <a href="/account"><i></i><span>账号管理</span></a></li><?php endif; ?>
			<?php if(in_array('view_recycle', $profile['permissions'])): ?><li class="glyphicons bin <?php if($menu == "recycle"): ?>active<?php endif; ?>"> <a href="<?php echo get_url('order_recycle');?>"><i></i><span>回收站</span></a></li><?php endif; ?>
			</ul>
			<div class="clearfix" style="clear: both"></div>
		</div>
		<div id="content">
			<ul class="breadcrumb">
				<li><a href="/" class="glyphicons home"><i></i> Home</a></li>
				<li class="divider"></li>
				<li><?php echo ($page_title); ?></li>
			</ul>
			<div class="separator bottom"></div>
			<div class="heading-buttons">
				<h3><?php echo ($page_title); ?></h3>
				<div class="clearfix" style="clear: both;"></div>
			</div>
			<div class="separator bottom"></div>
						<div class="innerLR">
				<div class="filter">
					<select class="filter_mail">
						<option value="" <?php if(empty($status)): ?>selected="selected"<?php endif; ?>>全部状态</option>
						<option value="failure" <?php if($status == 'failure'): ?>selected="selected"<?php endif; ?>>发送失败</option>
						<option value="submit" <?php if($status == 'submit'): ?>selected="selected"<?php endif; ?>>等待发送</option>
						<option value="success" <?php if($status == 'success'): ?>selected="selected"<?php endif; ?>>发送成功</option>
						<option value="resolved" <?php if($status == 'resolved'): ?>selected="selected"<?php endif; ?>>二次解决</option>
					</select>
					<strong>邮件打开率：<?php echo ($open_percent); ?>%（<?php echo ($opened_count); ?> / <?php echo ($all_count); ?>）（仅针对直接投递成功的邮件，并且可能会有误差，仅供参考）</strong>
				</div>
				<table class="table table-bordered table-condensed table-vertical-center">
					<thead>
						<tr>
							<th width="8%" class="center">ID</th>
							<th width="10%" class="center">所属订单</th>
							<th width="6%" class="center">订单状态</th>
							<th width="20%" class="center">用户名称</th>
							<th width="25%" class="center">用户邮箱</th>
							<th width="7%" class="center">发送状态</th>
							<th width="7%" class="center">状态描述</th>
							<th width="7%" class="center">打开状态</th>
							<th width="10%" class="center">操作</th>
						</tr>
					</thead>
					<tbody>
					<?php if(!empty($mails)): if(is_array($mails)): $i = 0; $__LIST__ = $mails;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr class="selectable <?php echo ($vo["mail_status_bg_color"]); ?>">
							<td class="center"><a href="javascript:;" class="view_mail_detail" data-email="<?php echo ($vo["email"]); ?>"><?php echo ($vo["id"]); ?></a></td>
							<td class="center">
								<a href="<?php echo (get_url('order_detail',$vo["order_id"])); ?>" data-toggle="tooltip" data-trigger="hover" data-html="true" data-placement="right" data-original-title="网站：<?php echo ($vo["domain"]); ?><br>编号：<?php echo ($vo["prefix"]); ?><br>下单：<?php echo ($vo["date"]); ?>"><?php echo ($vo["prefix"]); ?>-<?php echo ($vo["order_numid"]); ?></a>
							</td>
							<td class="center"><?php echo ($vo["order_status"]); ?></td>
							<td class="center"><?php echo ($vo["first_name"]); ?> <?php echo ($vo["last_name"]); ?></td>
							<td class="center"><?php echo ($vo["email"]); ?></td>
							<td class="center">
								<span class="label label-<?php echo ($vo["mail_status_color"]); ?>"><?php echo ($vo["mail_status"]); ?></span>
							</td>
							<td class="center">
								<?php if($vo['resolved'] == 1): ?><a href="javascript:;" data-html="true" data-toggle="tooltip" data-placement="left" data-original-title="<?php echo ($vo["status_remark"]); ?>"><span class="label label-important">二次解决</span></a>
								<?php else: ?>
								<span class="label label-success">直接投递</span><?php endif; ?>
							</td>
							<td class="center">
								<?php if($vo['opened'] == 1): ?><span class="label label-success">已打开</span>
								<?php else: ?>
								<span class="label label-error">未打开</span><?php endif; ?>
							</td>
							<td class="center">
								<a href="javascript:;" data-mail-id="<?php echo ($vo["id"]); ?>" class="view_mail_log">日志</a> &nbsp;
								<?php if($vo['mail_status_color'] == 'important'): ?><a href="javascript:;" data-mail-id="<?php echo ($vo["id"]); ?>" class="solve_problem">解决问题</a><?php endif; ?>
							</td>
						</tr><?php endforeach; endif; else: echo "" ;endif; ?>
					<?php else: ?>
					<td class="center" colspan="8">当前没有任何邮件记录！</td><?php endif; ?>
					</tbody>
				</table>
				<div class="separator top form-inline small">
					<div class="pagination pull-right" style="margin: 0;">
						<?php echo ($page); ?>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div id="view_mail_detail_modal" data-width="80" class="auto-height modal hide fade">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="heading">邮件内容</h3>
				</div>
				<div class="modal-body"></div>
				<div class="modal-footer">
					<button class="btn" data-dismiss="modal" aria-hidden="true">关闭</button>
				</div>
			</div>
			<div id="mail_log" data-width="80" class="auto-height modal hide fade">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="heading">邮件日志</h3>
				</div>
				<div class="modal-body">
					<p>正在载入日志...</p>
				</div>
				<div class="modal-footer">
					<button class="btn" data-dismiss="modal" aria-hidden="true">关闭</button>
				</div>
			</div>
			<div id="solve_problem_modal" class="modal hide fade">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="heading">解决问题邮件</h3>
				</div>
				<div class="modal-body">
					<p>一般遇到发送失败或者发送异常的邮件，根据错误消息通过手动再次发送邮件或者其他方法来解决此问题。</p>
					<p>姓名：<input name="solver" type="text" placeholder="由谁来解决此问题邮件，这里输入姓名" value='<?php echo ($solver); ?>' class="solver input-xlarge"></p>
					<p>解决方式：</p>
					<p>
						<label class="checkbox" for="manual_send">
							<input type="radio" id="manual_send" class="resolutive_way" name="resolutive_way" value="manual_send"> 手动发送
						</label>
						<label class="checkbox" for="mail_error">
							<input type="radio" id="mail_error" class="resolutive_way" name="resolutive_way" value="mail_error"> 邮箱不正确
						</label>
						<label class="checkbox" for="need_not">
							<input type="radio" id="need_not" class="resolutive_way" name="resolutive_way" value="other"> 其他
						</label>
						<label class="checkbox" for="other_desc">
							<input type="text" id="other_desc" name="other_desc" class="input-xlarge" placeholder="这里输入其他原因">
						</label>
					</p>
				</div>
				<div class="modal-footer">
					<button class="btn btn-primary" id="resolved_affirm">我已确定解决了此问题邮件</button>
					<button class="btn" data-dismiss="modal" aria-hidden="true">关闭</button>
				</div>
			</div>
			<script>
				$(function(){
					$("select.filter_mail").change(function(){
						window.location.href="/mail/lists?status=" + $(this).val();
					});

					$("a.solve_problem").click(function(event){
						var mail_id = $(this).attr("data-mail-id");
						var solve_problem_modal = $("#solve_problem_modal");
						$("#resolved_affirm").attr("data-mail-id", mail_id);
						solve_problem_modal.modal();
					});

					$("a.view_mail_detail").click(function(event){
						var mail_id = $(this).html();
						var email = $(this).attr("data-email");
						var view_mail_detail_modal = $("div#view_mail_detail_modal");
						view_mail_detail_modal.find(".modal-body").html('<p>正在载入邮件...</p>');
						view_mail_detail_modal.modal();
						$.ajax({
							url: '/ajax/get_mail_detail', type: 'GET', dataType: 'json',
							data: {mail_id: mail_id, 'email':email},
							success:function(result){
								view_mail_detail_modal.find(".modal-body").html(result.data);
							}
						});
					});

					$("button#resolved_affirm").click(function(event){
						var solver = $("input.solver").val();
						var other_desc = $("#other_desc").val();
						var mail_id = $(this).attr("data-mail-id");
						var resolutive_way = $("input.resolutive_way:checked").val();
						if (solver == '') {
							alert('请输入解决者姓名！');
							$("input.solver").focus();
							return false;
						};

						if (resolutive_way == '' || resolutive_way == 'undefined' || resolutive_way == undefined) {
							alert("请选择解决方式！");
							return false;
						};

						if (resolutive_way == 'other' && other_desc == '') {
							alert("请输入其他原因！");
							$("input#other_desc").focus();
							return false;
						}
						console.log(resolutive_way);
						$.ajax({
							url: '/ajax/solve_problem_mail', type: 'POST', dataType: 'json',
							data: {mail_id: mail_id, solver: solver, resolutive_way:resolutive_way, other_desc: other_desc},
							success:function(result){
								if (result.status == 200) {
									alert(result.info);
									window.location.reload();
								} else {
									alert(result.info);
								}
							}
						});
					});

					$("a.view_mail_log").click(function(event){
						var string = "";
						var mail_log = $("#mail_log");
						var mail_id = $(this).attr("data-mail-id");
						var modalbody = mail_log.find("div.modal-body");
						modalbody.html("<p>正在载入日志...</p>");
						mail_log.modal();
						$.ajax({
							url: '/ajax/get_mail_log', type: 'GET', dataType: 'json',
							data: {mail_id: mail_id},
							success:function(result){
								if (result.status == 200) {
									string = "<table class='table table-bordered'><thead><tr><td width='15%'>时间</td><td width='10%'>状态</td><td width='70%'>消息</td><td width='5%'>代码</td></tr></thead><tbody>";
									for (var i = 0; i < result.logs.length; i++) {
										string += "<tr>";
										string += "<td>"+result.logs[i].pubdate+"</td>";
										string += "<td><label class='label "+result.logs[i].event_class+"'>"+result.logs[i].event_text+"</label></td>";
										string += "<td>"+result.logs[i].message+"</td>";
										string += "<td>"+result.logs[i].code+"</td>";
										string += "</tr>";
									};
									string += "</tbody></table>";
									modalbody.html(string);
								} else {
									
								}
							}
						});
					})
				})
			</script>
		</div>
		<!-- End Wrapper --> 
	</div>
</div>

<!-- JQueryUI v1.9.2 --> 
<script src="/theme/scripts/jquery-ui-1.9.2.custom/js/jquery-ui-1.9.2.custom.min.js"></script>
<script src="/theme/scripts/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>
<script src="/theme/scripts/jquery.cookie.js"></script>
<script src="/theme/scripts/jquery.ba-resize.js"></script>
<script src="/theme/scripts/pixelmatrix-uniform/jquery.uniform.min.js"></script>
<script src="/bootstrap/js/bootstrap.min.js"></script>
<script src="/bootstrap/extend/bootstrap-select/bootstrap-select.js"></script>
<script src="/bootstrap/extend/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
<script src="/bootstrap/extend/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js"></script>
<script src="/bootstrap/extend/jasny-bootstrap/js/jasny-bootstrap.min.js" type="text/javascript"></script>
<script src="/bootstrap/extend/bootbox.js" type="text/javascript"></script>
<script src="/theme/scripts/load.js"></script>
<script type="text/javascript">
	$(function(){
		$("div.auto-height.modal").on("show", function(){
			var data_width = $(this).attr("data-width");
			var mar_left = data_width / 2;
			var winheight = $(window).height();
			var modalheight = winheight * 0.77;
			$(this).css({"top":"3%", "width":data_width+"%", "margin-left":"-"+mar_left+"%"});
			$(this).find(".modal-body").css({"max-height":modalheight});
		});

		$("a#tgbl").click(function(event){
			if (<?php echo ($customs_hold); ?> > 0 && $(this).attr("data-shown") != 'true') {
				var n = notyfy({
					text: '<h4 style="color:#fff">海关扣押!</h4> <p>当前有<?php echo ($customs_hold); ?>个订单被海关扣押！<a href="/order/customs_hold" style="color:#000">点击这里查看。</a></p>',
					type: 'error', layout: 'bottomRight', theme: 'boolight', closeWith: ['click'],
					hideEffect: function(bar) {
						bar.animate({ height: 'toggle' }, 500, 'swing');
						$("a#tgbl").attr("data-shown", 'false');
					}
				});
				$(this).attr("data-shown", 'true');
			}
		});
		
		$("a#failure_mail").click(function(event){
			if (<?php echo ($failure_mail); ?> > 0 && $(this).attr("data-shown") != 'true') {
				var n = notyfy({
					text: '<h4 style="color:#826200">失败邮件!</h4> <p>当前有<?php echo ($failure_mail); ?>封邮件发送失败！<a href="/mail/lists?status=failure" style="color:#000">点击这里查看。</a></p>',
					type: 'warning', layout: 'bottomRight', theme: 'boolight', closeWith: ['click'],
					hideEffect: function(bar) {
						bar.animate({ height: 'toggle' }, 500, 'swing');
						$("a#failure_mail").attr("data-shown", 'false');
					}
				});
				$(this).attr("data-shown", 'true');
			}
		});

		$("a#beeptimer").click(function(event){
			if (<?php echo ($beeptimer); ?> > 0 && $(this).attr("data-shown") != 'true') {
				var n = notyfy({
					text: '<h4 style="color:darkgreen">定时提醒!</h4> <p>当前有<?php echo ($beeptimer); ?>个订单提醒！<a href="/order/beeptimer" style="color:#000">点击这里查看。</a></p>',
					type: 'success', layout: 'bottomRight', theme: false, closeWith: ['click'],
					hideEffect: function(bar) {
						bar.animate({ height: 'toggle' }, 500, 'swing');
						$("a#beeptimer").attr("data-shown", 'false');
					}
				});
				$(this).attr("data-shown", 'true');
			}
		});

		$("div.click_go").click(function(event){
			var href = $(this).attr("data-href");
			window.location.href=href;
		});

		$(".tooltips").tooltip({html:true});

		$(".hover_display").hover(function() {
			$(this).find('.cell_modify_button').removeClass('hidden');
		}, function() {
			$(this).find('.cell_modify_button').addClass('hidden');
		});

		$(".cell_modify_button").on('click', function(event) {
			var cell = $(this).parent("td");
			var brreg = new RegExp("");
			$(this).replaceWith('');
			var cell_val = cell.html();
			cell_val = cell_val.replace(/\n/g, "");
			cell_val = cell_val.trim();
			cell_val = cell_val.replace(/<br>/g, "\n");
			cell.html("<textarea class='instant_editing' data-type='options'>" + cell_val + "</textarea>")
		});

		$(document).on('blur', '.instant_editing', function(event) {
			var data = $(this).val();
			var global_order_id = $("#global_order_id").val();
			var data_type = $(this).parent('td').attr('data-td-type');
			var product_id = $(this).parents('tr').attr('data-product-id');
			$(this).attr('disabled', true);
			$.ajax({
				url: '/ajax/modify_product/' + data_type, type: 'POST', dataType: 'json',
				data: {product_id: product_id, data: data, order_id: global_order_id},
			})
			.done(function(result) {
				if (result.status != 200) {
					alert(result.info);
				}
			})
			.fail(function() {
				alert("网络遇到问题，请稍后再试！");
			})
			.always(function() {
				window.location.reload();
			});
		});
	});

	var _paq = _paq || [];
	_paq.push(["trackPageView"]);
	_paq.push(["enableLinkTracking"]);

	(function() {
	var u=(("https:" == document.location.protocol) ? "https" : "http") + "://antstats.com/";
	_paq.push(["setTrackerUrl", u+"piwik.php"]);
	_paq.push(["setSiteId", "24"]);
	var d=document, g=d.createElement("script"), s=d.getElementsByTagName("script")[0]; g.type="text/javascript";
	g.defer=true; g.async=true; g.src=u+"piwik.js"; s.parentNode.insertBefore(g,s);
	})();
</script>
</body>
</html>