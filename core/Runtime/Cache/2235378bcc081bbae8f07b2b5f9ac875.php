<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>    <html class="lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>    <html class="lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html>
<!--<![endif]-->
<head>
<title><?php echo ($app_name); ?></title>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<?php  if (APP_DEBUG === true) { ?>
<link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css" />
<link rel="stylesheet" href="/bootstrap/css/bootstrap-responsive.min.css" />
<link rel="stylesheet" href="/bootstrap/extend/jasny-bootstrap/css/jasny-bootstrap.min.css">
<link rel="stylesheet" href="/bootstrap/extend/jasny-bootstrap/css/jasny-bootstrap-responsive.min.css">
<link rel="stylesheet" href="/theme/scripts/jquery-ui-1.9.2.custom/css/smoothness/jquery-ui-1.9.2.custom.min.css" />
<link rel="stylesheet" href="/theme/css/glyphicons.css" />
<link rel="stylesheet" href="/bootstrap/extend/bootstrap-select/bootstrap-select.css" />
<link rel="stylesheet" href="/bootstrap/extend/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
<link rel="stylesheet" href="/theme/scripts/pixelmatrix-uniform/css/uniform.default.css" />
<link rel="stylesheet" href="/theme/scripts/notyfy/jquery.notyfy.css"/>
<link rel="stylesheet" href="/theme/scripts/notyfy/themes/default.css"/>
<link rel="stylesheet" href="/theme/scripts/Gritter/css/jquery.gritter.css"/>
<link rel="stylesheet" href="/theme/scripts/google-code-prettify/prettify.css" type="text/css" />
<link rel="stylesheet" href="/theme/css/style.min.css?1362656609" />
<script type="text/javascript" src="/theme/scripts/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="/theme/scripts/modernizr.custom.76094.js"></script>
<script type="text/javascript" src="/theme/scripts/notyfy/jquery.notyfy.js"></script>
<script type="text/javascript" src="/theme/scripts/Gritter/js/jquery.gritter.min.js"></script>
<script type="text/javascript" src="/theme/scripts/less-1.3.3.min.js"></script>
<?php  } else { ?>
<link rel="stylesheet" href="/bootstrap/??css/bootstrap.min.css,css/bootstrap-responsive.min.css,extend/jasny-bootstrap/css/jasny-bootstrap.min.css,extend/jasny-bootstrap/css/jasny-bootstrap-responsive.min.css,extend/bootstrap-select/bootstrap-select.css,extend/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
<link rel="stylesheet" href="/theme/scripts/??jquery-ui-1.9.2.custom/css/smoothness/jquery-ui-1.9.2.custom.min.css,pixelmatrix-uniform/css/uniform.default.css,notyfy/jquery.notyfy.css,notyfy/themes/default.css,Gritter/css/jquery.gritter.css,google-code-prettify/prettify.css" />
<link rel="stylesheet" href="/theme/css/glyphicons.css" />
<link rel="stylesheet" href="/theme/css/style.min.css?1362656609" />
<script type="text/javascript" src="/theme/scripts/??jquery-1.8.2.min.js,modernizr.custom.76094.js,notyfy/jquery.notyfy.js,Gritter/js/jquery.gritter.min.js,less-1.3.3.min.js"></script>
<?php  } ?>
<link rel="shortcut icon" href="/theme/images/favicon.png">
<style type="text/css">
.table-condensed{font-size: 12px}
.label-cancal{color:#777;background: #333}
.navbar.main .topnav>li.open .advanced-search{width:350px;}
.navbar.main .topnav>li.open .advanced-search input{color:#333;}
.navbar.main .topnav>li.open .advanced-search .control-group{margin:0;}
.navbar.main .topnav>li.open .advanced-search .control-label{width:65px;height:30px;line-height: 30px}
.navbar.main .topnav>li.open .advanced-search .controls{margin-left:85px}
input.nmb,select.nmb,form.nmb{margin-bottom: 0}
.dropdown-menu>li>a{text-align: left}
input[type=text], input[type=password], select, textarea{color:#666;}
div#add_webcate,.lg-modal{width:900px;margin-left: -450px;top:3%;}
#add_account.modal{width:90%;margin-left: -45%;top:3%;}
div#add_webcate div.widget-body{max-height: 500px;overflow-y:auto}
div#change_order_status_modal div.modal-body{max-height: 81%}
div#change_order_status_modal div.modal-body .lg{width:95%;}
div#change_order_status_modal .ems_number_wrap{display: none}
div#change_order_status_modal .ems_number_wrap input{margin-bottom: 0;}
.navbar.main .topnav>li .notif_except li>a.glyphicons.white i:before{color:#fff;}
div.click_go{cursor: pointer;}
.notyfy_error{background-color: #B94A48}
.relative{position: relative}
.hover_display{position: relative}
.cell_modify_button{position: absolute;left: 5px; bottom:3px;}
</style>
</head>
<body>

<!-- Start Content -->
<div class="container-fluid"> 	<div class="navbar main">
		<a href="/" class="appbrand"><span><?php echo ($app_name); ?></span></a>
		<button type="button" class="btn btn-navbar">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<ul class="topnav pull-right">
			<?php if(in_array('view_order_archive', $profile['permissions'])): ?><li><a href="/order/prints" class="glyphicons calendar" target="_blank"><i></i><span class="hidden-phone">发货归档</span></a></li><?php endif; ?>
			<li><a href="/order/search/?order_status=stockout" class="glyphicons circle_remove"><i></i><span class="hidden-phone">缺货</span>(<?php echo ($stockout); ?>)</a></li>
			<li><a href="/order/customs_hold" class="glyphicons skull"><i></i><span class="hidden-phone">海关扣押</span>(<?php echo ($customs_hold); ?>)</a></li>
			<li><a href="/mail/lists?status=failure" class="glyphicons message_ban"><i></i><span class="hidden-phone">失败邮件</span>(<?php echo ($failure_mail); ?>)</a></li>
			<li><a href="/order/beeptimer" class="glyphicons alarm"><i></i><span class="hidden-phone">定时提醒</span>(<?php echo ($beeptimer); ?>)</a></li>
			<?php if(in_array('advanced_search', $profile['permissions'])): ?><li class="dropdown visible-desktop">
				<a href="" data-toggle="dropdown" class="glyphicons search"><i></i>订单搜索 <span class="caret"></span></a>
				<div class="dropdown-menu advanced-search">
					<form class="form-horizontal" id="validateSubmitForm" method="POST" action="/order/search/">
						<div class="control-group">
							<label class="control-label" for="email">邮箱</label>
							<div class="controls"><input placeholder="account@example.com" id="email" name="email" type="text" value="<?php if(!empty($search_condition["email"])): echo ($search_condition["email"]); endif; ?>"></div>
						</div>
						<div class="control-group">
							<label class="control-label" for="uname">姓名</label>
							<div class="controls">
								<input placeholder="姓名" id="uname" name="uname" type="text" value="<?php if(!empty($search_condition["uname"])): echo ($search_condition["uname"]); endif; ?>">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="address">地址</label>
							<div class="controls">
								<input placeholder="客户地址" id="address" name="address" type="text" value="<?php if(!empty($search_condition["address"])): echo ($search_condition["address"]); endif; ?>">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="project_name">产品</label>
							<div class="controls">
								<input placeholder="产品名称" id="project_name" name="project_name" type="text" value="<?php if(!empty($search_condition["project_name"])): echo ($search_condition["project_name"]); endif; ?>">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="order_id">订单号</label>
							<div class="controls"><input placeholder="订单号，不带前缀" id="order_id" name="order_id" value="<?php if(!empty($search_condition["order_id"])): echo ($search_condition["order_id"]); endif; ?>" type="text"></div>
						</div>
						<div class="control-group">
							<label class="control-label" for="order_status">订单状态</label>
							<div class="controls">
								<select id="order_status" name="order_status">
									<option value="">全部订单状态</option>
									<option value="O" <?php if(!empty($search_condition['order_status']) AND $search_condition['order_status'] == 'O'): ?>selected="selected"<?php endif; ?>>已下单</option>
									<option value="Q" <?php if(!empty($search_condition['order_status']) AND $search_condition['order_status'] == 'Q'): ?>selected="selected"<?php endif; ?>>已确认</option>
									<option value="C" <?php if(!empty($search_condition['order_status']) AND $search_condition['order_status'] == 'C'): ?>selected="selected"<?php endif; ?>>已付款</option>
									<option value="ready" <?php if(!empty($search_condition['order_status']) AND $search_condition['order_status'] == 'ready'): ?>selected="selected"<?php endif; ?>>待发货</option>
									<option value="wait" <?php if(!empty($search_condition['order_status']) AND $search_condition['order_status'] == 'wait'): ?>selected="selected"<?php endif; ?>>等货</option>
									<option value="stockout" <?php if(!empty($search_condition['order_status']) AND $search_condition['order_status'] == 'stockout'): ?>selected="selected"<?php endif; ?>>缺货</option>
									<option value="P" <?php if(!empty($search_condition['order_status']) AND $search_condition['order_status'] == 'P'): ?>selected="selected"<?php endif; ?>>已发货</option>
									<option value="I" <?php if(!empty($search_condition['order_status']) AND $search_condition['order_status'] == 'I'): ?>selected="selected"<?php endif; ?>>已取消</option>
									<option value="T" <?php if(!empty($search_condition['order_status']) AND $search_condition['order_status'] == 'T'): ?>selected="selected"<?php endif; ?>>已退款</option>
								</select>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="include_kf_remark">客服备注</label>
							<div class="controls"><input type="checkbox" id="include_kf_remark" name="remark" value="1" <?php if(!empty($search_condition['remark'])): ?>checked="checked"<?php endif; ?>></div>
						</div>
						<?php if(in_array('view_order_price', $profile['permissions'])): ?><div class="control-group">
							<label class="control-label" for="prices">订单金额</label>
							<div class="controls"><input placeholder="5000，或者1000-5000" id="prices" name="prices" type="text" value="<?php if(!empty($search_condition["display_prices"])): echo ($search_condition["display_prices"]); endif; ?>"></div>
						</div><?php endif; ?>
						<div class="control-group">
							<label class="control-label" for="cid">网站分类</label>
							<div class="controls">
								<select name="cid">
									<option value="">选择全部分类</option>
									<?php if(is_array($webcate)): $i = 0; $__LIST__ = $webcate;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$wc): $mod = ($i % 2 );++$i;?><option value="<?php echo ($wc["cid"]); ?>" <?php if(!empty($search_condition['cid']) AND $wc['cid'] == $search_condition['cid']): ?>selected="selected"<?php endif; ?>><?php echo ($wc["cate_name"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
								</select>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="wid">网站</label>
							<div class="controls">
								<select name="wid">
									<option value="">选择全部网站</option>
									<?php if(is_array($website)): $i = 0; $__LIST__ = $website;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$ws): $mod = ($i % 2 );++$i;?><optgroup label="<?php echo ($key); ?>">
										<?php if(is_array($ws)): $i = 0; $__LIST__ = $ws;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?><option value="<?php echo ($v["id"]); ?>" <?php if(!empty($search_condition['wid']) AND $v['id'] == $search_condition['wid']): ?>selected="selected"<?php endif; ?>><?php if(!empty($v["prefix"])): echo ($v["prefix"]); ?> -&nbsp;<?php endif; echo ($v["weburl"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
									</optgroup><?php endforeach; endif; else: echo "" ;endif; ?>
								</select>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="shipping_method">物流方式</label>
							<div class="controls">
								<select name="shipping_method">
									<option value="">选择全部物流</option>
									<option value="EMS" <?php if(!empty($search_condition['shipping_method']) AND 'EMS' == $search_condition['shipping_method']): ?>selected="selected"<?php endif; ?>>EMS</option>
									<option value="DHL" <?php if(!empty($search_condition['shipping_method']) AND 'DHL' == $search_condition['shipping_method']): ?>selected="selected"<?php endif; ?>>DHL</option>
								</select>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="ip">IP</label>
							<div class="controls"><input placeholder="IP地址，例：127.0.0.1" id="ip" name="ip" type="text" value="<?php if(!empty($search_condition["ip"])): echo ($search_condition["ip"]); endif; ?>"></div>
						</div>
						<div class="control-group">
							<label class="control-label" for="first_name">日期</label>
							<div class="controls">
								<input class="input-small" placeholder="开始日期" id="start_date" name="start_date" type="text" value="<?php if(!empty($search_condition["start_date"])): echo (date('Y-m-d',$search_condition["start_date"])); endif; ?>" /> &nbsp;
								<input class="input-small" placeholder="结束日期" id="end_date" name="end_date" type="text" value="<?php if(!empty($search_condition["end_date"])): echo (date('Y-m-d',$search_condition["end_date"])); endif; ?>"/>
							</div>
						</div>
						<div class="control-group">
							<div class="controls">
								<button type="submit" class="btn btn-primary">走你</button>
							</div>
						</div>
					</form>
				</div>
			</li><?php endif; ?>
			<li class="account"> <a data-toggle="dropdown" href="javascript:void(0);" class="glyphicons logout lock"><span class="hidden-phone text"><?php echo ($username); ?></span><i></i></a>
				<ul class="dropdown-menu pull-right">
					<li><a href="javascript:void(0);" class="glyphicons cogwheel">Settings<i></i></a></li>
					<li class="highlight profile"> <span> <span class="heading">Profile <a href="javascript:void(0);" class="pull-right">edit</a></span> <span class="img"></span> <span class="details"> <a href="javascript:void(0);"><?php echo ($username); ?></a> contact@mosaicpro.biz </span> <span class="clearfix"></span> </span> </li>
					<li> <span> <a class="btn btn-default btn-small pull-right" style="padding: 2px 10px; background: #fff;" href="/account/logout">退出登录</a> </span> </li>
				</ul>
			</li>
		</ul>
	</div>
	<div id="wrapper"> 		<div id="menu" class="hidden-phone"> <span class="profile"> <a class="img" href="javascript:void(0);"><img src="/theme/images/photo.gif" alt="<?php echo ($username); ?>" /></a> <span> <strong><?php echo ($username); ?></strong> <a href="javascript:void(0);">edit account</a> </span> </span>
			<div id="search">
				<input type="text" placeholder="Quick search ..." />
				<button class="glyphicons search"><i></i></button>
			</div>
			<ul>
			<?php if(in_array('view_orders', $profile['permissions'])): ?><li class="glyphicons home <?php if($menu == "home"): ?>active<?php endif; ?>"> <a href="/"><i></i><span>首页</span></a></li><?php endif; ?>
			<?php if(in_array('view_orders', $profile['permissions'])): ?><li class="glyphicons list <?php if($menu == "orders"): ?>active<?php endif; ?>"> <a href="/order/lists"><i></i><span>订单列表</span></a></li><?php endif; ?>
			<?php if(in_array('view_orders_analysis', $profile['permissions'])): ?><li class="glyphicons charts <?php if($menu == "analysis"): ?>active<?php endif; ?>"> <a href="/order/analysis"><i></i><span>订单分析</span></a></li><?php endif; ?>
			<?php if(in_array('view_mail_list', $profile['permissions'])): ?><li class="glyphicons message_out <?php if($menu == "mail"): ?>active<?php endif; ?>"> <a href="/mail/lists"><i></i><span>发件箱</span></a></li><?php endif; ?>
			<?php if(in_array('view_mail_in', $profile['permissions'])): ?><li class="glyphicons message_in <?php if($menu == "inbox"): ?>active<?php endif; ?>">
					<a href="/mail/inbox"><i></i><span>收件箱</span></a>
					<span class="count"><?php echo ($inbox_count); ?></span>
				</li><?php endif; ?>
			<?php if(in_array('view_user_analysis', $profile['permissions'])): ?><li class="glyphicons parents <?php if($menu == "user_analysis"): ?>active<?php endif; ?>"> <a href="/order/user_analysis"><i></i><span>用户分析</span></a></li><?php endif; ?>
			<?php if(in_array('view_website', $profile['permissions'])): ?><li class="glyphicons paperclip <?php if($menu == "website"): ?>active<?php endif; ?>"> <a href="/website"><i></i><span>网站接入</span></a></li><?php endif; ?>
			<?php if(in_array('view_webcate', $profile['permissions'])): ?><li class="glyphicons show_big_thumbnails <?php if($menu == "webcate"): ?>active<?php endif; ?>"> <a href="/website/category"><i></i><span>网站分类</span></a></li><?php endif; ?>
			<?php if(in_array('view_account_list', $profile['permissions'])): ?><li class="glyphicons user <?php if($menu == "account"): ?>active<?php endif; ?>"> <a href="/account"><i></i><span>账号管理</span></a></li><?php endif; ?>
			<?php if(in_array('view_recycle', $profile['permissions'])): ?><li class="glyphicons bin <?php if($menu == "recycle"): ?>active<?php endif; ?>"> <a href="<?php echo get_url('order_recycle');?>"><i></i><span>回收站</span></a></li><?php endif; ?>
			</ul>
			<div class="clearfix" style="clear: both"></div>
		</div>
		<div id="content">
			<ul class="breadcrumb">
				<li><a href="/" class="glyphicons home"><i></i> Home</a></li>
				<li class="divider"></li>
				<li><?php echo ($page_title); ?></li>
			</ul>
			<div class="separator bottom"></div>
			<div class="heading-buttons">
				<h3><?php echo ($page_title); ?></h3>
				<div class="clearfix" style="clear: both;"></div>
			</div>
			<div class="separator bottom"></div>
						<style type="text/css">
			.second_filed table{border-top: 1px #d7d8da solid; border-bottom: 1px #d7d8da solid; border-right: 1px #d7d8da solid}
			.subtable .bankcol1 label{display:inline;}
			</style>
			<div class="innerLR">
				<input type="hidden" id="global_order_id" value="<?php echo ($vo["id"]); ?>"/>
				<div class="widget">
					<div class="widget-head"><h4 class="heading"><?php if(!empty($vo['prefix'])): echo ($vo["prefix"]); ?> -<?php endif; echo ($vo["values"]["increment_id"]); ?></h4>
				</div>
				<div class="widget-body">
					<div class="row-fluid">
						<div>
														<style type="text/css">
							.popover-title,.popover-content{text-align: left}
							</style>
							<?php
 $toptic = "<ul><li><a href='?'>查看最新数据</a></li>"; foreach ($order_history as $value) { $times = strtotime($value['pubdate']); if ($value['pubdate'] == $history_pubdate) { $toptic .= "<li><strong>于 <a href='?history={$times}'>{$value['pubdate']}</a> 被修改</strong></li>"; } else { $toptic .= "<li>于 <a href='?history={$times}'>{$value['pubdate']}</a> 被修改</li>"; } } $toptic .= "</ul>"; ?>
							<a href="javascript:;" class="add_product">添加产品</a>&nbsp;
							<a href="javascript:;" class="history_panel_popover" data-toggle="popover" data-content="<?php echo ($toptic); ?>" data-original-title="查看历史记录">查看历史记录</a>
							<script type="text/javascript">
							$(function(){
								$(".history_panel_popover").popover({html:true,});
							});
							</script>
							<table width="100%" class="table table-striped table-bordered table-condensed">
								<tr>
									<td class="center" width="5%"><strong>序号</strong></td>
									<td width="25%"><strong>产品名</strong></td>
									<td width="10%"><strong>产品图片</strong></td>
									<td class="center"><strong>产品属性</strong></td>
									<?php if(in_array('view_order_price', $profile['permissions'])): ?><td class="center" width="10%"><strong>实际金额(单价)</strong></td>
									<td class="center" width="10%"><strong>优惠金额(单价)</strong></td>
									<td class="center" width="10%"><strong>总价格</strong></td><?php endif; ?>
									<td class="center" width="5%"><strong>数量</strong></td>
									<td class="center" width="10%"><strong>编号</strong></td>
									<td class="center" width="5%"><strong>操作</strong></td>
								</tr>
								<?php if(!empty($vo['values']['product']['item_id'])): ?><tr>
									<td class="center" valign="middle">1.</td>
									<td><a href="<?php echo ($vo["values"]["product"]["product_url"]); ?>" target="_blank"><?php echo ($vo["values"]["product"]["name"]); ?></a></td>
									<td class="product_image">
									<?php if(!empty($vo["values"]["product"]["product_image"])): if(strpos($vo['values']['product']['product_image'], 'http') === false): ?><img width="150" src="http://<?php echo ($vo["values"]["domain"]); echo ($vo["values"]["product"]["product_image"]); ?>">
										<?php else: ?>
										<img width="150" src="<?php echo ($vo["values"]["product"]["product_image"]); ?>"><?php endif; ?>
									<?php else: ?>无图像<?php endif; ?>
									</td>
									<td class="center">
										<?php if(!empty($vo["values"]["product"]["product_options"]["options"])): $options = array(); $product_options = $vo['values']['product']['product_options']; foreach ($product_options['options'] as $value) { $options[] = $value['label'] . '：' . $value['value']; } echo join('<br/>', $options); ?>
										<?php else: ?>
										无<?php endif; ?>
									</td>
									<?php if(in_array('view_order_price', $profile['permissions'])): ?><td class="center">¥<?php echo (number_format($vo["values"]["product"]["price"])); ?></td>
									<td class="center">¥<?php echo ($vo['values']['product']['old_price'] - $vo['values']['product']['price']);?></td>
									<td class="center">¥<?php echo number_format($vo['values']['product']['price'] * $vo['values']['product']['qty_ordered'])?></td><?php endif; ?>
									<td class="center"><?php echo ($vo["values"]["product"]["qty_ordered"]); ?></td>
									<td class="center"><?php echo ($vo["values"]["product"]["sku"]); ?></td>
									<td class="center"><a href="javascript:;" class="delete_product">删除</a></td>
								</tr>
								<?php else: ?>
								<?php if(is_array($vo["values"]["product"])): $j = 0; $__LIST__ = $vo["values"]["product"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($j % 2 );++$j;?><tr data-product-id="<?php echo ($v["item_id"]); ?>">
									<td class="center" valign="middle"><?php echo ($j); ?>.</td>
									<td class="product_name"><a href="<?php echo ($v["product_url"]); ?>" target="_blank"><?php echo ($v["name"]); ?></a></td>
									<td class="product_image">
									<?php if(!empty($v["product_image"])): if(strpos($v['product_image'], 'http') === false): ?><img width="150" src="http://<?php echo ($vo["values"]["domain"]); echo ($v["product_image"]); ?>">
										<?php else: ?>
										<img width="150" src="<?php echo ($v["product_image"]); ?>"><?php endif; ?>
									<?php else: ?>无图像<?php endif; ?>
									</td>
									<td class="product_options center">
										<?php if(!empty($v["product_options"]["options"])): if (is_array($v['product_options']['options'])) { $options = array(); $product_options = $v['product_options']; foreach ($product_options['options'] as $value) { $options[] = $value['label'] . '：' . $value['value']; } echo join('<br/>', $options); } else { echo $v['product_options']['options']; } ?>
										<?php else: ?>
										无<?php endif; ?>
									</td>
									<?php if(in_array('view_order_price', $profile['permissions'])): ?><td class="center">¥<?php echo (number_format($v["price"])); ?></td>
									<td class="center">¥<?php echo $v['old_price'] - $v['price'];?></td>
									<td class="center">¥<?php echo number_format($v['price'] * $v['qty_ordered'])?></td><?php endif; ?>
									<td class="product_qty center"><?php echo (number_format($v["qty_ordered"])); ?></td>
									<td class="center"><?php echo ($v["sku"]); ?></td>
									<td class="center">
										<?php if(in_array('modify_order_products', $profile['permissions'])): ?><a href="javascript:;" class="modify_product">修改</a><br><br><?php endif; ?>
										<?php if(in_array('delete_order_products', $profile['permissions'])): ?><a href="javascript:;" class="delete_product">删除</a><?php endif; ?>
									</td>
								</tr><?php endforeach; endif; else: echo "" ;endif; endif; ?>
							</table>
							<div class="separator bottom"></div>
							<table class="table table-striped table-bordered table-condensed">
								<tr>
									<td><u>姓名</u>：</td>
									<td><strong><?php echo ($vo["first_name"]); ?> <?php echo ($vo["last_name"]); ?></strong></td>
								</tr>
								<tr>
									<td><u>邮箱</u>：</td>
									<td><strong><a href="mailto:<?php echo ($vo["email"]); ?>" target="_blank"><?php echo ($vo["email"]); ?></a></strong></td>
								</tr>
								<tr>
									<td><u>地址</u>：</td>
									<td><strong><?php echo ($vo["values"]["shipping_address"]); ?></strong></td>
								</tr>
								<tr>
									<td><u>电话</u>：</td>
									<td><strong><?php echo ($vo["values"]["telephone"]); ?></strong></td>
								</tr>
								<tr>
									<td><u>IP地址</u>：</td>
									<td><a href="http://ip.taobao.com/ipSearch.php?ipAddr=<?php echo ($vo["values"]["remote_ip"]); ?>" target="_blank"><strong><?php echo ($vo["values"]["remote_ip"]); ?></strong></a></td>
								</tr>
								<tr>
									<td>用户备注</td>
									<td style="color:red">
										<?php if(!empty($vo["values"]["comment"]["customercomment"])): echo ($vo["values"]["comment"]["customercomment"]); endif; ?>
										<?php if(!empty($vo["values"]["comment"]["customerfeedback"])): ?>郵便局留＆別のお届け先を指定：<strong><?php echo ($vo["values"]["comment"]["customerfeedback"]); ?></strong><?php endif; ?>
									</td>
								</tr>
							</table>
							<div class="separator bottom"></div>
							<table width="100%" class="table table-bordered table-condensed">
								<tr>
									<td width="10%" class="first_filed">来源</td>
									<td width="90%" class="second_filed"><?php echo ($vo["values"]["domain"]); ?></td>
								</tr>
								<tr>
									<td class="first_filed">订单号</td>
									<td class="second_filed"><?php if(!empty($vo['prefix'])): echo ($vo["prefix"]); ?>-<?php endif; echo ($vo["values"]["increment_id"]); ?></td>
								</tr>
								<tr>
									<td class="first_filed">订单状态</td>
									<td class="second_filed">
										<div class="btn-group">
											<?php if(in_array('change_order_status', $profile['permissions'])): ?><a class="btn <?php echo ($vo["order_status_btn_class"]); ?> dropdown-toggle btn-mini" data-toggle="dropdown" href="#">
											<?php echo ($vo["order_status"]); ?>
											<span class="caret"></span></a>
											<ul class="dropdown-menu order_data" data-order-numid="<?php if(!empty($vo['prefix'])): echo ($vo["prefix"]); ?>-<?php endif; echo ($vo["order_id"]); ?>" data-order-id="<?php echo ($vo["id"]); ?>" data-price="<?php echo (number_format($vo["prices"])); ?>" data-domain="<?php echo ($vo["domain"]); ?>" data-smtp="<?php echo ($vo["smtp"]); ?>" data-account-name="<?php echo ($vo["account_name"]); ?>" data-username="<?php echo ($vo["first_name"]); ?> <?php echo ($vo["last_name"]); ?>" data-email="<?php echo ($vo["email"]); ?>" data-date="<?php echo (date('Y-m-d H:i:s', $vo["timestamp"])); ?>" data-press="<?php echo ($vo["press"]); ?>">
												<li><a href="javascript:;" class="change_status_link" data-status="O">已下单</a></li>
												<li><a href="javascript:;" class="change_status_link" data-status="Q">已确认</a></li>
												<li><a href="javascript:;" class="change_status_link" data-status="C">已付款</a></li>
												<li><a href="javascript:;" class="change_status_link" data-status="ready">待发货</a></li>
												<li><a href="javascript:;" class="change_status_link" data-status="wait">等货</a></li>
												<li><a href="javascript:;" class="change_status_link" data-status="stockout">缺货</a></li>
												<li><a href="javascript:;" class="change_status_link" data-status="P">已发货</a></li>
												<li><a href="javascript:;" class="change_status_link" data-status="I">已取消</a></li>
												<li><a href="javascript:;" class="change_status_link" data-status="T">已退款</a></li>
											</ul>
											<?php else: ?>
											<span class="label <?php echo ($vo["order_status_label_class"]); ?>"><?php echo ($vo["order_status"]); ?></span><?php endif; ?>
										</div>&nbsp;&nbsp;&nbsp;&nbsp;
										<?php if($vo['customs_hold'] == 1): ?><span class="label btn-inverse">已被扣关</span><?php endif; ?>
									</td>
								</tr>
								<tr>
									<td class="first_filed">催款</td>
									<td class="second_filed">
										<?php if($vo["press"] == 0): ?><span class="label label-default">0</span>
										<?php else: ?>
											<span class="label label-success"><?php echo ($vo["press"]); ?></span><?php endif; ?>
										<?php if(in_array('send_press_mail', $profile['permissions'])): if($vo['order_status'] == '已确认'): ?>&nbsp;&nbsp;<a href="javascript:;" class="change_status_link" data-status="press">发送催款邮件</a><?php endif; endif; ?>
									</td>
								</tr>
								<tr>
									<td class="first_filed">客服备注</td>
									<td class="second_filed">
										<?php if(!empty($order_remark)): ?><table class="table table-striped table-bordered table-condensed">
											<tr>
												<td><strong>备注日期</strong></td>
												<td><strong>备注内容</strong></td>
												<td><strong>提醒</strong></td>
												<td><strong>提醒日期</strong></td>
											</tr>
											<?php if(is_array($order_remark)): $i = 0; $__LIST__ = $order_remark;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i; if($i == 1): ?><tr class="text-error">
												<td width="20%"><strong><?php echo ($v["pubdate"]); ?></strong></td>
												<td width="60%"><strong><?php echo ($v["remark"]); ?></strong></td>
												<td width="5%"><strong><?php if(empty($v["beeptimer"])): ?><span class="label">否</span><?php else: ?><span class="label label-important">是</span><?php endif; ?></strong></td>
												<td width="15%"><strong><?php echo ($v["beeptimer_date"]); ?></strong></td>
											</tr>
											<?php else: ?>
											<tr>
												<td width="20%"><?php echo ($v["pubdate"]); ?></td>
												<td width="60%"><?php echo ($v["remark"]); ?></td>
												<td width="5%"><?php if(empty($v["beeptimer"])): ?><span class="label">否</span><?php else: ?><span class="label label-important">是</span><?php endif; ?></td>
												<td width="15%"><?php echo ($v["beeptimer_date"]); ?></td>
											</tr><?php endif; endforeach; endif; else: echo "" ;endif; ?>
										</table><?php endif; ?>
										<a href="javascript:;" data-order-id="<?php echo ($vo["id"]); ?>" class="glyphicons edit publish_remark" style="padding-left:25px;color:#47759e"><i></i>写备注</a>
									</td>
								</tr>
								<tr>
									<td class="first_filed">下单日期</td>
									<td class="second_filed"><?php echo (date('Y-m-d H:i:s', $vo["timestamp"])); ?></td>
								</tr>
								<?php if(in_array('view_order_price', $profile['permissions'])): ?><tr>
									<td class="first_filed">总金额(含运费)</td>
									<td class="second_filed"><strong>¥<?php echo (number_format($vo["values"]["grand_total"])); ?></strong></td>
								</tr>
								<tr>
									<td class="first_filed">运费金额</td>
									<td class="second_filed"><strong>¥<?php echo (number_format($vo["values"]["shipping_amount"])); ?></strong></td>
								</tr>
								<tr>
									<td class="first_filed">优惠总金额</td>
									<td class="second_filed"><strong>¥<?php echo (number_format($vo["values"]["discount_amount"])); ?></strong></td>
								</tr><?php endif; ?>
								<tr>
									<td class="first_filed">支付方式</td>
									<td class="second_filed">銀行振込</td>
								</tr>
								<tr>
									<td class="first_filed">银行信息</td>
									<td class="second_filed subtable"><?php echo ($vo["values"]["payment_info"]); ?></td>
								</tr>
								<tr>
									<td class="first_filed">物流方式</td>
									<td class="second_filed"><?php echo ($vo["values"]["shipping_method"]); ?></td>
								</tr>
							</table>
						</div>
					</div>
				</div>
			</div>
							<div id="change_order_status_modal" class="modal hide fade">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						<h3 class="heading">更改订单状态</h3>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="span5">
								<p><strong>订单详情</strong></p>
								<p>
									<table class="table order_info">
										<tr>
											<td>订单号</td>
											<td class="oi_order_id">&nbsp;</td>
										</tr>
										<tr>
											<td>姓名</td>
											<td class="oi_username">&nbsp;</td>
										</tr>
										<tr>
											<td>网站</td>
											<td class="oi_website">&nbsp;</td>
										</tr>
										<tr>
											<td>金额</td>
											<td class="oi_price">&nbsp;</td>
										</tr>
										<tr>
											<td>邮箱</td>
											<td class="oi_email">&nbsp;</td>
										</tr>
										<tr>
											<td>邮箱检查</td>
											<td class="oi_message">&nbsp;</td>
										</tr>
										<tr>
											<td>状态</td>
											<td class="oi_status">&nbsp;</td>
										</tr>
										<tr>
											<td>时间</td>
											<td class="oi_date">&nbsp;</td>
										</tr>
									</table>
								</p>
							</div>
							<div class="span7">
								<p><span class="text-error">动作：<span class="action-text"></span></span> &nbsp;</p>
								<p class="ems_number_wrap">EMS号：</p>
								<p class="ems_number_wrap"><input type="text" class="ems_number lg" placeholder="EMS号写在这里，多个用英文空格隔开。邮件正文的星号无需更改，程序自动替换，否则会出错"></p>
								<p>邮件标题：</p>
								<p><input type="text" class="mail_title lg" placeholder="这里填写邮件标题"></p>
								<p>邮件内容：</p>
								<p><textarea class="mail_content lg" placeholder="系统发送给客户的邮件内容"></textarea></p>
								<p>备注：</p>
								<p><textarea class="change_status_remark lg" placeholder="备注，为本次状态更改登记描述（非必填）"></textarea></p>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<span class="change_status_message"></span>
						<button class="btn btn-primary" id="change_status_affirm" data-order-id="0" data-order-status="">确认更改</button>
						<button class="btn" data-dismiss="modal" aria-hidden="true">关闭</button>
					</div>
				</div>
				<script type="text/javascript">
				$(function(){
					$("a.change_status_link").on("click", function() {
						if ($(this).parent('li').hasClass('disabled')) {
							return false;
						} else {
							var winwidth = $(window).width();
							var winheight = $(window).height();
							var status = $(this).attr('data-status');
							if ($(this).parents("ul.order_data").length > 0) {
								var order_data = $(this).parents("ul.order_data");
							} else {
								var order_data = $("ul.order_data");
							}
							var order_id = order_data.attr('data-order-id');
							var order_numid = order_data.attr('data-order-numid');
							var des_status = $(this).html();
							var src_status = order_data.siblings('a').text();
							var smtp_info = order_data.attr('data-smtp');
							var order_price = order_data.attr('data-price');
							var order_domain = order_data.attr('data-domain');
							var account_name = order_data.attr('data-account-name');
							var press_count = order_data.attr('data-press');
							var username = order_data.attr('data-username');
							var useremail = order_data.attr('data-email');
							var order_date = order_data.attr('data-date');
							var cosm = $("#change_order_status_modal");
							var table_order_info = cosm.find("table.order_info");
							var modalbody = cosm.find('div.modal-body');
							var mail_content = cosm.find('textarea.mail_content');
							var oi_message = table_order_info.find("td.oi_message");
							var not_send_mail_status = <?php echo ($not_send_mail_status); ?>;
							cosm.find('input.mail_title').attr("disabled", false).val("");
							cosm.find('textarea.mail_content').attr("disabled", false).val("");
							$('#change_status_affirm').attr("disabled", true);
							cosm.css({"width":winwidth * 0.96, "margin-left":-(winwidth * 0.48), "max-height":winheight * 0.95, "top":winheight * 0.03, "height":winheight * 0.95});
							modalbody.height(winheight * 0.95);
							if (des_status == '已发货') {
								mail_content.height(winheight * 0.32);
							} else {
								mail_content.height(winheight * 0.4);
							}
							
							table_order_info.find("td.oi_order_id").html(order_numid);
							table_order_info.find("td.oi_username").html(username);
							table_order_info.find("td.oi_website").html(order_domain);
							table_order_info.find("td.oi_price").html("¥"+order_price);
							table_order_info.find("td.oi_email").html("<input type='text' name='useremail' class='input_oi_email' value='"+useremail+"'>");
							table_order_info.find("td.oi_status").html(src_status);
							table_order_info.find("td.oi_date").html(order_date);

							if (not_send_mail_status.indexOf(des_status) == -1) {
								oi_message.html("正在检查电子邮箱有效性... <img src='/theme/images/loading.gif' width='18'>");
							}

							if (not_send_mail_status.indexOf(des_status) == -1 && smtp_info != '1') {
								if (modalbody.find('.smtp-error').length > 0) {
									modalbody.find('.smtp-error').html('警告！网站未设置SMTP邮件服务，无法更改订单状态。<a href="/website">到网站页面设置SMTP');
								} else {
									modalbody.prepend('<div class="alert alert-error smtp-error">警告！网站未设置SMTP邮件服务，无法更改订单状态。<a href="/website">到网站页面设置SMTP</a>。</div>');
								}
								cosm.find('#change_status_affirm').attr("disabled", true);
							} else {
								modalbody.find('.smtp-error').remove();

								if (modalbody.find('.load_mail_temp').length > 0) {
									modalbody.find('.load_mail_temp').html('正在加载邮件模版...');
								} else {
									modalbody.prepend('<div class="alert load_mail_temp">正在加载邮件模版...</div>');
								}

								if (not_send_mail_status.indexOf(des_status) == -1) {
									$.ajax({
										url: '/ajax/get_mail_temp', type: 'GET', dataType: 'json',
										data: {order_id: order_id, des_status:des_status, account_name:account_name},
										success:function(result){
											console.log(not_send_mail_status.indexOf(des_status));
											if (not_send_mail_status.indexOf(des_status) == -1) {
												modify_validate_message(result);
											};
											if (result.status == 200) {
												cosm.find('input.mail_title').val(result.mail_title);
												mail_content.val(result.mail_content);
												modalbody.find('.load_mail_temp').remove();

												if (des_status == '已发货') {
													$(".ems_number_wrap").show();
												} else {
													$(".ems_number_wrap").hide();
												}
											} else {
												cosm.find('input.mail_title').val('');
												mail_content.val('');
												cosm.find('div.modal-body').find('.load_mail_temp').html(result.info);
											}
											cosm.find('#change_status_affirm').attr("disabled", false);
										}
									});
								} else {
									$("input.input_oi_email").attr('disabled', true).val("此状态无需发邮件");
									modalbody.find('.load_mail_temp').remove();
									cosm.find('input.mail_title').attr('disabled', true).val("此状态无需发邮件");
									cosm.find('textarea.mail_content').attr('disabled', true).val("此状态无需发邮件");
									cosm.find('#change_status_affirm').attr("disabled", false);
									table_order_info.find("td.oi_message").html("此状态无需发邮件");
								}
							}

							// cosm.find('em.src_status').html(src_status);
							// cosm.find('em.des_status').html(des_status);
							if (status != "press") {
								cosm.find('span.action-text').html('<strong>【<em>'+src_status+'</em>】</strong> 更改为 <strong>【<em>'+des_status+'</em>】</strong>');
							} else {
								press_count++;
								cosm.find('span.action-text').html('发送第&nbsp;<strong><em>'+press_count+'</em></strong>&nbsp;次催款邮件');
							}

							cosm.find('#change_status_affirm').attr({
								"data-order-id": order_id,
								"data-order-status": status
							});
							cosm.find('input.mail_title').attr('data-account-name', account_name);
							cosm.modal();
						}
					});

					$("input.input_oi_email").on("change", function(event){
						var msg = "";
						var oi_message = $("#change_order_status_modal").find("table.order_info").find("td.oi_message");
						oi_message.html("正在检查电子邮箱有效性... <img src='/theme/images/loading.gif' width='18'>");
						$.ajax({
							url: '/ajax/validate_email', type: 'GET', dataType: 'json',
							data:{"email":$(this).val(), "return":false},
							success:function(result){
								if (result.status == 200) {
									modify_validate_message(result);
								};
							}
						});
					});

					$("#change_status_affirm").click(function(event) {
						var cosm = $("#change_order_status_modal");
						var remark = cosm.find('textarea.change_status_remark');
						var account_name = cosm.find('input.mail_title').attr('data-account-name');
						var mail_title = cosm.find('input.mail_title');
						var mail_content = cosm.find('textarea.mail_content');
						var message = cosm.find('.change_status_message');
						var order_status = $(this).attr('data-order-status');
						var order_id = $(this).attr('data-order-id');
						var ems_number = cosm.find("input.ems_number").val();
						var input_oi_email = "";
						if ($(".input_oi_email").attr("disabled") != true) {
							input_oi_email = $(".input_oi_email").val();
						};

						if (order_status == 'P' && ems_number == '') {
							alert("请输入EMS号。");
							cosm.find("input.ems_number").focus();
							return false;
						};

						$(this).attr('disabled', true).html('正在执行...');

						$.post('/ajax/change_order_status', {"order_id":order_id, "order_status":order_status, "account_name":account_name, "mail_title":mail_title.val(), "mail_content":mail_content.val(), "remark":remark.val(), "ems_number":ems_number, "input_oi_email":input_oi_email}, function(data) {
							if (data.status == 200) {
								message.attr('class', 'change_status_message text-success').html('<strong>操作成功！</strong>订单状态已经更改。').show();
								setTimeout(function(){
									window.location.reload();
								}, 500);
							} else {
								$(this).attr('disabled', false).html('确认更改');
								message.attr('class', 'change_status_message text-error').html('<strong>操作失败！</strong>遇到致命错误，请刷新页面重试。' + data.info).show();
							}
						}, 'json');
					});
				});

				function modify_validate_message(result) {
					var msg = "";
					var mgve = result.mg_validate_email;
					var oi_message = $("#change_order_status_modal").find("table.order_info").find("td.oi_message");
					if (mgve.http_response_code == 200) {
						var hrb = mgve.http_response_body;
						if (hrb.is_valid == true) {
							msg = "<span class='label label-success'>邮箱格式检查通过！</span>";
							$("input.input_oi_email").attr("disabled", true);
						} else {
							msg = "<span class='label label-important'>邮件地址有误，请手动填写或电话联系！</span>";
						}
						if (hrb.did_you_mean != null) {
							msg += "<br>推荐地址："+hrb.did_you_mean;
						}
						if (hrb.parts.display_name != null) {
							msg += "<br>Display Name:"+hrb.parts.display_name;
						};
						if (hrb.parts.domain != null) {
							msg += "<br>运营商:"+hrb.parts.domain;
						};
						if (hrb.parts.local_part != null) {
							msg += "<br>邮箱名:"+hrb.parts.local_part;
						};
					} else {
						msg = "<span class='label label-warning'>邮箱地址检查失败！</span>";
					}
					oi_message.html(msg);
				}
				</script>
							<div id="publish_remark_modal" class="modal hide fade">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						<h3 class="heading">撰写备注</h3>
					</div>
					<div class="modal-body">
						<input type="hidden" class="remark_order_id" value="">
						<p><textarea class="remark_content" style="width:95%" placeholder="备注内容"></textarea></p>
						<p><label class="checkbox"><input type="checkbox" class='beeptimer' name="beeptimer" value="yes"> 提醒催款？(以最后一次为准，之前设置的时间将被覆盖)</label></p>
						<div class="control-group">
							<div class="controls">
								<input class="span1 beeptimer_num" disabled type="text" value="0">
								<span class="help-inline">天后提醒：<span class="preview_beeptimer"><?php echo date('Y-m-d');?></span></span>
							</div>
						</div>
						<p id="publish_remark_message"></p>
					</div>
					<div class="modal-footer">
						<button class="btn btn-primary" id="publish_remark_affirm" data-order-id="0" autocomplete="off" data-order-status="">保存备注</button>
						<button class="btn" data-dismiss="modal" aria-hidden="true">关闭</button>
					</div>
				</div>
				<script type="text/javascript">
				Date.prototype.Format = function(fmt) {
					var o = {
						"M+" : this.getMonth()+1,
						"d+" : this.getDate(),
						"h+" : this.getHours(),
						"m+" : this.getMinutes(),
						"s+" : this.getSeconds(),
						"q+" : Math.floor((this.getMonth()+3)/3),
						"S"  : this.getMilliseconds()
					}; 
					if(/(y+)/.test(fmt)) 
						fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length)); 
					for(var k in o) {
						if(new RegExp("("+ k +")").test(fmt)) {
							fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));
						}
					}
					return fmt; 
				}

				$(function(){
					var pr_message = $("#publish_remark_message");
					$("a.publish_remark").on("click", function() {
						var pprm = $("#publish_remark_modal");
						var order_id = $(this).attr('data-order-id');
						$("input.remark_order_id").val(order_id);
						pprm.modal();
					});

					$("input.beeptimer").on('change', function(event) {
						if ($(this).attr('checked') == 'checked') {
							$("input.beeptimer_num").attr('disabled', false).focus();
						} else {
							$("input.beeptimer_num").attr('disabled', true).val('0');
						}
					});

					$("input.beeptimer_num").on('change', function(event) {
						var beeptimer_num = $("input.beeptimer_num").val();
						if (!isNaN(beeptimer_num)) {
							var current = new Date();
							var des_date = new Date( ( current / 1000 + 86400 * beeptimer_num ) * 1000 );
							var des_format = des_date.Format("yyyy-MM-dd");
							pr_message.html("");
							$("span.preview_beeptimer").html(des_format);
						} else {
							pr_message.attr('class', '').addClass('text-error').html("错误：天数输入纯数字");
						}
					});

					$("#publish_remark_affirm").on('click', function(event) {
						var remark_content = $("textarea.remark_content").val();
						var beeptimer = $("input.beeptimer:checked").val();
						var beeptimer_num = $("input.beeptimer_num").val();
						var order_id = $("input.remark_order_id").val();
						var publish_remark_affirm = $(this);
						if (remark_content == '' || order_id == 0) {
							pr_message.attr('class', '').addClass('text-error').html("错误：备注内容或ID为空");
							return false;
						};
						publish_remark_affirm.attr('disabled', true);
						$.ajax({
							url: '/ajax/publish_order_remark', type: 'POST', dataType: 'json',
							data: {remark_content: remark_content, beeptimer: beeptimer, beeptimer_num: beeptimer_num, order_id: order_id},
						})
						.done(function(result) {
							if (result.status == 200) {
								pr_message.attr('class', '').addClass('text-success').html("成功：备注保存成功");
								$("textarea.remark_content").val('');
								$("input.beeptimer").attr('disabled', false);
								$("input.beeptimer_num, input.remark_order_id").val('0');
								window.location.reload();
							}
						})
						.fail(function() {
							publish_remark_affirm.attr('disabled', false);
							pr_message.attr('class', '').addClass('text-error').html("错误：备注保存失败");
						});
					});
				});
			</script>
							<div id="add_product" class="modal hide fade lg-modal">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						<h3 class="heading">添加产品</h3>
					</div>
					<div class="modal-body">
						<table class="table">
							<tr>
								<td width="20%">标题：</td>
								<td><input type="text" class="product_title span5" placeholder="填写商品标题"></td>
							</tr>
							<tr>
								<td>图片地址：</td>
								<td><input type="text" class="product_image span5" placeholder="填写图片地址"></td>
							</tr>
							<tr>
								<td>属性：</td>
								<td><textarea class="product_options span5" placeholder="填写商品属性"></textarea></td>
							</tr>
							<tr>
								<td>实际金额：</td>
								<td><input type="text" class="product_sjje span5" placeholder="填写商品实际金额"></td>
							</tr>
							<tr>
								<td>优惠金额：</td>
								<td><input type="text" class="product_yhje span5" placeholder="填写商品优惠金额"></td>
							</tr>
							<tr>
								<td>数量：</td>
								<td><input type="text" class="product_qty span5" placeholder="填写商品数量"></td>
							</tr>
							<tr>
								<td>编号：</td>
								<td><input type="text" class="product_bh span5" placeholder="填写商品编号"></td>
							</tr>
						</table>
					</div>
					<div class="modal-footer">
						<span class="message"></span>
						<button class="btn btn-primary" id="add_product_affirm">确定添加</button>
						<button class="btn" data-dismiss="modal" aria-hidden="true">关闭</button>
					</div>
				</div>
				<div id="modify_product" class="modal hide fade lg-modal">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						<h3 class="heading">修改产品</h3>
					</div>
					<div class="modal-body">
						<table class="table">
							<tr>
								<td width="20%">标题：</td>
								<td class="product_name text-error"></td>
							</tr>
							<tr>
								<td>图片地址：</td>
								<td><input type="text" class="product_image span5"></td>
							</tr>
							<tr>
								<td>属性：</td>
								<td><textarea class="product_options span5"></textarea></td>
							</tr>
							<tr>
								<td>数量：</td>
								<td><input type="text" class="product_qty span5"></td>
							</tr>
						</table>
					</div>
					<div class="modal-footer">
						<span class="message"></span>
						<button class="btn btn-primary" id="modify_product_affirm">确定修改</button>
						<button class="btn" data-dismiss="modal" aria-hidden="true">关闭</button>
					</div>
				</div>
				<div id="delete_product" class="modal hide fade">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						<h3 class="heading">删除产品</h3>
					</div>
					<div class="modal-body">
						<p>你确定要删除此产品咩？</p>
					</div>
					<div class="modal-footer">
						<button class="btn" id="delete_product_affirm">确定删除，并保证3天不后悔</button>
						<button class="btn" data-dismiss="modal" aria-hidden="true">关闭</button>
					</div>
				</div>
				<script type="text/javascript">
				$(function(){
					var add_product_affirm = $("#add_product_affirm");
					var modify_product_affirm = $("#modify_product_affirm");
					var add_product_modal = $('#add_product');
					var modify_product_modal = $('#modify_product');

					$("a.add_product").bind("click", function(){
						add_product_modal.modal("show");
					});

					add_product_affirm.bind("click", function() {
						$(this).attr('disabled', true);
						var mbody = add_product_modal.find('.modal-body');
						var global_order_id = $("#global_order_id").val();
						var product_title = mbody.find('.product_title').val();
						var product_image = mbody.find('.product_image').val();
						var product_options = mbody.find('.product_options').val();
						var product_sjje = mbody.find('.product_sjje').val();
						var product_yhje = mbody.find('.product_yhje').val();
						var product_qty = mbody.find('.product_qty').val();
						var product_bh = mbody.find('.product_bh').val();
						var mbody = $('#delete_product').find('.modal-body');
						$.post('/ajax/add_product',{product_title:product_title,product_image:product_image,product_options:product_options,product_qty:product_qty,order_id: global_order_id,product_sjje:product_sjje,product_yhje:product_yhje,product_bh:product_bh},function(resdata) {
							if (resdata.status == 200) {
								add_product_modal.find('.message').attr('class', 'message text-success').html('添加成功！');
								setTimeout(function() {
									window.location.reload();
								}, 500);
							} else {
								add_product_affirm.attr('disabled', false);
								add_product_modal.find('.message').attr('class', 'message text-error').html('遇到错误：' + resdata.info);
								if (resdata.status == 104) {
									setTimeout(function() {
										window.location.href="/";
									}, 2000);
								};
							}
						}, 'json');
					});

					$("a.modify_product").bind("click", function(){
						modify_product_modal.modal('show');
						var product_id = $(this).parents("tr").attr('data-product-id');
						var mbody = modify_product_modal.find('.modal-body');
						var product_name = $(this).parent().siblings('.product_name').find('a').html();
						var product_options = $(this).parent().siblings('.product_options').html();

						product_options = product_options.replace(/\n/g, "");
						product_options = product_options.trim();
						product_options = product_options.replace(/<br>/g, "\n");

						mbody.find('.product_name').html(product_name);
						mbody.find('.product_image').val($(this).parent().siblings('.product_image').find('img').attr('src'));
						mbody.find('.product_options').html(product_options);
						mbody.find('.product_qty').val($(this).parent().siblings('.product_qty').html());
						modify_product_affirm.attr('data-product-id', product_id);
					});

					modify_product_affirm.bind("click", function() {
						$(this).attr('disabled', true);
						var mbody = modify_product_modal.find('.modal-body');
						var product_id = $(this).attr('data-product-id');
						var global_order_id = $("#global_order_id").val();
						var product_image = mbody.find('.product_image').val();
						var product_options = mbody.find('.product_options').val();
						var product_qty = mbody.find('.product_qty').val();
						var mbody = $('#delete_product').find('.modal-body');
						$.post('/ajax/modify_product',{product_id:product_id, product_image:product_image, product_options:product_options, product_qty:product_qty, order_id: global_order_id}, function(resdata) {
							if (resdata.status == 200) {
								modify_product_modal.find('.message').attr('class', 'message text-success').html('修改成功！');
								setTimeout(function() {
									window.location.reload();
								}, 500);
							} else {
								modify_product_affirm.attr('disabled', false);
								modify_product_modal.find('.message').attr('class', 'message text-error').html('遇到错误：' + resdata.info);
								if (resdata.status == 104) {
									setTimeout(function() {
										window.location.href="/";
									}, 2000);
								};
							}
						}, 'json');
					});

					$("a.delete_product").bind("click", function(){
						$("#delete_product").modal('show');
						var product_id = $(this).parents("tr").attr('data-product-id');
						var delete_product_affirm = $("#delete_product_affirm");
						var mbody = $('#delete_product').find('.modal-body');
						var product_name = $(this).parent().siblings('.product_name').find('a').html();

						mbody.html('<p>你确定要删除此产品咩？</p><p class="text-error"><strong>' + product_name + '</strong></p>');
						delete_product_affirm.attr('data-product-id', product_id);
					});

					$("#delete_product_affirm").bind("click", function() {
						$(this).attr('disabled', true);
						var product_id = $(this).attr('data-product-id');
						var global_order_id = $("#global_order_id").val();
						var mbody = $('#delete_product').find('.modal-body');
						mbody.html('正在删除请稍后......');
						$.post('/ajax/modify_product/delete_product',{product_id:product_id, order_id: global_order_id}, function(resdata){
							if (resdata.status == 200) {
								mbody.html("<span style='color:green'>删除成功！</span>");
								setTimeout(function(){
									window.location.reload();
								}, 500);
							} else {
								$("#delete_product_affirm").attr('disabled', false);
								mbody.html('删除遇到错误：<p style="color:red">'+resdata.info+'</p>');
								if (resdata.status == 104) {
									setTimeout(function(){
										window.location.href="/";
									},2000);
								};
							}
						}, 'json');
					});
				});
			</script>
		</div>
		<!-- End Wrapper --> 
	</div>
</div>

<!-- JQueryUI v1.9.2 --> 
<script src="/theme/scripts/jquery-ui-1.9.2.custom/js/jquery-ui-1.9.2.custom.min.js"></script>
<script src="/theme/scripts/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>
<script src="/theme/scripts/jquery.cookie.js"></script>
<script src="/theme/scripts/jquery.ba-resize.js"></script>
<script src="/theme/scripts/pixelmatrix-uniform/jquery.uniform.min.js"></script>
<script src="/bootstrap/js/bootstrap.min.js"></script>
<script src="/bootstrap/extend/bootstrap-select/bootstrap-select.js"></script>
<script src="/bootstrap/extend/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
<script src="/bootstrap/extend/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js"></script>
<script src="/bootstrap/extend/jasny-bootstrap/js/jasny-bootstrap.min.js" type="text/javascript"></script>
<script src="/bootstrap/extend/bootbox.js" type="text/javascript"></script>
<script src="/theme/scripts/load.js"></script>
<script type="text/javascript">
	$(function(){
		$("div.auto-height.modal").on("show", function(){
			var data_width = $(this).attr("data-width");
			var mar_left = data_width / 2;
			var winheight = $(window).height();
			var modalheight = winheight * 0.77;
			$(this).css({"top":"3%", "width":data_width+"%", "margin-left":"-"+mar_left+"%"});
			$(this).find(".modal-body").css({"max-height":modalheight});
		});

		$("a#tgbl").click(function(event){
			if (<?php echo ($customs_hold); ?> > 0 && $(this).attr("data-shown") != 'true') {
				var n = notyfy({
					text: '<h4 style="color:#fff">海关扣押!</h4> <p>当前有<?php echo ($customs_hold); ?>个订单被海关扣押！<a href="/order/customs_hold" style="color:#000">点击这里查看。</a></p>',
					type: 'error', layout: 'bottomRight', theme: 'boolight', closeWith: ['click'],
					hideEffect: function(bar) {
						bar.animate({ height: 'toggle' }, 500, 'swing');
						$("a#tgbl").attr("data-shown", 'false');
					}
				});
				$(this).attr("data-shown", 'true');
			}
		});
		
		$("a#failure_mail").click(function(event){
			if (<?php echo ($failure_mail); ?> > 0 && $(this).attr("data-shown") != 'true') {
				var n = notyfy({
					text: '<h4 style="color:#826200">失败邮件!</h4> <p>当前有<?php echo ($failure_mail); ?>封邮件发送失败！<a href="/mail/lists?status=failure" style="color:#000">点击这里查看。</a></p>',
					type: 'warning', layout: 'bottomRight', theme: 'boolight', closeWith: ['click'],
					hideEffect: function(bar) {
						bar.animate({ height: 'toggle' }, 500, 'swing');
						$("a#failure_mail").attr("data-shown", 'false');
					}
				});
				$(this).attr("data-shown", 'true');
			}
		});

		$("a#beeptimer").click(function(event){
			if (<?php echo ($beeptimer); ?> > 0 && $(this).attr("data-shown") != 'true') {
				var n = notyfy({
					text: '<h4 style="color:darkgreen">定时提醒!</h4> <p>当前有<?php echo ($beeptimer); ?>个订单提醒！<a href="/order/beeptimer" style="color:#000">点击这里查看。</a></p>',
					type: 'success', layout: 'bottomRight', theme: false, closeWith: ['click'],
					hideEffect: function(bar) {
						bar.animate({ height: 'toggle' }, 500, 'swing');
						$("a#beeptimer").attr("data-shown", 'false');
					}
				});
				$(this).attr("data-shown", 'true');
			}
		});

		$("div.click_go").click(function(event){
			var href = $(this).attr("data-href");
			window.location.href=href;
		});

		$(".tooltips").tooltip({html:true});

		$(".hover_display").hover(function() {
			$(this).find('.cell_modify_button').removeClass('hidden');
		}, function() {
			$(this).find('.cell_modify_button').addClass('hidden');
		});

		$(".cell_modify_button").on('click', function(event) {
			var cell = $(this).parent("td");
			var brreg = new RegExp("");
			$(this).replaceWith('');
			var cell_val = cell.html();
			cell_val = cell_val.replace(/\n/g, "");
			cell_val = cell_val.trim();
			cell_val = cell_val.replace(/<br>/g, "\n");
			cell.html("<textarea class='instant_editing' data-type='options'>" + cell_val + "</textarea>")
		});

		$(document).on('blur', '.instant_editing', function(event) {
			var data = $(this).val();
			var global_order_id = $("#global_order_id").val();
			var data_type = $(this).parent('td').attr('data-td-type');
			var product_id = $(this).parents('tr').attr('data-product-id');
			$(this).attr('disabled', true);
			$.ajax({
				url: '/ajax/modify_product/' + data_type, type: 'POST', dataType: 'json',
				data: {product_id: product_id, data: data, order_id: global_order_id},
			})
			.done(function(result) {
				if (result.status != 200) {
					alert(result.info);
				}
			})
			.fail(function() {
				alert("网络遇到问题，请稍后再试！");
			})
			.always(function() {
				window.location.reload();
			});
		});
	});

	var _paq = _paq || [];
	_paq.push(["trackPageView"]);
	_paq.push(["enableLinkTracking"]);

	(function() {
	var u=(("https:" == document.location.protocol) ? "https" : "http") + "://antstats.com/";
	_paq.push(["setTrackerUrl", u+"piwik.php"]);
	_paq.push(["setSiteId", "24"]);
	var d=document, g=d.createElement("script"), s=d.getElementsByTagName("script")[0]; g.type="text/javascript";
	g.defer=true; g.async=true; g.src=u+"piwik.js"; s.parentNode.insertBefore(g,s);
	})();
</script>
</body>
</html>