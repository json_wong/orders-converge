<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>    <html class="lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>    <html class="lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html>
<!--<![endif]-->
<head>
<title><?php echo ($app_name); ?></title>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css" />
<link rel="stylesheet" href="/bootstrap/css/bootstrap-responsive.min.css" />
<link rel="stylesheet" href="/bootstrap/extend/jasny-bootstrap/css/jasny-bootstrap.min.css">
<link rel="stylesheet" href="/bootstrap/extend/jasny-bootstrap/css/jasny-bootstrap-responsive.min.css">
<link rel="stylesheet" href="/theme/scripts/jquery-ui-1.9.2.custom/css/smoothness/jquery-ui-1.9.2.custom.min.css" />
<link rel="stylesheet" href="/theme/css/glyphicons.css" />
<link rel="stylesheet" href="/bootstrap/extend/bootstrap-select/bootstrap-select.css" />
<link rel="stylesheet" href="/bootstrap/extend/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
<link rel="stylesheet" href="/theme/scripts/pixelmatrix-uniform/css/uniform.default.css" />
<link rel="stylesheet" href="/theme/scripts/notyfy/jquery.notyfy.css"/>
<link rel="stylesheet" href="/theme/scripts/notyfy/themes/default.css"/>
<link rel="stylesheet" href="/theme/scripts/Gritter/css/jquery.gritter.css"/>
<link rel="stylesheet" href="/theme/scripts/google-code-prettify/prettify.css" type="text/css" />
<link rel="stylesheet" href="/theme/css/style.min.css?1362656609" />
<script type="text/javascript" src="/theme/scripts/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="/theme/scripts/modernizr.custom.76094.js"></script>
<script type="text/javascript" src="/theme/scripts/notyfy/jquery.notyfy.js"></script>
<script type="text/javascript" src="/theme/scripts/Gritter/js/jquery.gritter.min.js"></script>
<script type="text/javascript" src="/theme/scripts/less-1.3.3.min.js"></script>
<link rel="shortcut icon" href="/theme/images/favicon.png">
<style type="text/css">
.table-condensed{font-size: 12px}
.label-cancal{color:#777;background: #333}
.navbar.main .topnav>li.open .advanced-search{width:350px;}
.navbar.main .topnav>li.open .advanced-search input{color:#333;}
.navbar.main .topnav>li.open .advanced-search .control-group{margin:0;}
.navbar.main .topnav>li.open .advanced-search .control-label{width:65px;height:30px;line-height: 30px}
.navbar.main .topnav>li.open .advanced-search .controls{margin-left:85px}
input.nmb,select.nmb,form.nmb{margin-bottom: 0}
</style>
</head>
<body>

<!-- Start Content -->
<div class="container-fluid"> 	<div class="navbar main"> <a href="/" class="appbrand"><span><?php echo ($app_name); ?></span></a>
		<button type="button" class="btn btn-navbar"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
		<ul class="topnav pull-right">
			<?php if(in_array('advanced_search', $profile['permissions'])): ?><li class="dropdown visible-desktop">
				<a href="" data-toggle="dropdown" class="glyphicons search"><i></i>订单高级搜索 <span class="caret"></span></a>
				<div class="dropdown-menu advanced-search">
					<form class="form-horizontal" id="validateSubmitForm" method="POST" action="/order/search/">
						<div class="control-group">
							<label class="control-label" for="email">邮箱</label>
							<div class="controls"><input placeholder="account@example.com" id="email" name="email" type="text" value="<?php if(!empty($search_condition["email"])): echo ($search_condition["email"]); endif; ?>"></div>
						</div>
						<div class="control-group">
							<label class="control-label" for="uname">姓名</label>
							<div class="controls">
								<input placeholder="姓名" id="uname" name="uname" type="text" value="<?php if(!empty($search_condition["uname"])): echo ($search_condition["uname"]); endif; ?>">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="order_id">订单号</label>
							<div class="controls"><input placeholder="订单号，不带#号" id="order_id" name="order_id" value="<?php if(!empty($search_condition["order_id"])): echo ($search_condition["order_id"]); endif; ?>" type="text"></div>
						</div>
						<?php if(in_array('view_order_price', $profile['permissions'])): ?><div class="control-group">
							<label class="control-label" for="prices">订单金额</label>
							<div class="controls"><input placeholder="5000.00" id="prices" name="prices" type="text" value="<?php if(!empty($search_condition["prices"])): echo ($search_condition["prices"]); endif; ?>"></div>
						</div><?php endif; ?>
						<div class="control-group">
							<label class="control-label" for="domain">网站</label>
							<div class="controls">
								<select name="domain">
									<option value="">选择所有网站</option>
									<?php if(is_array($website)): $i = 0; $__LIST__ = $website;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$ws): $mod = ($i % 2 );++$i;?><optgroup label="<?php echo ($key); ?>">
										<?php if(is_array($ws)): $i = 0; $__LIST__ = $ws;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?><option value="<?php echo ($v["weburl"]); ?>" <?php if(!empty($search_condition['domain']) AND $v['weburl'] == $search_condition['domain']): ?>selected="selected"<?php endif; ?>><?php echo ($v["weburl"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
									</optgroup><?php endforeach; endif; else: echo "" ;endif; ?>
								</select>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="ip">IP</label>
							<div class="controls"><input placeholder="IP地址，例：127.0.0.1" id="ip" name="ip" type="text" value="<?php if(!empty($search_condition["ip"])): echo ($search_condition["ip"]); endif; ?>"></div>
						</div>
						<div class="control-group">
							<label class="control-label" for="first_name">日期</label>
							<div class="controls">
								<input class="input-small" placeholder="开始日期" id="start_date" name="start_date" type="text" value="<?php if(!empty($search_condition["start_date"])): echo (date('Y-m-d',$search_condition["start_date"])); endif; ?>" /> &nbsp;
								<input class="input-small" placeholder="结束日期" id="end_date" name="end_date" type="text" value="<?php if(!empty($search_condition["end_date"])): echo (date('Y-m-d',$search_condition["end_date"])); endif; ?>"/>
							</div>
						</div>
						<div class="control-group">
							<div class="controls">
								<button type="submit" class="btn btn-primary">走你</button>
							</div>
						</div>
					</form>
				</div>
			</li><?php endif; ?>
			<li class="account"> <a data-toggle="dropdown" href="javascript:void(0);" class="glyphicons logout lock"><span class="hidden-phone text"><?php echo ($username); ?></span><i></i></a>
				<ul class="dropdown-menu pull-right">
					<li><a href="javascript:void(0);" class="glyphicons cogwheel">Settings<i></i></a></li>
					<li class="highlight profile"> <span> <span class="heading">Profile <a href="javascript:void(0);" class="pull-right">edit</a></span> <span class="img"></span> <span class="details"> <a href="javascript:void(0);"><?php echo ($username); ?></a> contact@mosaicpro.biz </span> <span class="clearfix"></span> </span> </li>
					<li> <span> <a class="btn btn-default btn-small pull-right" style="padding: 2px 10px; background: #fff;" href="/account/logout">退出登录</a> </span> </li>
				</ul>
			</li>
		</ul>
	</div>
	<div id="wrapper"> 		<div id="menu" class="hidden-phone"> <span class="profile"> <a class="img" href="javascript:void(0);"><img src="/theme/images/photo.gif" alt="<?php echo ($username); ?>" /></a> <span> <strong><?php echo ($username); ?></strong> <a href="javascript:void(0);">edit account</a> </span> </span>
			<div id="search">
				<input type="text" placeholder="Quick search ..." />
				<button class="glyphicons search"><i></i></button>
			</div>
			<ul>
			<?php if(in_array('view_orders', $profile['permissions'])): ?><li class="glyphicons home <?php if($menu == "home"): ?>active<?php endif; ?>"> <a href="/"><i></i><span>首页</span></a></li><?php endif; ?>
			<?php if(in_array('view_orders', $profile['permissions'])): ?><li class="glyphicons list <?php if($menu == "orders"): ?>active<?php endif; ?>"> <a href="/order/lists"><i></i><span>订单列表</span></a></li><?php endif; ?>
			<?php if(in_array('view_orders_analysis', $profile['permissions'])): ?><li class="glyphicons charts <?php if($menu == "analysis"): ?>active<?php endif; ?>"> <a href="/order/analysis"><i></i><span>订单分析</span></a></li><?php endif; ?>
			<?php if(in_array('view_website', $profile['permissions'])): ?><li class="glyphicons cargo <?php if($menu == "website"): ?>active<?php endif; ?>"> <a href="/website"><i></i><span>网站接入</span></a></li><?php endif; ?>
			<?php if(in_array('view_account_list', $profile['permissions'])): ?><li class="glyphicons user <?php if($menu == "account"): ?>active<?php endif; ?>"> <a href="/account"><i></i><span>账号管理</span></a></li><?php endif; ?>
			</ul>
			<div class="clearfix" style="clear: both"></div>
		</div>
		<div id="content">
			<ul class="breadcrumb">
				<li><a href="/" class="glyphicons home"><i></i> Home</a></li>
				<li class="divider"></li>
				<li><?php echo ($page_title); ?></li>
			</ul>
			<div class="separator bottom"></div>
			<div class="heading-buttons">
				<h3><?php echo ($page_title); ?></h3>
				<div class="clearfix" style="clear: both;"></div>
			</div>
			<div class="separator bottom"></div>
			<h1>Not Found</h1><p>The requested URL was not found on this server.</p>509
		</div>
		<!-- End Wrapper --> 
	</div>
</div>

<!-- JQueryUI v1.9.2 --> 
<script src="/theme/scripts/jquery-ui-1.9.2.custom/js/jquery-ui-1.9.2.custom.min.js"></script>
<script src="/theme/scripts/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>
<script src="/theme/scripts/jquery.cookie.js"></script>
<script src="/theme/scripts/jquery.ba-resize.js"></script>
<script src="/theme/scripts/pixelmatrix-uniform/jquery.uniform.min.js"></script>
<script src="/bootstrap/js/bootstrap.min.js"></script>
<script src="/bootstrap/extend/bootstrap-select/bootstrap-select.js"></script>
<script src="/bootstrap/extend/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
<script src="/bootstrap/extend/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js"></script>
<script src="/bootstrap/extend/jasny-bootstrap/js/jasny-bootstrap.min.js" type="text/javascript"></script>
<script src="/bootstrap/extend/bootbox.js" type="text/javascript"></script>
<script src="/theme/scripts/load.js"></script>
</body>
</html>