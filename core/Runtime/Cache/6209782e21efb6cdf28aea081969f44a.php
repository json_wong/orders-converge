<?php if (!defined('THINK_PATH')) exit();?><!---->
<!DOCTYPE html>
<html>
<head>
<title>发货归档数据</title>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Bootstrap -->
<?php  if (APP_DEBUG === true) { ?>
<link rel="stylesheet" href="/bootstrap3/css/bootstrap.min.css">
<link rel="stylesheet" href="/bootstrap3/css/bootstrap-datetimepicker.min.css">
<?php  } else { ?>
<link rel="stylesheet" href="/bootstrap3/css/??bootstrap.min.css,bootstrap-datetimepicker.min.css">
<?php
} ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="/bootstrap3/js/html5shiv.min.js"></script>
<script src="/bootstrap3/js/respond.min.js"></script>
<![endif]-->
<style type="text/css">
@media (min-width:768px){
	.modal.lg .modal-dialog{width: 80%;}
}
table.first tr{border-bottom: 3px #999 solid}
/*#generate_excel_wrap{left:5px;top:20px;border: 1px #ccc solid;border-radius: 6px;-webkit-border-radius: 6px;padding: 10px;background-color: #fff}*/
</style>
</head>
<body>
<div class="container">
	<br>
	<div class="row">
		<div class="col-md-12">
			<p>归档日期：<?php echo ($query); ?>&nbsp;&nbsp;<a id="datetimepicker" class="btn btn-primary btn-xs" href="javascript:;">选择日期</a></p>
			<?php if(!empty($gc_hash)): ?>工厂访问地址：<code>http://tagtank.com/GFfehg9zHwo0NYU/?s=<?php echo ($date_encode); ?></code>&nbsp;&nbsp;密码：<code><?php echo ($gc_hash); ?></code><?php endif; if(!empty($kd_hash)): ?><br>快递访问地址：<code>http://tagtank.com/K2IskEZcQBLja/?s=<?php echo ($date_encode); ?></code>&nbsp;&nbsp;密码：<code><?php echo ($kd_hash); ?></code><?php endif; ?>
		</div>
	</div>
	<br>
<?php if(empty($orders)): ?><blockquote>当前日期没有归档数据</blockquote>
<?php else: ?>
	<table class="table first table-bordered table-condensed">
	<thead>
		<tr>
			<th width="5%">序号</th>
			<th width="30%">收货地址</th>
			<th width="55%">产品详情</th>
			<th width="10%">操作</th>
		</tr>
	</thead>
	<tbody>
		<?php if(is_array($orders)): $i = 0; $__LIST__ = $orders;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i; $tr_class = ""; if ($ids[$vo['id']]['visible'] != 'show') { $tr_class = "active"; } switch ($vo['order_status']) { case '已发货':$tr_class = "success";break; case '缺货':$tr_class = "danger";break; } ?>
		<tr data-order-id="<?php echo ($vo["id"]); ?>" class="<?php echo ($tr_class); ?>">
			<td><a name="<?php echo ($vo["id"]); ?>"></a><?php echo ($i); ?></td>
			<td>
				<p class="order_number">订单号：<?php if(!empty($vo["prefix"])): echo ($vo["prefix"]); ?>-<?php endif; echo ($vo["order_id"]); ?></p>
			<?php switch($vo["system_type"]): case "cscart": ?><p class="user_info">
						<?php echo ($vo["first_name"]); ?> <?php echo ($vo["last_name"]); ?><br/>
						<?php echo ($vo["values"]["user_address"]); ?><br/>
						<?php if(!empty($vo["values"]["user_address_2"])): echo ($vo["values"]["user_address_2"]); ?><br/><?php endif; ?>
						<?php echo ($vo["values"]["user_city"]); ?>, <?php echo ($vo["values"]["user_state"]); ?>, <?php echo ($vo["values"]["user_zipcode"]); ?><br/>
						<?php echo ($vo["values"]["user_country"]); ?><br/>
						T: <?php echo ($vo["values"]["user_phone"]); ?>
					</p>
					<div class="panel panel-warning">
						<div class="panel-heading">用户备注：<?php if(!empty($vo["values"]["cart"]["notes"])): echo ($vo["values"]["cart"]["notes"]); else: ?>无<?php endif; ?></div>
					</div><?php break;?>
				<?php case "magento": ?><p class="user_info"><?php echo ($vo["values"]["shipping_address"]); ?></p>
					<div class="panel panel-warning">
						<div class="panel-heading">用户备注：
						<?php if(!empty($vo["values"]["comment"]["customercomment"])): echo ($vo["values"]["comment"]["customercomment"]); else: ?>无<?php endif; ?><br>
						<?php if(!empty($vo["values"]["comment"]["customerfeedback"])): ?>郵便局留＆別のお届け先を指定：<strong><?php echo ($vo["values"]["comment"]["customerfeedback"]); ?></strong><?php endif; ?>
						</div>
					</div><?php break;?>
				<?php case "zencart": ?><p class="user_info">
					<?php echo ($vo["first_name"]); ?> <?php echo ($vo["last_name"]); ?><br/>
					<?php echo ($vo["values"]["customer"]["street_address"]); ?> <?php echo ($vo["values"]["customer"]["suburb"]); ?><br/>
					<?php echo ($vo["values"]["customer"]["city"]); ?>, <?php echo ($vo["values"]["customer"]["state"]); ?>, <?php echo ($vo["values"]["customer"]["postcode"]); ?><br/>
					<?php echo ($vo["values"]["customer"]["country"]["title"]); ?><br/>
					T: <?php echo ($vo["values"]["customer"]["telephone"]); ?>
					</p>
					<div class="panel panel-warning"><div class="panel-heading">用户备注：<?php if(!empty($vo["values"]["info"]["comments"])): echo ($vo["values"]["info"]["comments"]); else: ?>无<?php endif; ?></div>
					</div><?php break;?>
				<?php case "wordpress": ?><p class="user_info">
					<?php echo ($vo["first_name"]); ?> <?php echo ($vo["last_name"]); ?><br/>
					<?php echo ($vo["values"]["user_info"]["4"]); ?> <?php echo ($vo["values"]["user_info"]["5"]); ?><br/>
					<?php echo ($vo["values"]["user_info"]["6"]); ?>, <?php echo ($vo["values"]["user_info"]["8"]); ?><br/>
					<?php echo ($vo["values"]["user_info"]["7"]["0"]); ?><br/>
					T: <?php echo ($vo["values"]["user_info"]["18"]); ?>
					</p><?php break; endswitch;?>
			</td>
			<td>
			<?php switch($vo["system_type"]): case "cscart": ?><table width="100%" class="table table-bordered table-condensed">
						<?php if(is_array($vo["values"]["cart"]["products"])): $j = 0; $__LIST__ = $vo["values"]["cart"]["products"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($j % 2 );++$j;?><tr class="<?php echo ($tr_class); ?>">
							<td width="25%">
								<?php if(strpos($v['main_pair']['detailed']['image_path'], 'http') === false): ?><img class="img-thumbnail" src="http://<?php echo ($vo["values"]["domain"]); echo ($v["main_pair"]["detailed"]["image_path"]); ?>">
								<?php else: ?>
									<img class="img-thumbnail" src="<?php echo ($v["main_pair"]["detailed"]["image_path"]); ?>"><?php endif; ?>
							</td>
							
							<?php if(!empty($v["product_options_info"])): ?><td width="65%" class="product_options">
							<?php  if (is_array($v['product_options_info'])) { $options = array(); foreach ($v['product_options_info'] as $value) { foreach ($value as $op_k => $op_v) { $options[] = $op_k . '：' . $op_v; } } echo join('<br/>', $options); } else { echo $v['product_options_info']; } ?>
							</td><?php endif; ?>
							<td><?php echo (number_format($v["amount"])); ?>件</td>
						</tr><?php endforeach; endif; else: echo "" ;endif; ?>
					</table><?php break;?>
				<?php case "magento": ?><table width="100%" class="table table-striped table-bordered table-condensed">
						<?php if(!empty($vo['values']['product']['item_id'])): ?><tr class="<?php echo ($tr_class); ?>">
							<td width="70%">
								<a class="hidden-print" href="<?php echo ($vo["values"]["product"]["product_url"]); ?>" target="_blank"><?php echo ($vo["values"]["product"]["name"]); ?></a>
								<div class="visible-print product_title"><?php echo ($vo["values"]["product"]["name"]); ?></div>
							</td>
							<td width="20%" class="product_options">
							<?php  if (is_array($vo['values']['product']['product_options']['options'])) { $options = array(); $product_options = $vo['values']['product']['product_options']; foreach ($product_options['options'] as $value) { $options[] = $value['label'] . '：' . $value['value']; } echo join('<br/>', $options); } else { echo $vo['values']['product']['product_options']['options']; } ?>
							</td>
							<td><?php echo (number_format($vo["values"]["product"]["qty_ordered"])); ?>件</td>
						</tr>
						<?php else: ?>
						<?php if(is_array($vo["values"]["product"])): $j = 0; $__LIST__ = $vo["values"]["product"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($j % 2 );++$j;?><tr class="<?php echo ($tr_class); ?>">
							<td width="70%">
								<a class="hidden-print" href="<?php echo ($v["product_url"]); ?>" target="_blank"><?php echo ($v["name"]); ?></a>
								<div class="visible-print product_title"><?php echo ($v["name"]); ?></div>
							</td>
							<?php if(!empty($v["product_options"]["options"])): ?><td width="20%" class="product_options">
							<?php  if (is_array($v['product_options']['options'])) { $options = array(); foreach ($v['product_options']['options'] as $value) { $options[] = $value['label'] . '：' . $value['value']; } echo join('<br/>', $options); } else { echo $v['product_options']['options']; } ?>
							</td><?php endif; ?>
							<td><?php echo (number_format($v["qty_ordered"])); ?>件</td>
						</tr><?php endforeach; endif; else: echo "" ;endif; endif; ?>
					</table><?php break;?>
				<?php case "zencart": ?><table width="100%" class="table table-striped table-bordered table-condensed">
						<?php if(is_array($vo["values"]["products"])): $j = 0; $__LIST__ = $vo["values"]["products"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($j % 2 );++$j;?><tr class="<?php echo ($tr_class); ?>">
							<td width="70%">
								<a class="hidden-print" href="http://<?php echo ($vo["values"]["domain"]); ?>/<?php echo ($v["name"]); ?>-p-<?php echo ($v["id"]); ?>.html" target="_blank"><?php echo ($v["name"]); ?></a>
								<div class="visible-print product_title"><?php echo ($v["name"]); ?></div>
							</td>
							<?php if(!empty($v["product_options"]["options"])): ?><td width="20%" class="product_options">
							<?php  if (is_array($v['product_options_info'])) { $options = array(); foreach ($v['product_options_info'] as $value) { foreach ($value as $op_k => $op_v) { $options[] = $op_k . '：' . $op_v; } } echo join('<br/>', $options); } else { echo $v['product_options_info']; } ?>
							</td><?php endif; ?>
							<td class="center"><?php echo (number_format($v["qty"])); ?>件</td>
						</tr><?php endforeach; endif; else: echo "" ;endif; ?>
					</table><?php break;?>
				<?php case "wordpress": ?><table width="100%" class="table table-striped table-bordered table-condensed">
						<?php if(is_array($vo["values"]["order_info"]["cart_items"])): $j = 0; $__LIST__ = $vo["values"]["order_info"]["cart_items"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($j % 2 );++$j;?><tr class="<?php echo ($tr_class); ?>">
							<td width="70%">
								<a class="hidden-print" href="<?php echo ($v["product_url"]); ?>" target="_blank"><?php echo ($v["product_name"]); ?></a>
								<div class="visible-print product_title"><?php echo ($v["product_name"]); ?></div>
							</td>
							<?php if(!empty($v["product_options_info"])): ?><td width="20%" class="product_options">
							<?php  if (is_array($v['product_options_info'])) { $options = array(); foreach ($v['product_options_info'] as $value) { foreach ($value as $op_k => $op_v) { $options[] = $op_k . '：' . $op_v; } } echo join('<br/>', $options); } else { echo $v['product_options_info']; } ?>
							</td><?php endif; ?>
							<td class="center"><?php echo (number_format($v["quantity"])); ?>件</td>
						</tr><?php endforeach; endif; else: echo "" ;endif; ?>
					</table><?php break; endswitch;?>
			<?php if(!empty($vo["order_remark"])): ?><div class="panel panel-default">
				<div class="panel-heading">客服备注</div>
				<div class="panel-body order_remark">
					<ul>
						<?php if(is_array($vo["order_remark"])): $i = 0; $__LIST__ = $vo["order_remark"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?><li><?php echo ($v["pubdate"]); ?> <?php echo ($v["remark"]); ?></li><?php endforeach; endif; else: echo "" ;endif; ?>
					</ul>
				</div>
			</div><?php endif; ?>
			</td>
			<td>
				<?php if($vo['order_status'] == '待发货'): ?><p>&nbsp;</p>
				<!-- <p><button type="button" class="btn btn-success btn-sm mark_p">标记为已发货</button></p> -->
				<p><button type="button" class="btn btn-danger btn-sm mark_stockout">标记为缺货</button></p>
				<?php if($ids[$vo['id']]['visible'] == 'show'): ?><a href="javascript:;" class="toggle_order_visible" data-value="hidden">隐藏此单</a>
				<?php else: ?>
				<a href="javascript:;" class="toggle_order_visible" data-value="show">显示此单</a><?php endif; ?>
				<?php elseif($vo['order_status'] == '已发货'): ?>
				<p><span class="label <?php echo ($vo["order_status_label_class"]); ?>"><?php echo ($vo["order_status"]); ?></span></p>
				<p>物流单号：<br><?php echo (join('<br>', $vo["ems_number"])); ?></p>
				<?php else: ?>
				<p><span class="label <?php echo ($vo["order_status_label_class"]); ?>"><?php echo ($vo["order_status"]); ?></span></p><?php endif; ?>
			</td>
		</tr><?php endforeach; endif; else: echo "" ;endif; ?>
	</tbody>
	</table><?php endif; ?>
<!-- 	<div id="generate_excel_wrap" class="hidden-print" data-spy="affix">
		<a href="javascript:;" class="generate_excel_link">生成Excel</a>
	</div> -->
</div>
<div class="modal fade lg" id="P-modal" tabindex="-1" role="dialog" aria-labelledby="modal-p-title" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="modal-p-title">标记订单为【已发货】</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-7">
						<div class="col-sm-6">
							<p id="user-info"></p>
						</div>
						<div class="col-sm-6"><p id="order-number"></p></div>
						<hr style="margin:5px 0 10px">
						<div class="col-sm-12"><p id="product-info"></p></div>
					</div>
					<div class="col-sm-5">
						<?php $__FOR_START_6130__=1;$__FOR_END_6130__=9;for($i=$__FOR_START_6130__;$i < $__FOR_END_6130__;$i+=1){ ?><p><input type="text" class="form-control logistics" placeholder="<?php echo ($i); ?>. 填写物流单号，一个文本框输入一个"></p><?php } ?>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<span class="modal-message"></span>
				<button type="button" class="btn btn-primary mark_p_confirm">标记为已发货</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="stockout-modal" tabindex="-1" role="dialog" aria-labelledby="modal-stockout-title" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="modal-stockout-title">标记订单为【缺货】</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-6">
						<p id="user-info"></p>
					</div>
					<div class="col-sm-6"><p id="order-number"></p></div>
					<hr style="margin:5px 0 10px">
					<div class="col-sm-12"><p id="product-info"></p></div>
				</div>
			</div>
			<div class="modal-footer">
				<span class="modal-message"></span>
				<button type="button" class="btn btn-danger mark_stockout_confirm">标记为缺货</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="hidden-order-modal" tabindex="-1" role="dialog" aria-labelledby="modal-hidden-order-title" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="modal-hidden-order-title">显示/隐藏此订单</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-6">
						<p id="user-info"></p>
					</div>
					<div class="col-sm-6"><p id="order-number"></p></div>
					<hr style="margin:5px 0 10px">
					<div class="col-sm-12"><p id="product-info"></p></div>
				</div>
			</div>
			<div class="modal-footer">
				<span class="modal-message"></span>
				<button type="button" class="btn btn-danger mark_hidden_order_confirm">显示/隐藏此订单</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
			</div>
		</div>
	</div>
</div>
<?php  if (APP_DEBUG === true) { ?>
<script src="/bootstrap3/js/jquery-1.11.1.min.js"></script>
<script src="/bootstrap3/js/bootstrap.min.js"></script>
<script src="/bootstrap3/js/bootstrap-datetimepicker.min.js"></script>
<script src="/bootstrap3/js/bootstrap-datetimepicker.zh-CN.js"></script>
<?php } else {?>
<script src="/bootstrap3/js/??jquery-1.11.1.min.js,bootstrap.min.js,bootstrap-datetimepicker.min.js,bootstrap-datetimepicker.zh-CN.js"></script>
<?php }?>
<script type="text/javascript">
/**
 * 时间对象的格式化;
 */
Date.prototype.format = function(format) {
    /*
     * eg:format="YYYY-MM-dd hh:mm:ss";
     */
    var o = {
        "M+" :this.getMonth() + 1, // month
        "d+" :this.getDate(), // day
        "h+" :this.getHours(), // hour
        "m+" :this.getMinutes(), // minute
        "s+" :this.getSeconds(), // second
        "q+" :Math.floor((this.getMonth() + 3) / 3), // quarter
        "S" :this.getMilliseconds()
    // millisecond
    }

    if (/(y+)/.test(format)) {
        format = format.replace(RegExp.$1, (this.getFullYear() + "")
                .substr(4 - RegExp.$1.length));
    }

    for ( var k in o ) {
        if (new RegExp("(" + k + ")").test(format)) {
            format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k]
                    : ("00" + o[k]).substr(("" + o[k]).length));
        }
    }
    return format;
}

$(function(){
	var p_modal = $("#P-modal");
	var ho_modal = $("#hidden-order-modal");
	var stockout_modal = $("#stockout-modal");
	var mark_p_confirm = p_modal.find('.mark_p_confirm');
	var mark_stockout_confirm = stockout_modal.find('.mark_stockout_confirm');
	var mark_hidden_order_confirm = ho_modal.find('.mark_hidden_order_confirm');

	$('#datetimepicker').datetimepicker(
		{format:"yyyy-mm-dd", language:'zh-CN', minView: 2, todayHighlight: 1, endDate:new Date().format("yyyy-MM-dd"), initialDate:'<?php echo ($query); ?>'}
	).on('changeDate', function(event) {
		window.location.href="/order/prints?query=" + event.date.format("yyyy-MM-dd");
	});

	$(".toggle_order_visible").on('click', function(event) {
		var imageurl = '';
		var option = '';
		var parentNode = $(this).parents('tr');
		var order_id = parentNode.attr('data-order-id');
		var order_number = parentNode.find('.order_number').html();
		var user_info = parentNode.find('.user_info').html();
		var toggle_value = $(this).attr('data-value');

		ho_modal.find('#order-number').html(order_number);
		ho_modal.find('#user-info').html(user_info);
		mark_hidden_order_confirm.attr({'data-order-id':order_id, "data-toggle-value": toggle_value});
		ho_modal.modal('show');
	});

	mark_hidden_order_confirm.on('click', function(event) {
		var number = "";
		var order_id = $(this).attr('data-order-id');
		var toggle_value = $(this).attr('data-toggle-value');
		var message = ho_modal.find('.modal-message');
		$(this).attr('disabled', true);

		$.ajax({
			url: '/ajax/toggle_order_visible', type: 'POST', dataType: 'json',
			data: {order_id: order_id, query: '<?php echo ($query); ?>', toggle_value: toggle_value},
		}).done(function(result) {
			if (result.status == 200) {
				message.attr('class', 'modal-message text-success').html("操作成功！");
				window.location.reload();
				// window.location.href=window.location.href + "#" + order_id
			} else {
				message.attr('class', 'modal-message text-danger').html(result.info);
			}
		}).fail(function() {
			message.attr('class', 'modal-message text-danger').html("网络或服务器遇到问题，请稍后重试！");
		}).always(function() {
			$(this).attr('disabled', false);
		});
	});

	$(".mark_stockout").on('click', function(event) {
		var imageurl = '';
		var option = '';
		var parentNode = $(this).parents('tr');
		var order_id = parentNode.attr('data-order-id');
		var order_number = parentNode.find('.order_number').html();
		var user_info = parentNode.find('.user_info').html();
		var products_title = parentNode.find('.product_title');
		var products_option = parentNode.find('.product_options');
		var products_image = parentNode.find('img.img-thumbnail');
		var products_info = '<table class="table table-condensed"><thead><tr><th width="20%">产品图片</th><th width="80%">产品属性</th></tr></thead><tbody>';

		products_image.each(function(index, el) {
			products_info += "<tr>";
			imageurl = $(products_image[index]).attr('src');
			option = $(products_option[index]).html();
			if (imageurl) {
				products_info += '<td><img src="' + imageurl + '" class="img-thumbnail"></td>';
			} else {
				products_info += '<td class="text-center"><span class="label label-danger">无图片</span></td>';
			}
			
			if (option) {
				products_info += '<td>' + $(products_option[index]).html() + '</td>';
			} else {
				products_info += '<td class="text-center"><span class="label label-danger">无属性</span></td>';
			}
			
			products_info += "</tr>";
		});
		products_info += '</tbody></table>';

		stockout_modal.find('#order-number').html(order_number);
		stockout_modal.find('#user-info').html(user_info);
		stockout_modal.find('#product-info').html(products_info);
		mark_stockout_confirm.attr('data-order-id', order_id);
		stockout_modal.modal('show');
	});

	mark_stockout_confirm.on('click', function(event) {
		var number = "";
		var order_id = $(this).attr('data-order-id');
		var message = stockout_modal.find('.modal-message');
		$(this).attr('disabled', true);

		$.ajax({
			url: '/ajax/mark_order_stockout', type: 'POST', dataType: 'json',
			data: {order_id: order_id},
		}).done(function(result) {
			if (result.status == 200) {
				message.attr('class', 'modal-message text-success').html("操作成功！");
				window.location.reload();
				// window.location.href=window.location.href + "#" + order_id
			};
		}).fail(function() {
			message.attr('class', 'modal-message text-danger').html("网络或服务器遇到问题，请稍后重试！");
		}).always(function() {
			$(this).attr('disabled', false);
		});
	});

	$('.mark_p').on('click', function (e) {
		var imageurl = '';
		var option = '';
		var parentNode = $(this).parents('tr');
		var order_id = parentNode.attr('data-order-id');
		var order_number = parentNode.find('.order_number').html();
		var user_info = parentNode.find('.user_info').html();
		var products_title = parentNode.find('.product_title');
		var products_option = parentNode.find('.product_options');
		var products_image = parentNode.find('img.img-thumbnail');
		var products_info = '<table class="table table-condensed"><thead><tr><th width="20%" class="text-center">产品图片</th><th width="50%">产品标题</th><th class="text-center" width="30%">产品属性</th></tr></thead><tbody>';
		products_title.each(function(index, el) {
			products_info += "<tr>";
			imageurl = $(products_image[index]).attr('src');
			option = $(products_option[index]).html();
			if (imageurl) {
				products_info += '<td><img src="' + imageurl + '" class="img-thumbnail"></td>';
			} else {
				products_info += '<td class="text-center"><span class="label label-danger">无图片</span></td>';
			}
			
			products_info += '<td>' + $(el).html() + '</td>';
			if (option) {
				products_info += '<td>' + $(products_option[index]).html() + '</td>';
			} else {
				products_info += '<td class="text-center"><span class="label label-danger">无属性</span></td>';
			}
			
			products_info += "</tr>";
		});
		products_info += '</tbody></table>';

		p_modal.find('#order-number').html(order_number);
		p_modal.find('#user-info').html(user_info);
		p_modal.find('#product-info').html(products_info);
		mark_p_confirm.attr('data-order-id', order_id);
		p_modal.modal('show');
	});

	mark_p_confirm.on('click', function(event) {
		var number = "";
		var order_id = $(this).attr('data-order-id');
		var logistics = [];
		var message = p_modal.find('.modal-message');
		$(this).attr('disabled', true);
		$("input.logistics").each(function(index, el) {
			number = $(el).val();
			if (number != '') {
				logistics.push(number);
			}
		});

		if (logistics.length > 0) {
			$.ajax({
				url: '/ajax/mark_order_p', type: 'POST', dataType: 'json',
				data: {order_id: order_id, logistics: logistics},
			})
			.done(function(result) {
				if (result.status == 200) {
					$("input.logistics").val("");
					message.attr('class', 'modal-message text-success').html("操作成功！");
					window.location.reload();
					// window.location.href=window.location.href + "#" + order_id
				};
			})
			.fail(function() {
				message.attr('class', 'modal-message text-danger').html("网络或服务器遇到问题，请稍后重试！");
			})
			.always(function() {
				$(this).attr('disabled', false);
			});
		} else {
			message.attr('class', 'modal-message text-danger').html("物流单号不能为空！");
		}
	});
});
</script>
</body>
</html>