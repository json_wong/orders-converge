<?php
return array(
		/* 常量设置 */
		'APP_NAME'				=>	APP_NAME,
		'APP_DOMAIN'			=>	APP_DOMAIN,
		'TMPL_PARSE_STRING'  	=>	array(
				'__PUBLIC__' 	=> 	APP_DOMAIN . '/public',
				'__JS__' 		=> 	APP_DOMAIN . '/public/js',
				'__CSS__'		=>	APP_DOMAIN . '/public/css',
				'__IMG__'		=>	APP_DOMAIN . '/public/images',
				'__UPLOAD__' 	=> 	APP_DOMAIN . '/upload'
		),
		
		/* 数据库设置 */
		'DB_TYPE'               => 	'mysql',     // 数据库类型
		'DB_HOST'               => 	'localhost', // 服务器地址
		'DB_NAME'               => 	'orders_converge',          // 数据库名
		'DB_USER'               => 	'root',      // 用户名
		'DB_PWD'                => 	'',          // 密码
		'DB_PORT'               => 	'3306',        // 端口
		'DB_PREFIX'             => 	'oc_',    // 数据库表前缀
		
		/* Cookie设置 */
		'COOKIE_PREFIX'         => 	'oc_',      // Cookie前缀 避免冲突
		'DEFAULT_TIMEZONE'		=>  'Asia/Shanghai',
		
		//模版定界符
		'TMPL_L_DELIM'			=> 	"<!--{",
		'TMPL_R_DELIM'			=> 	"}-->",
		'LAYOUT_ON'				=>	true,
		
		/* URL设置 */
		'URL_CASE_INSENSITIVE'  => 	true,   // 默认false 表示URL区分大小写 true则表示不区分大小写
		'URL_MODEL'             => 	2,       // URL访问模式,可选参数0、1、2、3,代表以下四种模式：
		// 0 (普通模式); 1 (PATHINFO 模式); 2 (REWRITE  模式); 3 (兼容模式)  默认为PATHINFO 模式，提供最好的用户体验和SEO支持
		'URL_PATHINFO_DEPR'     => 	'/',	// PATHINFO模式下，各参数之间的分割符号
		'URL_PATHINFO_FETCH'    =>  'ORIG_PATH_INFO,REDIRECT_PATH_INFO,REDIRECT_URL', // 用于兼容判断PATH_INFO 参数的SERVER替代变量列表
		'URL_HTML_SUFFIX'       => 	'.html',  // URL伪静态后缀设置
		'URL_DISPATCH_ON' 		=> 	true,
		
		//默认皮肤
		'DEFAULT_THEME'			=>	'default',
		
		/* 上传相关设置 */
		'UPLOAD_SAVE_PATH'		=>	'/upload/' . date('Y') . '/' . date('m') . '/',
		'FILE_MAX_SIZE'			=>	'5242880',
		'GENERATE_THUMB'		=>	true,
		'THUMB_PREFIX'			=>	's_',
		'THUMB_MAX_WIDTH'		=>	'313,313',
		'THUMB_MAX_HEIGHT'		=>	'235,235',
		'ALLOW_UPLOAD_EXTS'		=>	array('jpg','jpeg','gif','png'),
		
		'SYSTEM_RANDOM'			=>	'buXKiksWfDkuZOZNTZsdDzeNUUuVPJLtoEexZWwRYjZVniaOmIIFMLzbXGULgNbX',
		
		//调试相关
		'SHOW_PAGE_TRACE' 		=>	true,
		
		//安全
		'TOKEN_ON'				=>	true,
		'TOKEN_NAME'			=>	'hash',
		'TOKEN_TYPE'			=>	'md5',
		'TOKEN_RESET'			=>	true
);